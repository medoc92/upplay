/* Copyright (C) 2014 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _OHRADIO_QO_INCLUDED
#define _OHRADIO_QO_INCLUDED

#include <string>

#include <QObject>
#include <QThread>
#include <QTimer>

#include "libupnpp/log.h"
#include "libupnpp/control/ohradio.hxx"
#include "ohpool.h"

class OHRadioQO : public QObject, public UPnPClient::VarEventReporter {
Q_OBJECT

public:
    OHRadioQO(UPnPClient::OHRDH ohp, QObject *parent = 0)
        : QObject(parent), m_curid(-1), m_srv(ohp), m_errcnt(0) {
        m_srv->installReporter(this);
    }

    virtual ~OHRadioQO() {
        udiscon();
    }
    virtual void udiscon() {
        if (m_srv)
            m_srv->installReporter(0);
    }

    // Events: we are interested in:
    //  Id (current playing), TransportState, Shuffle, Repeat.
    //  IdArray (the current playlist. We use readList() to
    //   translate this into actual track information)
    
    virtual void changed(const char *nm, int value) {
        LOGDEB1("OHRadioQO: changed: " << nm << " (int): " << value << "\n");;
        if (!strcmp(nm, "Id")) {
            m_curid = value;
            emit currentTrackId(value);
        } else if (!strcmp(nm, "TransportState")) {
            emit tpStateChanged(value);
        }
    }
    virtual void changed(const char *nm, std::vector<int> ids) {
        Q_UNUSED(nm);
        LOGDEB1("OHRadioQO: changed: " << nm << " (vector<int>)\n");
        emit __idArrayChanged(ids);
    }
    virtual void changed(const char * nm, const char *value) {
        Q_UNUSED(nm);
        Q_UNUSED(value);
        LOGDEB1("OHRadioQO: changed: " << nm << " (char*): " << value << "\n");
    }

public slots:

    // Ping renderer to check it's still there.
    virtual void testconn() {
        int val;
        if (m_srv->id(&val) != 0) {
            if (m_errcnt++ > 2) {
                emit connectionLost();
            }
            return;
        }
        m_errcnt = 0;
    }

    // Read state from the remote. Used when starting up, to avoid having to wait for events.
    virtual void fetchState() {
        LOGDEB1("OHRadioQO::fetchState()\n");
        std::vector<int> ids;
        int tok;
        if (idArray(&ids, &tok))
            onIdArrayChanged(ids);
        if (m_srv->id(&tok) == 0) {
            m_curid = tok;
            emit currentTrackId(m_curid);
        }
        UPnPClient::OHPlaylist::TPState tpst;
        if (m_srv->transportState(&tpst) == 0)
            emit tpStateChanged(tpst);
    }

    virtual bool play() {
        LOGDEB1("OHRadioQO::play()\n");
        return m_srv->play() == 0;
    }

    virtual bool stop() {
        LOGDEB1("OHRadioQO::stop()\n");
        return m_srv->stop() == 0;
    }

    virtual bool pause() {
        LOGDEB1("OHRadioQO::pause()\n");
        m_srv->pause();
        // Pause generally silently fails to accomplish
        // anything. Fetch and signal the actual device state so that
        // the playctl buttons reflect it
        UPnPClient::OHPlaylist::TPState tpst;
        if (m_srv->transportState(&tpst) == 0) {
            LOGDEB1("OHPL::pause(): tpstate: " << tpst << "\n");
            emit tpStateChanged(tpst);
        }
        return true;
    }

    virtual bool setIdx(int idx) {
        LOGDEB1("OHRadioQO::setIdx: idx: " << idx << "\n");
        if (idx < 0 || idx >= int(m_idsv.size()))
            return false;

        int id = m_idsv[idx];
        LOGDEB1("OHRadioQO::setIdx: id: " << id << "\n");
        
        std::unordered_map<int, UPnPClient::UPnPDirObject>::iterator it;
        if ((it = m_metapool.find(id)) == m_metapool.end()) {
            LOGERR("OHRadioQO::setId: id not found\n");
            return false;
        }
        const std::string& uri = it->second.m_resources[0].m_uri;
        int ret = m_srv->setId(id, uri);
        if (ret != 0) {
            LOGERR("OHRadioQO: setId failed: " << ret << "\n");
            return false;
        }
        m_curid = id;
        return true;
    }

signals:
    void currentTrackId(int);
    void trackArrayChanged();
    void tpStateChanged(int);
    void connectionLost();
    
    // This is an internal signal. Use trackArrayChanged()
    void __idArrayChanged(std::vector<int>);

private slots:

    void emitCurId() {
        emit currentTrackId(m_curid);
    }        

    void onIdArrayChanged(std::vector<int> nids) {
        m_idsv = nids;

        if (!ohupdmetapool(nids, m_curid, m_metapool, m_srv))
            return;

        LOGDEB1("OHRad::onIdArrayChanged: emit trackArrayChanged(). " <<
                "idsv size " << m_idsv.size() << " pool size " << m_metapool.size() << "\n");
        emit trackArrayChanged();
        // For some weird reason, if I emit currentTrackId here, it is always queued *before* the
        // track array?? So that the current track can't be updated in the playlist because its
        // array is empty. So, QTimer...
        QTimer::singleShot(0, this, SLOT(emitCurId()));
    }
    
protected:
    std::vector<int> m_idsv;
    std::unordered_map<int, UPnPClient::UPnPDirObject> m_metapool;
    int m_curid;

private:
    virtual bool idArray(std::vector<int> *ids, int *tokp) {
        return m_srv->idArray(ids, tokp) == 0;
    }

    UPnPClient::OHRDH m_srv;
    int m_errcnt;
};

#endif // _OHRADIO_QO_INCLUDED
