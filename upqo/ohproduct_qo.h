/* Copyright (C) 2014 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _OHPRODUCT_QO_INCLUDED
#define _OHPRODUCT_QO_INCLUDED

#include <string>

#include <QObject>
#include <QThread>

#include "libupnpp/log.h"
#include "libupnpp/control/ohproduct.hxx"

class OHProductQO : public QObject, public UPnPClient::VarEventReporter {
Q_OBJECT

public:
    OHProductQO(UPnPClient::OHPRH ohp, QObject *parent = 0)
        : QObject(parent), m_srv(ohp) {
        m_srv->installReporter(this);
    }

    virtual ~OHProductQO() {
        udiscon();
    }
    virtual void udiscon() {
        if (m_srv)
            m_srv->installReporter(0);
    }

    virtual void changed(const char *nm, int value)
    {
        if (!strcmp(nm, "SourceIndex")) {
            if (!m_sources.empty()) {
                // Don't emit this if we have no sources yet, will only confuse things
                LOGDEB0("OHProductQo: Changed: " << nm << " (int): " << value << "\n");
                onSourceIndexChanged(value);
            }
        } 
    }

    virtual void changed(const char *nm, const char *value) {
        if (!strcmp(nm, "SourceXml")) {
            LOGDEB0("OHProductQO: Changed: " << nm << " (char*): " << value << "\n");
            std::string xml = value;
            m_sources.clear();
            m_srv->parseSourceXML(xml, m_sources);
        }
    }

    enum SourceType{OHPR_SourceUnknown, OHPR_SourcePlaylist, OHPR_SourceRadio, OHPR_SourceReceiver};
        
    int getSourceType() {
        getSourceIndex();
        fetchSources();
        if (m_source_index >= 0 && m_source_index < int(m_sources.size())) {
            return srcStringToType(m_sources[m_source_index].type);
        }
        return OHPR_SourceUnknown;
    }

    std::string getSourceName() {
        getSourceIndex();
        fetchSources();
        if (m_source_index >= 0 && m_source_index < int(m_sources.size())) {
            return m_sources[m_source_index].name;
        }
        return std::string();
    }

    virtual bool getSourceIndex(int *idxp = nullptr) {
        bool ret = true;
        if (m_source_index == -1) {
            ret = m_srv->sourceIndex(&m_source_index) == 0;
        }
        if (idxp)
            *idxp = m_source_index;
        return ret;
    }

    virtual bool getSources(std::vector<UPnPClient::OHProduct::Source>& srcs) {
        if (!fetchSources())
            return false;
        srcs = m_sources;
        return true;
    }

public slots:

    virtual bool setSourceIndex(int index) {
        return m_srv->setSourceIndex(index) == 0;
    }

    virtual bool setSourceIndexByName(const std::string& nm) {
        return m_srv->setSourceIndexByName(nm) == 0;
    }
    
signals:
    void sourceTypeChanged(int);

private:
    bool fetchSources() {
        if (m_sources.empty()) {
            return m_srv->getSources(m_sources) == 0;
        }
        return true;
    }
    int srcStringToType(const std::string& stype) {
        LOGDEB("OHProductQO::srcStringToType: [" << stype << "]\n");
        if (!stype.compare("Playlist")) {
            return OHPR_SourcePlaylist;
        } else if (!stype.compare("Radio")) {
            return OHPR_SourceRadio;
        } else if (!stype.compare("Receiver")) {
            return OHPR_SourceReceiver;
        }
        return OHPR_SourceUnknown;
    }

    void onSourceIndexChanged(int idx) {
        std::string stype;
        if (idx >= 0 && idx < int(m_sources.size())) {
            stype = m_sources[idx].type;
            m_source_index = idx;
        }
        emit sourceTypeChanged(srcStringToType(stype));
    }

    UPnPClient::OHPRH m_srv;
    std::vector<UPnPClient::OHProduct::Source> m_sources;
    int m_source_index{-1};
};

#endif // _OHPRODUCT_QO_INCLUDED
