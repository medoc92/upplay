/* Copyright (C) 2014-2021 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _RENDERINGCONTROL_QO_H_INCLUDED
#define _RENDERINGCONTROL_QO_H_INCLUDED
#include <QObject>
#include <QSettings>

#include "libupnpp/log.h"
#include "libupnpp/control/renderingcontrol.hxx"
#include "upadapt/upputils.h"

class RenderingControlQO : public QObject, public UPnPClient::VarEventReporter {
    Q_OBJECT

public:
    RenderingControlQO(UPnPClient::RDCH rdc, QObject *parent = 0)
        : QObject(parent), m_srv(rdc) {
        m_srv->installReporter(this);
            
        QSettings settings;
        QString varnm = ::u8s2qs(
            rendererSettingsKey("pollvolume", ::u8s2qs(m_srv->getFriendlyName())));
        if (settings.contains(varnm) && settings.value(varnm).toBool()) {
            LOGDEB(m_srv->getFriendlyName() <<  ": Volume polling\n");
            m_polling = true;
        }
    }
    ~RenderingControlQO() {
        udiscon();
    }
    virtual void udiscon() {
        if (m_srv)
            m_srv->installReporter(0);
    }

    bool polling() {
        return m_polling;
    }

    void changed(const char *nm, int value) {
        LOGDEB1("RDR: Changed: " << nm << " : " << value << " (int)\n");
        if (!strcmp(nm, "Volume")) {
            emit volumeChanged(value);
        } else if (!strcmp(nm, "Mute")) {
            emit muteChanged(value);
        }
    }
    void changed(const char * /*nm*/, const char * /*value*/) {
        LOGDEB1("RDR: Changed: " << nm << " : " << value << " (char*)\n");
    }


public slots:
    void update() {
        auto vol = volume();
        if (vol != m_volume) {
            m_volume = vol;
            emit volumeChanged(m_volume);
        }
        auto mt = mute();
        if (mt != bool(m_mute)) {
            m_mute = mt;
            emit muteChanged(m_mute);
        }        
    }
    
    int volume() {
        return m_srv->getVolume();
    }
    bool mute()  {
        return m_srv->getMute();
    }
    void setVolume(int vol) {
        m_srv->setVolume(vol);
    }
    void setMute(bool mute) {
        m_srv->setMute(mute);
    }

signals:
    void volumeChanged(int);
    void muteChanged(bool);

private:
    UPnPClient::RDCH m_srv;
    bool m_polling{false};
    int m_volume{-1};
    int m_mute{-1};
};
#endif /* _RENDERINGCONTROL_QO_H_INCLUDED */
