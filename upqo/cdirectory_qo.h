/* Copyright (C) 2014 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _DIRREADER_H_INCLUDED_
#define _DIRREADER_H_INCLUDED_

#include <string>
#include <vector>
#include <iostream>

#include <QThread>
#include <QString>

#include "libupnpp/log.h"
#include "libupnpp/control/cdirectory.hxx"

// Ensure that we build with older libupnpp versions which don't
// export the UPNP_E_XX error codes
#ifndef UPNP_E_SUCCESS
#define UPNP_E_SUCCESS            0
#endif
#ifndef UPNP_E_OUTOF_MEMORY
#define UPNP_E_OUTOF_MEMORY        -104
#endif

// Asynchronous Content Directory reader.
// A thread object to run a single directory read or search operation
// We emit a signal whenever we read a new slice of results.
class CDBrowseQO : public QThread {
    Q_OBJECT;

public:
    CDBrowseQO(UPnPClient::CDSH server, std::string objid,
               std::string ss = std::string(), QObject *parent = 0)
        : QThread(parent), m_serv(server), m_objid(objid), m_searchstr(ss),
          m_cancel(false) {
    }

    ~CDBrowseQO() {
    }

    virtual void run() {
        LOGDEB("CDBrowseQO::run. Search string: " << m_searchstr << "\n");
        int offset = 0;
        int toread = 100; // read small count the first time
        int total = 0;// Updated on first read.
        int count;

        while (total == 0 || (offset < total)) {
            if (m_cancel) {
                break;
            }
            UPnPClient::UPnPDirContent *slice = new UPnPClient::UPnPDirContent();
            if (slice == 0) {
                m_status = UPNP_E_OUTOF_MEMORY;
                emit done(m_status);
                return;
            }
            LOGDEB("dirreader: reading "<< toread << " total " << total << "\n");
            if (m_searchstr.empty()) {
                m_status = m_serv->readDirSlice(m_objid, offset, toread, *slice,  &count, &total);
            } else {
                m_status = m_serv->searchSlice(
                    m_objid, m_searchstr, offset, toread, *slice,  &count, &total);
            }
            if (m_cancel) {
                break;
            }
            if (m_status != UPNP_E_SUCCESS) {
                emit done(m_status);
                return;
            }
            offset += count;
            m_emitting = true;
            emit sliceAvailable(slice);
            m_emitting = false;
            if (count != toread || m_cancel)
                break;
            toread = m_serv->goodSliceSize();
        }
        emit done(m_status);
        m_status = UPNP_E_SUCCESS;
    }
    void setCancel() {
        m_cancel = true;
    }
    bool isEmitting() {
        return m_emitting;
    }
    const std::string& getObjid() {
        return m_objid;
    }
    const std::string& getModelName() {
        return m_serv->getModelName();
    }
    UPnPClient::ContentDirectory::ServiceKind getKind() {
        return m_serv->getKind();
    }

signals:
    void sliceAvailable(UPnPClient::UPnPDirContent *);
    void done(int);

private:
    UPnPClient::CDSH m_serv;
    std::string m_objid;
    std::string m_searchstr;
    // We use a list (vs vector) so that existing element addresses
    // are unchanged when we append
    int m_status;
    bool m_cancel;
    bool m_emitting{false};
};


// The content directory object is used to generate SystemUpdateId and
// ContainerUpdateIDs events in case we might care
class ContentDirectoryQO : public QObject, public UPnPClient::VarEventReporter {
    Q_OBJECT;

public:
    ContentDirectoryQO(UPnPClient::CDSH service, QObject *parent = 0)
        : QObject(parent), m_srv(service) {
        m_srv->installReporter(this);
    }
    virtual ~ContentDirectoryQO() {
        m_srv->installReporter(0);
    }

    // SystemUpdateId
    virtual void changed(const char *nm, int value) {
        LOGDEB1("CDQO: Changed: " << nm << " (int): " << value << "\n");
        if (!strcmp(nm, "SystemUpdateID")) {
            emit systemUpdateIDChanged(value);
        }
    }
    virtual void changed(const char *nm, const char *value) {
        LOGDEB1("CDQO: Changed: " << n << " (char*): " << v << "\n");
        if (!strcmp(nm, "ContainerUpdateIDs")) {
            emit containerUpdateIDsChanged(QString::fromUtf8(value));
        }
    }

    UPnPClient::CDSH srv() {return m_srv;}
    
signals:
    void systemUpdateIDChanged(int);
    void containerUpdateIDsChanged(QString);

private:
    UPnPClient::CDSH m_srv;
};

#endif /* _DIRREADER_H_INCLUDED_ */
