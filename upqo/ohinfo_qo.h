/* Copyright (C) 2014 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _OHINFO_QO_INCLUDED
#define _OHINFO_QO_INCLUDED

#include <string>

#include <QObject>
#include <QThread>

#include "libupnpp/log.h"
#include "libupnpp/control/ohinfo.hxx"
#include "ohpool.h"

class OHInfoQO : public QObject, public UPnPClient::VarEventReporter {
Q_OBJECT

public:
    OHInfoQO(UPnPClient::OHIFH ohp, QObject *parent = 0)
        : QObject(parent), m_srv(ohp) {
        m_srv->installReporter(this);
        qRegisterMetaType<UPnPClient::UPnPDirObject>("UPnPClient::UPnPDirObject");
    }

    virtual ~OHInfoQO() {
        udiscon();
    }
    virtual void udiscon() {
        if (m_srv)
            m_srv->installReporter(0);
    }

    virtual void changed(const char *nm, int value) override {
        Q_UNUSED(nm);Q_UNUSED(value);
        LOGDEB1("OHInfoQO: changed: " << nm << " (int): " << value << "\n");
    }
    virtual void changed(const char *nm, std::vector<int> ids) override{
        Q_UNUSED(nm); Q_UNUSED(ids);
        LOGDEB1("OHInfoQO: changed: " << nm << " (vector<int>)\n");
    }
    virtual void changed(const char * nm, const char *value) override{
        Q_UNUSED(nm);Q_UNUSED(value);
        LOGDEB1("OHInfoQO: changed: " << nm << " (char*): " << value << "\n");
    }
    virtual void changed(const char *nm, UPnPClient::UPnPDirObject dirent) override {
        Q_UNUSED(nm);
        LOGDEB1("OHInfoQO: changed: " << nm << " (UPnPDirObject)\n");
        emit metaChanged(dirent);
    };

    // Read state from the remote. Used when starting up, to avoid
    // having to wait for events.
    virtual bool getmetatext(UPnPClient::UPnPDirObject *dirent) {
        int ret = m_srv->metatext(dirent);
        LOGDEB1("OHInfoQO::metatext: ret " << ret << " data " << dirent->dump() << "\n");
        return ret == 0;
    }
public slots:

    virtual void fetchState() {
        UPnPClient::UPnPDirObject dirent;
        int ret = m_srv->metatext(&dirent);
        if (ret == 0) {
            emit metaChanged(dirent);
        }
    }
signals:
    void metaChanged(UPnPClient::UPnPDirObject);

private:
    UPnPClient::OHIFH m_srv;
};

#endif // _OHINFO_QO_INCLUDED
