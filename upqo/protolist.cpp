/* Copyright (C) 2024 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include "protolist.h"

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>

#include "libupnpp/log.h"

// Common variations on the MIME types. Not very elegant to repeat the permutations but much simpler
static const std::map<std::string, std::vector<std::string>> expansions {
    {"audio/flac", {"audio/x-flac", "application/flac", "application/x-flac"}},
    {"audio/x-flac", {"audio/flac", "application/flac", "application/x-flac"}},
    {"application/flac", {"audio/x-flac", "audio/flac", "application/x-flac"}},
    {"application/x-flac", {"audio/x-flac", "application/flac", "audio/flac"}},
    {"audio/dff", {"audio/x-dff"}},
    {"audio/x-dff", {"audio/dff"}},
    {"audio/dsd", {"audio/x-dsd"}},
    {"audio/x-dsd", {"audio/dsd"}},
    {"audio/dsf", {"audio/x-dsf"}},
    {"audio/x-dsf", {"audio/dsf"}},
    {"audio/wav", {"audio/x-wav", "audio/wave"}},
    {"audio/x-wav", {"audio/wav", "audio/wave"}},
    {"audio/wave", {"audio/x-wav", "audio/wav"}},
    {"audio/x-aiff", {"audio/aif", "audio/aiff"}},
    {"audio/aiff", {"audio/aif", "audio/x-aiff"}},
    {"audio/aif", {"audio/aiff", "audio/x-aiff"}},
    {"audio/ogg", {"audio/x-ogg"}},
    {"audio/x-ogg", {"audio/ogg"}},
    {"audio/x-vorbis+ogg", {"audio/x-vorbis", "audio/vorbis"}},
    {"audio/x-vorbis", {"audio/x-vorbis+ogg", "audio/vorbis"}},
    {"audio/vorbis", {"audio/x-vorbis", "audio/x-vorbis+ogg"}},
};

static std::string protomimes(const std::vector<UPnPP::ProtocolinfoEntry> &protoinfo)
{
    std::string out;
    for (const auto& e : protoinfo) {
        out += e.contentFormat + " ";
    }
    return out;
}

// Mostly recreate the original format (for debug and trace)
static std::string protoinfoAsStr(const std::vector<UPnPP::ProtocolinfoEntry> &protoinfo)
{
    std::string out;
    for (const auto& e : protoinfo) {
        out += e.protocol + ':' + e.network + ':' + e.contentFormat;
        if (!e.content_params.empty()) {
            out += ';';
            for (const auto& [n,v] : e.content_params) {
                out += n + '=' + v + ';';
            }
            out.pop_back();
        }
        out += ':' + e.additional + ",\n";
    }
    return out;
}

void tweakAndSortProtocolinfo(std::vector<UPnPP::ProtocolinfoEntry> &protoinfo)
{
    LOGDEB0("tweakAndSortProtocolinfo: in: " << protoinfoAsStr(protoinfo) << "\n");
    // Add some common variations of the MIME types
    std::vector<UPnPP::ProtocolinfoEntry> addentries;
    for (const auto& entry: protoinfo) {
        auto it = expansions.find(entry.contentFormat);
        if (it != expansions.end()) {
            for (const auto& exp : it->second) {
                UPnPP::ProtocolinfoEntry entry1(entry);
                entry1.contentFormat = exp;
                addentries.push_back(entry1);
            }
        }
    }
    protoinfo.insert(protoinfo.end(), addentries.begin(), addentries.end());
    std::sort(protoinfo.begin(), protoinfo.end(),
              [] (const UPnPP::ProtocolinfoEntry&a,
                  const UPnPP::ProtocolinfoEntry&b)->bool {
                  return a.contentFormat < b.contentFormat;
              });
    auto uit = std::unique(protoinfo.begin(), protoinfo.end(),
                           [] (const UPnPP::ProtocolinfoEntry&a,
                               const UPnPP::ProtocolinfoEntry&b)->bool {
                               return a.protocol == b.protocol && a.network == b.network &&
                                   a.contentFormat == b.contentFormat &&
                                   a.content_params == b.content_params &&
                                   a.additional == b.additional;
                           });
    protoinfo.resize(uit - protoinfo.begin());
    LOGDEB0("tweakAndSortProtocolinfo: out [" << protomimes(protoinfo) << "]\n");
}
