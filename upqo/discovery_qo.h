/* Copyright (C) 2020 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _DISCOVERY_QO_INCLUDED
#define _DISCOVERY_QO_INCLUDED

#include <iostream>
#include <string>
#include <functional>
using namespace std::placeholders;

#include <QObject>
#include <QThread>

#include "libupnpp/log.h"
#include "libupnpp/control/discovery.hxx"
#include "libupnpp/control/description.hxx"
#include "libupnpp/control/renderingcontrol.hxx"
#include "libupnpp/control/ohproduct.hxx"
using namespace UPnPClient;

class DiscoveryQO : public QObject {
Q_OBJECT;

public:
    DiscoveryQO(QObject *parent = 0)
        : QObject(parent) {
    }

    virtual ~DiscoveryQO() {
        UPnPDeviceDirectory::delCallback(m_idx);
    }
    void activate() {
                m_idx = UPnPDeviceDirectory::addCallback(
            std::bind(&DiscoveryQO::onDiscovered, this, _1, _2));
    }
    bool onDiscovered(const UPnPDeviceDesc& dev, const UPnPServiceDesc& srv) {
        if (RenderingControl::isRDCService(srv.serviceType) ||
            OHProduct::isOHPrService(srv.serviceType)) {
            emit sig_discovered(dev);
        }
        return true;
    }

signals:
    void sig_discovered(UPnPClient::UPnPDeviceDesc);
                                         
private:
    int m_idx{-1};
};

Q_DECLARE_METATYPE(UPnPClient::UPnPDeviceDesc)

#endif // _DISCOVERY_QO_INCLUDED
