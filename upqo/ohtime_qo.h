/* Copyright (C) 2014 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _OHTIME_QO_INCLUDED
#define _OHTIME_QO_INCLUDED

#include <string>

#include <QObject>
#include <QThread>

#include "libupnpp/control/ohtime.hxx"
#include "libupnpp/log.h"

class OHTimeQO : public QObject, public UPnPClient::VarEventReporter {
Q_OBJECT

public:
    OHTimeQO(UPnPClient::OHTMH ohp, QObject *parent = 0)
        : QObject(parent), m_srv(ohp)
    {
        LOGDEB1("OHTimeQO: install reporter\n");
        m_srv->installReporter(this);
    }

    virtual ~OHTimeQO() {
        udiscon();
    }
    virtual void udiscon() {
        if (m_srv)
            m_srv->installReporter(0);
    }

    // TrackCount, Duration and Seconds
    virtual void changed(const char *nm, int value)
    {
        LOGDEB1("OHTM: Changed: " << nm << " (int): " << value << "\n");
        if (!strcmp(nm, "TrackCount")) {
            emit trackCountChanged(value);
        } else if (!strcmp(nm, "Duration")) {
            emit durationChanged(value);
        } else if (!strcmp(nm, "Seconds")) {
            emit secsInSongChanged(value);
        }
    }
    virtual void changed(const char *nm, const char *val) {
        Q_UNUSED(nm);
        Q_UNUSED(val);
        LOGDEB1("OHTM: Changed: " << nm << " (char*): " << val << "\n");
    }

public slots:
    virtual bool time(UPnPClient::OHTime::Time& tm) {
        return m_srv->time(tm) == 0;
    }

signals:
    void trackCountChanged(int);
    void durationChanged(int);
    void secsInSongChanged(quint32);
                                         
private:
    UPnPClient::OHTMH m_srv;
};

#endif // _OHTIME_QO_INCLUDED
