/* Copyright (C) 2015 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "ohpool.h"

#include <unordered_set>
#include <unordered_map>
#include <vector>


#include "libupnpp/control/ohplaylist.hxx"
#include "libupnpp/control/ohradio.hxx"
#include "libupnpp/log.h"

template <class T>
bool ohupdmetapool(const std::vector<int>& nids, int curid,
                   std::unordered_map<int, UPnPClient::UPnPDirObject>& metapool, T srv)
{
    // Clean up metapool entries not in ids. We build a set with
    // the new ids list first. For small lists it does not matter,
    // for big ones, this will prevent what would otherwise be a
    // linear search the repeated search to make this
    // quadratic. We're sort of O(n * log(n)) instead.
    if (!metapool.empty() && !nids.empty()){
        std::unordered_set<int> tmpset(nids.begin(), nids.end());
        for (auto it = metapool.begin(); it != metapool.end();) {
            if (tmpset.find(it->first) == tmpset.end()) {
                it = metapool.erase(it);
            } else {
                it++;
            }
        }
    }

    // Find ids for which we have no metadata. Always re-read current title
    std::vector<int> unids; // unknown
    for (auto id : nids) {
        if (metapool.find(id) == metapool.end() || curid == id)
            unids.push_back(id);
    }
    if (!unids.empty()) {
        LOGDEB1("OHPL::onIdArrayChanged: need metadata for: " << ivtos(unids) << "\n") ;
    }
    // Fetch needed metadata, 10 entries at a time
    const unsigned int batchsize(10);
    for (unsigned int i = 0; i < unids.size();) {
        unsigned int j = 0;
        std::vector<int> metaslice;
        for (; j < batchsize && (i+j) < unids.size(); j++) {
            metaslice.push_back(unids[i+j]);
        }

        LOGDEB1("OHPL::onIdArrayChanged: Requesting metadata for " << vtos(metaslice) << "\n");
        std::vector<UPnPClient::OHPlaylist::TrackListEntry> entries;
        int ret;
        if ((ret = srv->readList(metaslice, &entries))) {
            LOGERR("OHPL: readList failed: " << ret << "\n");
            return false;
        }
        for (auto& entry : entries) {
            // Kazoo for example does not set a resource (uri) inside the dirent. Set it from the
            // uri field in this case.
            if (entry.dirent.m_resources.empty()) {
                UPnPClient::UPnPResource res;
                res.m_uri = entry.url;
                entry.dirent.m_resources.push_back(res);
            }
            metapool[entry.id] = entry.dirent;
        }
        i += j;
    }

#if 0
    std::cerr << "Metadata Pool now: \n";
    for (const auto& meta : metapool) {
        std::cerr << "Id " << meta.first << "->\n" << meta.second.dump().c_str();
    }
#endif
    return true;
}

template bool ohupdmetapool<UPnPClient::OHPLH>
(const std::vector<int>&, int, std::unordered_map<int, UPnPClient::UPnPDirObject>& , UPnPClient::OHPLH);

template bool ohupdmetapool<UPnPClient::OHRDH>
(const std::vector<int>&, int, std::unordered_map<int, UPnPClient::UPnPDirObject>&, UPnPClient::OHRDH);
