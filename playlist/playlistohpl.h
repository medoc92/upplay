/* Copyright (C) 2011  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLAYLISTOHPL_H_
#define PLAYLISTOHPL_H_

#include <vector>
#include <memory>

#include "HelperStructs/MetaData.h"
#include "playlist.h"

#include <libupnpp/upnpavutils.hxx>

class OHTimeQO;
class OHPlayer;

class PlaylistOHPL : public Playlist {
    Q_OBJECT

public:
    PlaylistOHPL(OHPlayer *ohpl, OHTimeQO *ohtm, QObject *parent=0);
    virtual void update_state() override;
    virtual void getprotoinfo(std::vector<UPnPP::ProtocolinfoEntry>&) override;
    virtual std::string getFriendlyName() override;
    
signals:
    // All our signals are connected to the OHPlaylist object
    void sig_clear_playlist();
    void sig_insert_tracks(const MetaDataList&, int);
    void sig_seek(int secs);
    void sig_seekIndex(int);
    void sig_tracks_removed(const QList<int>& rows);
    void sig_fetchstate();
    void sig_songsecs(int);

public slots:

    // These receives changes from the remote state.
    void onRemoteCurrentTrackid(int id);
    void onRemoteTpState_impl(AudioState, const char *) override;
    void onRemoteSecsInSong_impl(quint32 s) override;
    
    // The following are connected to GUI signals, for responding to user actions.
    void psl_change_track_impl(int idx) override;
    void psl_clear_playlist_impl() override;
    void psl_play() override;
    void psl_pause_impl() override;
    void psl_stop() override;
    void psl_forward() override;
    void psl_backward() override;
    void psl_remove_rows_impl(const QList<int>& rows, bool) override;
    // Insert after idx. Use -1 to insert at start
    void psl_insert_tracks(const MetaDataList&, int) override;

    // Process playlist data from device
    void onRemoteMetaArray(const MetaDataList&);

    void psl_seek(int) override;

private:
    // My link to the OpenHome Renderer
    OHPlayer *m_ohplo{nullptr};
    // We use this to retrieve the duration if not set in the didl frag
    OHTimeQO *m_ohtmo{nullptr};
    // Position in current song, 0 if unknown
    quint32 m_cursongsecs;
    // Current song is last in playlist
    bool m_lastsong;
    // Playing the last 5 S of last song
    bool m_closetoend;
    // We store this on initialization as it's not going to change.
    std::vector<UPnPP::ProtocolinfoEntry> m_protoinfo;
    
    void resetPosState() {
        m_cursongsecs = 0;
        m_lastsong = m_closetoend = false;
    }
    void maybeSetDuration(bool);
};

#endif /* PLAYLISTOHPL_H_ */
