/* Copyright (C) 2014 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <libupnpp/log.hxx>

#include "playlistohpl.h"
#include "upadapt/ohpladapt.h"
#include "upqo/ohtime_qo.h"

using namespace UPnPP;

PlaylistOHPL::PlaylistOHPL(OHPlayer *ohpl, OHTimeQO *ohtm, QObject *parent)
    : Playlist(parent), m_ohplo(ohpl), m_ohtmo(ohtm),
      m_cursongsecs(0), m_lastsong(false), m_closetoend(false)
{
    m_protoinfo = m_ohplo->getprotoinfo();
    LOGDEB("PlaylistOHPL: got " << m_protoinfo.size() << " protocol info entries\n");
    
    // Connections from OpenHome renderer to local playlist
    connect(m_ohplo, SIGNAL(metadataArrayChanged(const MetaDataList&)),
            this, SLOT(onRemoteMetaArray(const MetaDataList&)));
    connect(m_ohplo, SIGNAL(currentTrackId(int)), this, SLOT(onRemoteCurrentTrackid(int)));
    connect(m_ohplo, SIGNAL(audioStateChanged(AudioState, const char *)),
            this, SLOT(onRemoteTpState(AudioState, const char *)));
    connect(m_ohplo, SIGNAL(connectionLost()), this, SIGNAL(connectionLost()));
    connect(m_ohplo, SIGNAL(renewSubscriptions()), this, SIGNAL(renewSubscriptions()));
    connect(m_ohplo, SIGNAL(playlistModeChanged(int)), this, SIGNAL(playlistModeChanged(int)));
    
    // Connections from local playlist to openhome
    connect(this, SIGNAL(sig_clear_playlist()), m_ohplo, SLOT(clear()));
    connect(this, SIGNAL(sig_insert_tracks(const MetaDataList&, int)),
            m_ohplo, SLOT(insertTracks(const MetaDataList&, int)));
    connect(this, SIGNAL(sig_tracks_removed(const QList<int>&)),
            m_ohplo, SLOT(removeTracks(const QList<int>&)));
    connect(this, SIGNAL(sig_mode_changed(int)), m_ohplo, SLOT(changeMode(int)));
    connect(this, SIGNAL(sig_sync()), m_ohplo, SLOT(sync()));
    connect(this, SIGNAL(sig_pause()), m_ohplo, SLOT(pause()));
    connect(this, SIGNAL(sig_stop()),  m_ohplo, SLOT(stop()));
    connect(this, SIGNAL(sig_resume_play()), m_ohplo, SLOT(play()));
    connect(this, SIGNAL(sig_forward()), m_ohplo, SLOT(next()));
    connect(this, SIGNAL(sig_backward()), m_ohplo, SLOT(previous()));
    connect(this, SIGNAL(sig_fetchstate()), m_ohplo, SLOT(fetchState()));
    connect(this, SIGNAL(sig_seek(int)), m_ohplo, SLOT(seek(int)));
    connect(this, SIGNAL(sig_seekIndex(int)), m_ohplo, SLOT(seekIndex(int)));
    connect(this, SIGNAL(sig_songsecs(int)), m_ohplo, SLOT(setSongSecs(int)));
}

void PlaylistOHPL::update_state()
{
    emit sig_fetchstate();
}

static bool samelist(const MetaDataList& mdv1, const MetaDataList& mdv2)
{
    if (mdv1.size() != mdv2.size())
        return false;
    for (int i = 0; i < int(mdv1.size()); i++) {
        if (mdv1[i].url.compare(mdv2[i].url) ||
            mdv1[i].id != mdv2[i].id)
            return false;
    }
    return true;
}

std::string PlaylistOHPL::getFriendlyName()
{
    return m_ohplo->getFriendlyName();
}

void PlaylistOHPL::getprotoinfo(std::vector<UPnPP::ProtocolinfoEntry>& pti)
{
    pti.clear();
    pti = m_protoinfo;
}

void PlaylistOHPL::onRemoteMetaArray(const MetaDataList& mdv)
{
    LOGDEB0("PlaylistOHPL::onRemoteMetaArray: " << mdv.size() << " entries\n");
    if (mdv.empty() || !samelist(mdv, m_meta)) {
        m_meta = mdv;

        emit sig_playlist_updated(m_meta, m_play_idx, 0);
    }
}

void PlaylistOHPL::psl_seek(int secs)
{
    emit sig_seek(secs);
}

void PlaylistOHPL::maybeSetDuration(bool needsig)
{
    if (m_play_idx < 0 || m_play_idx >= int(m_meta.size()) || !m_ohtmo) {
        return;
    }
    MetaData& meta(m_meta[m_play_idx]);
    if (meta.length_ms <= 0) {
        UPnPClient::OHTime::Time tm;
        if (m_ohtmo->time(tm)) {
            meta.length_ms = tm.duration * 1000;
            if (needsig) {
                emit sig_track_metadata(meta);
            }
        }
    }
    // Set the songsec every time, it's cheap and it makes
    // things work when the duration is not in the didl (else
    // there are order of events issues which result in unset
    // songsecs in ohpladapt
    emit sig_songsecs(meta.length_ms / 1000);
}

void PlaylistOHPL::onRemoteCurrentTrackid(int id)
{
    LOGDEB0("PlaylistOHPL::onRemoteCurrentTrackid: " << id << "\n");

    if (id <= 0 || m_meta.size() == 0) {
        return;
    } 

    for (auto it = m_meta.begin(); it != m_meta.end(); it++) {
        if (it->id == id) {
            if (m_play_idx != it - m_meta.begin()) {
                m_play_idx = it - m_meta.begin();
                if (m_play_idx == int(m_meta.size()) - 1) {
                    m_lastsong = true;
                }
                LOGDEB1(" new track index " << m_play_idx << "\n");
            }
            maybeSetDuration(false);

            // Emit the current index in any case to let the playlist
            // UI scroll to show the currently playing track (some
            // time after a user interaction scrolled it off)
            emit sig_playing_track_changed(m_play_idx);
            // If the track id change was caused by the currently
            // playing track having been removed, the play index did
            // not change but the metadata did, so emit metadata in
            // all cases.
            emit sig_track_metadata(*it);
            m_cursongsecs = it->length_ms / 1000;
            return;
        }
    }
    resetPosState();
    LOGINF("PlaylistOHPL::onRemoteCurrentTrackid: track " << id << " not found in array (size " <<
           m_meta.size() << ")\n");
}

void PlaylistOHPL::onRemoteSecsInSong_impl(quint32 secs)
{
    if (m_lastsong && m_cursongsecs > 0 && int(secs) > int(m_cursongsecs) - 3) {
        LOGDEB1("PlaylistOHPL::onRemoteSecsInSong_impl: close to end\n");
        m_closetoend = true;
    }
    maybeSetDuration(true);
}

void PlaylistOHPL::onRemoteTpState_impl(AudioState, const char *)
{
    LOGDEB1("PlaylistOHPL::onRemoteTpState_impl: " << sst << "\n");
    if (m_tpstate == AUDIO_STOPPED && m_closetoend == true) {
        resetPosState();
        emit sig_playlist_done();
    }
}

void PlaylistOHPL::psl_clear_playlist_impl()
{
    // Tell the Open Home Playlist to do it.
    emit sig_clear_playlist();
    // If we're playing, signal playlist done (this is to inform the
    // "random play by group" thing to start next group
    emit sig_playlist_done();
}

void PlaylistOHPL::psl_change_track_impl(int idx) {
    emit sig_seekIndex(idx);
}

void PlaylistOHPL::psl_play() 
{
    if (m_tpstate ==  AUDIO_STOPPED && valid_row(get_selection_min_row())) {
        emit sig_seekIndex(get_selection_min_row());
    } else {
        emit sig_resume_play();
    }
}

void PlaylistOHPL::psl_pause_impl() 
{
    emit sig_pause();
}

void PlaylistOHPL::psl_stop() 
{
    emit sig_stop();
}

void PlaylistOHPL::psl_forward() 
{
    emit sig_forward();
}

void PlaylistOHPL::psl_backward() 
{
    emit sig_backward();
}

void PlaylistOHPL::psl_insert_tracks(const MetaDataList& meta, int afteridx)
{
    LOGDEB0("PlaylistOHPL::psl_insert_tracks ntracks " << meta.size() <<
            " afteridx" << afteridx << "\n");
    emit sig_insert_tracks(meta, afteridx);
}

void PlaylistOHPL::psl_remove_rows_impl(const QList<int>& rows, bool)
{
    if (rows.contains(m_play_idx)) {
        m_play_idx = -1;
    }
    emit sig_tracks_removed(rows);
}
