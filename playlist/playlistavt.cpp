/* Copyright (C) 2011  Lucio Carreras
 * Copyright (C) 2012-2022 J.F. Dockes
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <ctime>

#include <QFile>
#include <QList>
#include <QObject>
#include <QDate>
#include <QTime>
#include <QDir>
#include <QTimer>

#include <libupnpp/upnpplib.hxx>
#include <libupnpp/control/avtransport.hxx>
#include <libupnpp/log.hxx>

#include "playlistavt.h"
#include "HelperStructs/Helper.h"
#include "upadapt/upputils.h"

PlaylistAVT::PlaylistAVT(AVTPlayer *avtp, const std::string& _udn, QObject *parent)
    : Playlist(parent), m_avto(avtp)
{
    std::string sqid(_udn);
    if (sqid.find("uuid:") == 0) {
        sqid = sqid.substr(5);
    }
    if (sqid.empty())
        sqid = "savedQueue";
    sqid = sqid.insert(0, "sq-");
    m_savefile = qs2utf8s(QDir(Helper::getHomeDataPath()).filePath(u8s2qs(sqid)));

    mdlUnSerialize(m_meta, u8s2qs(m_savefile));

    connect(m_avto, SIGNAL(newTrackPlaying(const QString&)),
            this, SLOT(onExtTrackChange(const QString&)));
    connect(m_avto, SIGNAL(sig_currentMetadata(const MetaData&)),
            this, SLOT(onCurrentMetadata(const MetaData&)));
    connect(m_avto, SIGNAL(audioStateChanged(AudioState, const char*)),
            this, SLOT(onRemoteTpState(AudioState, const char *)));
    connect(m_avto, SIGNAL(stoppedAtEOT()), this,  SLOT(psl_forward()));
    connect(m_avto,  SIGNAL(connectionLost()), this, SIGNAL(connectionLost()));

    connect(this, SIGNAL(sig_stop()),  m_avto, SLOT(stop()));
    connect(this, SIGNAL(sig_resume_play()), m_avto, SLOT(play()));
    connect(this, SIGNAL(sig_pause()), m_avto, SLOT(pause()));

    QTimer::singleShot(0, m_avto, SLOT(fetchState()));
    QTimer::singleShot(0, this, SLOT(playlist_updated()));
}

void PlaylistAVT::getprotoinfo(std::vector<UPnPP::ProtocolinfoEntry>& pti)
{
    pti.clear();
    pti = m_avto->getprotoinfo();
}

std::string PlaylistAVT::getFriendlyName() {
    return m_avto->getFriendlyName();
}

void PlaylistAVT::set_for_playing(int row)
{
    LOGDEB("PlaylistAVT::set_for_playing " << row << "\n");
    if (!valid_row(row)) {
        m_play_idx = -1;
        mdlSetCurPlayTrack(m_meta, -1);
        return;
    }

    m_play_idx = row;
    mdlSetCurPlayTrack(m_meta, row);
    m_meta[row].shuffle_played = true;

    m_playjuststarted = true;
    m_avto->changeTrack(m_meta[row], 0, true);
    emit sig_playing_track_changed(row);
    emit sig_track_metadata(m_meta[row]);
    prepare_next_track();
}

// Player switched tracks under us. Hopefully the uri matches another
// track.  We first look ahead, because the normal situation is that
// the device switched to the nextURI track
void PlaylistAVT::onExtTrackChange(const QString& uri)
{
    LOGDEB("PlaylistAVT::onExtTrackChange: playlist size " << m_meta.size() <<
           " idx " << m_play_idx << " uri " << qs2utf8s(uri) <<"\n");

    if (m_play_idx < -1) {
        // ??
        LOGERR("PlaylistAVT::onExtTrackChange: bad m_play_idx " << m_play_idx << "\n");
        return;
    }

    if (m_meta.empty())
        return;

   
    if (m_play_idx == -1) {
        // Just started
        m_play_idx = 0;
    }
    
    // Look for the uri in tracks following this one, then preceding
    int ifound = -1;
    int startatnext = m_playjuststarted ? 0 : 1;
    m_playjuststarted = false;
    for (int i = m_play_idx + startatnext; i < int(m_meta.size()); i++) {
        LOGDEB0("PlaylistAVT::onExtTrackChange: idx " << i << " comparing:\n" <<
                qs2utf8s(uri) << "\n" << qs2utf8s(m_meta[i].url) << "\n");
        if (!uri.compare(m_meta[i].url)) {
            ifound = i;
            break;
        }
    }
    if (ifound == -1) {
        for (int i = 0; i <= m_play_idx && i < int(m_meta.size()); i++) {
            LOGDEB0("PlaylistAVT::onExtTrackChange: idx " << i << " comparing\n" <<
                    qs2utf8s(uri) << "\n" <<
                    qs2utf8s(m_meta[i].url) << "\n");
            if (!uri.compare(m_meta[i].url)) {
                ifound = i;
                break;
            }
        }
    }

    LOGDEB0("PlaylistAVT::onExtTrackChange: ifound " << ifound << "\n");
    if (valid_row(ifound)) {
        m_play_idx = ifound;
        mdlSetCurPlayTrack(m_meta, ifound);
        emit sig_playing_track_changed(ifound);
        emit sig_track_metadata(m_meta[ifound]);
        prepare_next_track();
    }
}

void PlaylistAVT::maybeSetDuration(bool needsig)
{
    if (m_play_idx< 0 || m_play_idx >= int(m_meta.size())) {
        return;
    }
    MetaData& meta(m_meta[m_play_idx]);
    if (meta.length_ms <= 0) {
        int secs = m_avto->trackSecs();
        if (secs > 0) {
            meta.length_ms = secs * 1000;
            if (needsig) {
                emit sig_track_metadata(meta);
            }
        }
    }
}

void PlaylistAVT::onRemoteSecsInSong_impl(quint32)
{
    maybeSetDuration(true);
}

void PlaylistAVT::psl_seek(int secs)
{
    m_avto->seek(secs);
}

void PlaylistAVT::onCurrentMetadata(const MetaData& md)
{
    LOGDEB("PlaylistAVT::onCurrentMetadata: uri [" << qs2utf8s(md.url) << "]\n");
    if (md.url.isEmpty())
        return;
    MetaData *localmeta = 0;
    if (!mdlContains(m_meta, md, true, &localmeta)) {
        m_meta.push_back(md);
        m_play_idx = m_meta.size() -1;
        playlist_updated();
    }
    maybeSetDuration(false);
    bool preferlocal = true;
    if (localmeta && preferlocal) {
        emit sig_track_metadata(*localmeta);
    } else {
        emit sig_track_metadata(md);
    }
}

void PlaylistAVT::prepare_next_track()
{
    if (!valid_row(m_play_idx) || m_play_idx >= int(m_meta.size()) - 1)
        return;

    // Do not do this in shuffle mode: makes no sense
    if (CSettingsStorage::getInstance()->getPlaylistMode() & PLM_shuffle) 
        return;

    // Can be disabled in prefs
    QSettings settings;
    if (settings.value("noavtsetnext").toBool()) {
        LOGDEB("PlaylistAVT::prepare_next_track: not using AVT gapless (disabled in prefs)\n");
        return;
    }
    LOGDEB("PlaylistAVT::prepare_next: prepare " << m_play_idx + 1 << "\n");
    m_avto->infoNextTrack(m_meta[m_play_idx + 1]);
}

// fwd was pressed -> next track
void PlaylistAVT::psl_forward()
{
    LOGDEB("PlaylistAVT::psl_forward()\n");

    int track_num = -1;
    if(m_meta.empty()) {
        LOGDEB("PlaylistAVT::psl_forward(): empty playlist\n");
        goto out;
    }

    if (CSettingsStorage::getInstance()->getPlaylistMode() & PLM_shuffle) {
        // Build an array on unplayed tracks
        std::vector<unsigned int> unplayed;
        for (int i = 0; i < int(m_meta.size()); i++) {
            if (!m_meta[i].shuffle_played)
                unplayed.push_back(i);
        }
        if (unplayed.empty())
            goto out;

        track_num = unplayed[rand() % unplayed.size()];
    } else {
        if (m_play_idx >= int(m_meta.size()) -1) {
            // last track
            LOGDEB("PlaylistAVT::psl_forward(): was last, stop or loop\n");
            if (CSettingsStorage::getInstance()->getPlaylistMode() & PLM_repAll) {
                track_num = 0;
            }
        } else {
            track_num = m_play_idx + 1;
        }
    }

out:
    LOGDEB("PlaylistAVT::psl_forward(): new tnum " << track_num << "\n");
    if (track_num >= 0) {
        // valid next track
        set_for_playing(track_num);
    } else {
        set_for_playing(-1);
        emit sig_stop();
        emit sig_playlist_done();
        return;
    }
}

void PlaylistAVT::psl_clear_playlist_impl()
{
    emit sig_stop();
    millisleep(100);
    playlist_updated();
    emit sig_playlist_done();
}

void PlaylistAVT::psl_play()
{
    if (m_meta.empty()) {
        return;
    }

    if (m_play_idx < 0) {
        if (valid_row(get_selection_min_row())) {
            set_for_playing(get_selection_min_row());
        } else {
            set_for_playing(0);
        }
    } else {
        emit sig_resume_play();
    }
}

void PlaylistAVT::psl_pause_impl()
{
    emit sig_pause();
}

void PlaylistAVT::psl_stop()
{
    set_for_playing(-1);
    emit sig_stop();
    playlist_updated();
}

// GUI -->
void PlaylistAVT::psl_backward()
{
    if (m_play_idx > 0) {
        set_for_playing(m_play_idx - 1);
    }
}

// GUI -->
void PlaylistAVT::psl_change_track_impl(int track_num)
{
    if (valid_row(track_num)) {
        set_for_playing(track_num);
    }
}

// insert tracks after idx which may be -1
void PlaylistAVT::psl_insert_tracks(const MetaDataList& nmeta, int row)
{
    // Change to more conventional "insert before"
    ++row;

    LOGDEB("PlaylistAVT::psl_insert_tracks: cur size " << m_meta.size() <<
           ". " << nmeta.size() << " rows before row " << row << "\n");
    if (m_meta.empty()) {
        if (row != 0) {
            LOGINF("PlaylistAVT::psl_insert_tracks: insert row " << row << " on empty playlist\n");
            return;
        }
        m_meta.insert(m_meta.end(), nmeta.begin(), nmeta.end());
    } else {
        if (row < 0 || row > int(m_meta.size())) {
            LOGDEB("PlaylistAVT::psl_insert_tr.: bad row "<<row<<" plsize "<<m_meta.size()<<"\n");
            return;
        }
        m_meta.insert(m_meta.begin() + row, nmeta.begin(), nmeta.end());
    }

    // Find the playing track (could be part of nmeta if this is a d&d cut/paste) and adjust
    // m_play_idx
    m_play_idx = -1;
    for (auto it = m_meta.begin(); it != m_meta.end(); it++) {
        if (it->pl_playing && m_play_idx == -1) {
            m_play_idx = it - m_meta.begin();
        } else {
            it->pl_playing = false;
        }
        // Not sure this is necessary, but looks reasonable
        it->shuffle_played = false;
    }

    prepare_next_track();
    playlist_updated();
}

void PlaylistAVT::playlist_updated()
{
    emit sig_playlist_updated(m_meta, m_play_idx, 0);
    std::string tmp(m_savefile + "-");
    mdlSerialize(m_meta, u8s2qs(tmp));
#ifdef _WIN32
    // Windows refuses to rename to an existing file. It appears that
    // there are workarounds (See MoveFile, MoveFileTransacted), but
    // the only consequence of non-atomicity here is that we may lose
    // the current playlist in the extraordinary case that we fail
    // after the unlink, no big deal.
    unlink(m_savefile.c_str());
#endif
    if (rename(tmp.c_str(),  m_savefile.c_str())) {
        LOGSYSERR("PlaylistAVT::playlist_updated", "rename", tmp + "->" + m_savefile);
    }
}

void PlaylistAVT::psl_remove_rows_impl(const QList<int>& rows, bool is_drag_drop)
{
    LOGDEB("PlaylistAVT::psl_remove_rows: m_meta.size(): " << m_meta.size() << " nrows " <<
           rows.size() << " dragdrop " <<  is_drag_drop << "\n");

    if (rows.empty()) {
        return;
    }

    // Remember the position of the first removed row. If is_drag_drop is false we will select the
    // row which ends up in this position. This allows removing rows by selecting one and repeatedly
    // typing delete.
    int first_row = rows[0];

    // Build a temporary list with the tracks not in the delete list. 
    MetaDataList v_tmp_md;
    for (int i = 0; i < int(m_meta.size()); i++) {
        if (!rows.contains(i)) {
            MetaData md = m_meta[i];
            md.pl_dragged = false;
            md.pl_selected = false;
            md.pl_playing = (i == m_play_idx);
            v_tmp_md.push_back(md);
        }
    }

    m_meta = v_tmp_md;
    if (m_meta.empty()) {
        psl_clear_playlist();
        return;
    }
    m_play_idx = -1;
    for (int i = 0; i < int(m_meta.size()); i++) {
        if (m_meta[i].pl_playing)
            m_play_idx = i;
    }
    if (!is_drag_drop && first_row < int(m_meta.size())) {
        m_meta[first_row].pl_selected = true;
    }

    if (m_play_idx == -1) {
        psl_stop();
    }
    playlist_updated();
}
