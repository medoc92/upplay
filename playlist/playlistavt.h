/* Copyright (C) 2011  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLAYLISTAVT_H_
#define PLAYLISTAVT_H_

#include <iostream>

#include <QObject>
#include <QList>
#include <QMap>
#include <QStringList>

#include "HelperStructs/MetaData.h"
#include "HelperStructs/globals.h"
#include "HelperStructs/CSettingsStorage.h"
#include "playlist.h"
#include "upadapt/avtadapt.h"

class PlaylistAVT : public Playlist {
    Q_OBJECT

public:
    PlaylistAVT(AVTPlayer *avtp, const std::string& UDN, QObject *parent = 0);
    virtual void getprotoinfo(std::vector<UPnPP::ProtocolinfoEntry>&) override;
    virtual std::string getFriendlyName() override;

public slots:
    // Slots connected to player events
    void onExtTrackChange(const QString& uri);
    void onCurrentMetadata(const MetaData&);
    void onRemoteSecsInSong_impl(quint32 s) override;

    // Insert after idx. Use -1 to insert at start
    void psl_insert_tracks(const MetaDataList&, int idx) override;

    void psl_change_track_impl(int) override;
    void psl_clear_playlist_impl() override;
    void psl_play() override;
    void psl_pause_impl() override;
    void psl_stop() override;
    void psl_forward() override;
    void psl_backward() override;
    void psl_remove_rows_impl(const QList<int> &, bool) override;
    void psl_seek(int) override;
    
protected:
    void set_for_playing(int row);

private slots:
    void playlist_updated();
private:
    AVTPlayer *m_avto{nullptr};
    std::string m_savefile;

    // This is used to signal that we just started play. The search triggered by the first event
    // signalling a track change should begin at the current track, not the next one. Else, if the
    // same track is found later in the playlist, we would switch the playlist position to the later
    // track.
    bool m_playjuststarted{false};
    
    void maybeSetDuration(bool);
    void prepare_next_track();
};

#endif /* PLAYLISTAVT_H_ */
