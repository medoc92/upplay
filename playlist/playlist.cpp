/* Copyright (C) 2011  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <sys/stat.h>
#include <sys/types.h>

#include <ctime>
#include <climits>

#include <QList>
#include <QDir>
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>

#include <libupnpp/upnpavutils.hxx>
#include <libupnpp/log.hxx>

#include "upadapt/upputils.h"
#include "playlist.h"
#include "HelperStructs/Helper.h"


void Playlist::getprotoinfo(std::vector<UPnPP::ProtocolinfoEntry>& entries)
{
    entries.clear();
}

// GUI -->
void Playlist::psl_clear_playlist()
{
    m_meta.clear();
    m_play_idx = -1;
    m_selection_min_row = -1;
    m_insertion_point = -1;
    MetaData md;
    emit sig_track_metadata(md);
    psl_clear_playlist_impl();
}

// Remove one row
void Playlist::remove_row(int row)
{
    LOGDEB2("Playlist::remove_row\n");
    QList<int> remove_list;
    remove_list << row;
    psl_remove_rows(remove_list, false);
}

void Playlist::psl_remove_rows(const QList<int> &lst, bool is_drag_drop)
{
    int decr = 0;
    for (int i = 0; i < lst.size(); i++) {
        if (lst[i] <= m_insertion_point + 1) {
            ++decr;
        }
    }
    m_insertion_point -= decr;
    if (m_insertion_point < -1)
        m_insertion_point = -1;
    psl_remove_rows_impl(lst, is_drag_drop);
}

void Playlist::onRemoteTpState(AudioState tps, const char *sst)
{
#if 0
    LOGDEB("Playlist::onRemoteTpState " << s << " play_idx " << m_play_idx << "\n");
    if (m_play_idx >= 0 && m_play_idx < int(m_meta.size())) 
        LOGDEB("     meta[idx].pl_playing " << m_meta[m_play_idx].pl_playing << "\n");
#endif
    
    m_tpstate = tps;
    switch (tps) {
    case AUDIO_UNKNOWN:
    case AUDIO_STOPPED:
    default:
        LOGDEB2("Playlist::onRemoteTpState: STOPPED\n");
        emit sig_stopped();
        break;
    case AUDIO_PLAYING:
        LOGDEB2("Playlist::onRemoteTpState: PLAYING\n");
        emit sig_playing();
        break;
    case AUDIO_PAUSED:
        LOGDEB2("Playlist::onRemoteTpState: PAUSED\n");
        emit sig_paused();
        break;
    }
    onRemoteTpState_impl(tps, sst);
}

void Playlist::onRemoteSecsInSong(quint32 s)
{
    if (m_play_idx >= 0 && m_play_idx < int(m_meta.size())) {
        m_meta[m_play_idx].curseeksecs = s;
    }
    onRemoteSecsInSong_impl(s);
}

void Playlist::psl_pause()
{
    if (m_play_idx >= 0 && m_play_idx < int(m_meta.size())) {
        m_meta[m_play_idx].pauseseeksecs = m_meta[m_play_idx].curseeksecs;
    }
    psl_pause_impl();
}

void Playlist::psl_change_track(int num)
{
    m_insertion_point = -1;
    m_play_idx = -1;
    psl_change_track_impl(num);
    QSettings settings;
    bool saveseek = settings.value("saveseekpos").toBool();
    if (saveseek && num >= 0 && num < int(m_meta.size())) {
        int playlist_mode = CSettingsStorage::getInstance()->getPlaylistMode();
        int seeksecs = (playlist_mode & PLM_repAll) ? m_meta[num].pauseseeksecs :
            m_meta[num].curseeksecs;
        if (seeksecs > 0) {
            psl_seek(seeksecs);
        }
    }
}

int Playlist::mode()
{
    return CSettingsStorage::getInstance()->getPlaylistMode();
}

// GUI -->
void Playlist::psl_change_mode(int mode)
{
    CSettingsStorage::getInstance()->setPlaylistMode(mode);
    m_insertion_point = -1;
    emit sig_mode_changed(mode);
}

void Playlist::get_metadata(MetaDataList& out)
{
    out = m_meta;
}

class MetaDataCmp {
public:
    enum SortCrit {SC_TIT, SC_ART, SC_ALB, SC_Y, SC_FN, SC_TNO};

    MetaDataCmp(const std::vector<SortCrit>& crits)
        : m_crits(crits) {}
    bool operator()(const MetaData& o1, const MetaData& o2) {
        int rel;
        QString s1, s2;
        for (unsigned int i = 0; i < m_crits.size(); i++) {
            SortCrit crit = m_crits[i];
            rel = obgetprop(o1, crit, s1).compare(obgetprop(o2, crit, s2));
            if (rel < 0)
                return true;
            else if (rel > 0)
                return false;
        }
        return false;
    }
    const QString& obgetprop(const MetaData& o, SortCrit crit, QString& stor) {

        qint32 val;
        switch(crit) {
        case SC_TIT: return o.title;
        case SC_ART: return o.artist;
        case SC_ALB: return o.album;
        case SC_FN: return o.url;
        case SC_Y: val = o.year; goto valtostr;
        case SC_TNO: val = o.track_num;
        valtostr: {
                char num[30];
                snprintf(num, 30, "%010d", int(val));
                stor = num;
                return stor;
            } 
        default:
            return nullstr;
        }
    }

    std::vector<SortCrit> m_crits;
    static QString nullstr;
};
QString MetaDataCmp::nullstr;

static void mdsort(const MetaDataList& inlist, MetaDataList& outlist,
                   const std::vector<MetaDataCmp::SortCrit> crits)
{
    MetaDataCmp cmpo(crits);
    outlist = inlist;
    std::sort(outlist.begin(), outlist.end(), cmpo);
}

void Playlist::psl_sort_by_tno()
{
    std::vector<MetaDataCmp::SortCrit> crits;
    // Makes no sense to sort by tno independantly of album
    crits.push_back(MetaDataCmp::SC_ALB);
    crits.push_back(MetaDataCmp::SC_TNO);
    MetaDataList md;
    mdsort(m_meta, md, crits);
    psl_clear_playlist();
    psl_insert_tracks(md, -1);
}

void Playlist::psl_add_tracks(const MetaDataList& _v_md, bool nodups)
{
    LOGDEB("Playlist::psl_add_tracks: " << " ninserttracks " <<  _v_md.size() << 
           " m_meta.size() " << m_meta.size() << " insertion point " << m_insertion_point <<
           "play_idx " << m_play_idx << "\n");

    emit sig_sync();

    int playlist_mode = CSettingsStorage::getInstance()->getPlaylistMode();
    if (playlist_mode & PLM_replace)
        psl_clear_playlist();

    MetaDataList v_md;
    if (nodups) {
        for (const auto& n : _v_md) {
            for (const auto& e : m_meta) {
                if (n.url == e.url) {
                    goto dup;
                }
            }
            v_md.push_back(n);
        dup:
            ;
        }
    } else {
        v_md = _v_md;
    }
    
    int afteridx = -1;
    if (playlist_mode & PLM_append) {
        afteridx = m_meta.size() - 1;
    } else {
        if (m_insertion_point >= 0) {
            afteridx = m_insertion_point;
        } else if (m_play_idx >= 0) {
            afteridx = m_play_idx;
        } else {
            // The more natural thing to do if neither playing nor selection is to append. Else
            // clicking on several tracks inserts them in reverse order
            afteridx = m_meta.size() - 1;
        }
    }

    psl_insert_tracks(v_md, afteridx);
    m_insertion_point = afteridx + v_md.size();

    if (playlist_mode & PLM_playAdded) {
        psl_change_track(afteridx+1);
        psl_play();
    }

    emit sig_insert_done();
}

static std::string maybemakesavedir()
{
    QDir topdata(Helper::getHomeDataPath());
    if (!topdata.exists("pl") && !topdata.mkdir("pl")) {
        LOGINF("Could not create: " << qs2utf8s(topdata.absoluteFilePath("pl")) << "\n");
    }
    return qs2utf8s(topdata.absoluteFilePath("pl"));
}

void Playlist::psl_load_playlist()
{
    std::string savedir = maybemakesavedir();
    if (savedir.empty())
        return;
    QString fn =  QFileDialog::getOpenFileName(
        0, tr("Open Playlist"), u8s2qs(savedir), tr("Saved playlists (*.pl)"));
    if (!fn.isNull()) {
        psl_do_load_playlist(fn);
    }
}

void Playlist::psl_do_load_playlist(const QString& fn)
{
    MetaDataList meta;
    mdlUnSerialize(meta, fn);
    psl_clear_playlist();
    psl_add_tracks(meta);
}

void Playlist::psl_save_playlist()
{
    std::string savedir = maybemakesavedir();
    if (savedir.empty())
        return;

    // Note: no way to use getSaveFilename with multiple filters as far as I could find out
    const QStringList filters({
            tr("Saved playlists (*.pl)"),
            tr("M3U8 (*.m3u *.m3u8)"),
            tr("CSV (*.csv)"),
        });
    QFileDialog fileDialog;
    fileDialog.setDirectory(u8s2qs(savedir));
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setNameFilters(filters);
    fileDialog.exec();
    if (fileDialog.selectedFiles().isEmpty())
        return;

    // Add appropriate extension if not already there
    auto fn = fileDialog.selectedFiles()[0];
    auto flt = fileDialog.selectedNameFilter();
    QString ext(".pl");
    if (flt == filters[1]) {
        ext = ".m3u";
    } else if (flt == filters[2]) {
        ext = ".csv";
    }
    if (!fn.endsWith(ext, Qt::CaseInsensitive))
        fn += ext;

    // Save
    bool ret;
    if (fn.endsWith(".csv", Qt::CaseInsensitive)) {
        ret = playlistToCSV(m_meta, fn);
    } else if (fn.endsWith(".m3u", Qt::CaseInsensitive)) {
        ret = playlistToM3U(m_meta, fn);
    } else {
        ret = mdlSerialize(m_meta, fn);
    }
    if (!ret)
        QMessageBox::warning(0, "Upplay", tr("Could not save data."));
}

void Playlist::psl_selected_list(const QList<int>& lst)
{
    LOGDEB1("Playlist::psl_selected_list: lstsz " << lst.size() << "\n");
    m_selection_min_row = INT_MAX;
    for (int i = 0; i < int(m_meta.size()); i++) {
        m_meta[i].pl_selected = 0;
    }
    for (int i = 0; i < lst.size(); i++) {
        if (lst[i] < int(m_meta.size())) {
            if (m_selection_min_row < lst[i])
                m_selection_min_row = lst[i];
            m_meta[lst[i]].pl_selected = 1;
        }
    }
    if (m_selection_min_row == INT_MAX)
        m_selection_min_row = -1;
    LOGDEB2("Playlist::psl_selected_list: minrow now" << m_selection_min_row << "\n");
}
