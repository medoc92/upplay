/* Copyright (C) 2014 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "playlistohrd.h"

#include "libupnpp/log.hxx"

using namespace UPnPP;

PlaylistOHRD::PlaylistOHRD(OHRadioQA *ohrd, OHInfoQA *ohif, QObject *parent)
    : Playlist(parent), m_ohrdo(ohrd), m_ohifo(ohif)
{
    // Connections from OpenHome renderer to local playlist
    connect(m_ohrdo, SIGNAL(metadataArrayChanged(const MetaDataList&)),
            this, SLOT(onRemoteMetaArray(const MetaDataList&)));
    connect(m_ohrdo, SIGNAL(currentTrackId(int)),
            this, SLOT(onRemoteCurrentTrackid(int)));
    connect(m_ohrdo, SIGNAL(audioStateChanged(AudioState, const char *)),
            this, SLOT(onRemoteTpState(AudioState, const char *)));
    connect(m_ohrdo, SIGNAL(connectionLost()), this, SIGNAL(connectionLost()));

    if (m_ohifo) {
        connect(m_ohifo, SIGNAL(metatextChanged(const MetaData&)),
                this, SIGNAL(sig_track_metadata(const MetaData&)));
        connect(this, SIGNAL(sig_sync()), m_ohifo, SLOT(fetchState()));
    }

    // Connections from local playlist to openhome
    connect(this, SIGNAL(sig_sync()), m_ohrdo, SLOT(fetchState()));
    connect(this, SIGNAL(sig_pause()), m_ohrdo, SLOT(pause()));
    connect(this, SIGNAL(sig_stop()),  m_ohrdo, SLOT(stop()));
    connect(this, SIGNAL(sig_resume_play()), m_ohrdo, SLOT(play()));
    connect(this, SIGNAL(sig_set_idx(int)), m_ohrdo, SLOT(setIdx(int)));
}

static bool samelist(const MetaDataList& mdv1, const MetaDataList& mdv2)
{
    if (mdv1.size() != mdv2.size())
        return false;
    for (int i = 0; i < int(mdv1.size()); i++) {
        if (mdv1[i].url.compare(mdv2[i].url))
            return false;
    }
    return true;
}

void PlaylistOHRD::onRemoteMetaArray(const MetaDataList& mdv)
{
    LOGERR("PlaylistOHRD::onRemoteMetaArray: " << mdv.size() << " entries\n");
    if (!samelist(mdv, m_meta)) {
        m_meta = mdv;
        emit sig_playlist_updated(m_meta, m_play_idx, 0);
    }
}

void PlaylistOHRD::onRemoteCurrentTrackid(int id)
{
    LOGERR("PlaylistOHRD::onRemoteCurrentTrackid: " << id << " array size " << m_meta.size() <<"\n");

    if (id <= 0) {
        return;
    } 

    for (auto it = m_meta.begin(); it != m_meta.end(); it++) {
        if (it->id == id) {
            m_play_idx = it - m_meta.begin();
            // Emit the current index in any case to let the playlist
            // UI scroll to show the currently playing track (some
            // time after a user interaction scrolled it off)
            emit sig_playing_track_changed(m_play_idx);
            // If the track id change was caused by the currently
            // playing track having been removed, the play index did
            // not change but the metadata did, so emit metadata in
            // all cases.
            MetaData md;
            if (m_ohifo && m_ohifo->metatext(md)) {
                emit sig_track_metadata(md);
            } else {
                emit sig_track_metadata(*it);
            }
            return;
        }
    }
    LOGINF("PlaylistOHRD::onRemoteCurrentTrackid: trackid " << id << " not found in array (size " <<
           m_meta.size() << ")\n");
}

void PlaylistOHRD::update_state()
{
    emit sig_sync();
}

void PlaylistOHRD::psl_change_track_impl(int idx) {
    emit sig_set_idx(idx);
    emit sig_resume_play();
}

void PlaylistOHRD::psl_play() 
{
    if (m_tpstate ==  AUDIO_STOPPED && valid_row(get_selection_min_row())) {
        emit sig_set_idx(get_selection_min_row());
    }
    emit sig_resume_play();
}

void PlaylistOHRD::psl_pause_impl() 
{
    emit sig_pause();
}

void PlaylistOHRD::psl_stop() 
{
    emit sig_stop();
}
