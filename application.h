/* 
 * Copyright (C) 2017-2023  J.F. Dockes
 * Copyright (C) 2013  Lucio Carreras
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <string>
#include <memory>

#include <QObject>
#include <QApplication>
#include <QString>

struct MetaData;

class Application : public QObject
{
    Q_OBJECT;

public:
    Application(QApplication* qapp, QObject *parent = 0);
    virtual ~Application();

    void getIdleMeta(MetaData* mdp, bool connlost=false);
    static float horizontalDPI;
    QString currentRendererFN() const;
                       
public slots:
    void chooseRenderer();
    void clearRenderer();
    void chooseSource();
    void tryPlay();
    void loadURL();
    void doLoadURL(const QString& url, const QString& title, bool nodups=false);
    void doLoadPlaylist(const QString& fn);
    void openSongcast();
    void onConnectionLost();
    void newSubs();
    void onSourceTypeChanged(int);
    void onDirSortOrder();
    void onPrefsChanged();
    void writeDescription();
    void switchToRadio(bool);
    void setupRenderer();
    void onPaused();
    void onPlaying();
    void onStopped();
    void onPlayPause();
    void onVolumeChanged(int);
    void onMprisSetVolume(double);
    void onFocusChange(QWidget *old, QWidget *now);
    
signals:
    void sig_openhome_renderer(bool isopenhome, const QString& friendlyname);
    void sig_openhome_source_radio(bool);
    void sig_openhome_set_source_index(int);
    void sig_thr_delrd();
    void sig_thr_delpl();
    void sig_renderer_selected(const QString&);
    
private:
    class Internal;
    Internal *m;

    void init_connections();
    void renderer_connections();
    void playlist_connections();
    bool setupRenderer(std::string uid);
    void createPlaylistForOpenHomeSource();
    void chooseSourceOH();
    void chooseSourceAVT();
    int currentVolume();
    bool hasrdr();
};

extern Application *upplay_app;

#endif // APPLICATION_H
