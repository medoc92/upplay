#!/bin/sh
fatal()
{
    echo $*
    exit 1
}
Usage()
{
    fatal prepare-build.sh
}

test $# -eq 0 || Usage

# Script to copy the pre-cooked macos config file in place of the autoconf ones

TOP=/Users/dockes/upnp
UPP=${TOP}/upplay
LIBUPNPP=${TOP}/libupnpp
LIBNPUPNP=${TOP}/npupnp

cp -p ${LIBNPUPNP}/macos/autoconfig-macos.h ${LIBNPUPNP}/autoconfig.h || fatal autoconfig
test -d ${LIBNPUPNP}/inc/upnp || mkdir ${LIBNPUPNP}/inc/upnp
cp -p ${LIBNPUPNP}/macos/upnpconfig-macos.h ${LIBNPUPNP}/inc/upnpconfig.h || fatal upnpconfig
cp -p ${LIBNPUPNP}/inc/*.* ${LIBNPUPNP}/inc/upnp/ || fatal npupnp includes
