#!/bin/sh

qcbuildloc=Qt_6_7_3_for_macOS
deploy=~/Qt/6.7.3/macos/bin/macdeployqt

BUILDDIR=~/upnp/upplay/build/${qcbuildloc}-Release
UPSRC=~/upnp/upplay


VERSION=`egrep ^VERSION ${UPSRC}/upplay.pro  | awk '{print $3}'`
DATE=`date +%Y%m%d`
HASH=`(cd ${UPSRC}; git log -n 1|head -1|awk '{print $2}'|cut -b 1-8)`
PLATFORM=webengine

echo VERSION $VERSION DATE $DATE HASH $HASH PLATFORM $PLATFORM

rm -f ${BUILDDIR}/upplay.dmg

${deploy} ${BUILDDIR}/upplay.app -dmg || exit 1

DMGNAME=~/Documents/upplay-${VERSION}-${DATE}-${HASH}-${PLATFORM}.dmg

echo Creating $DMGNAME
mv -f ${BUILDDIR}/upplay.dmg ${DMGNAME}

