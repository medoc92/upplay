/* Copyright (C) 2014-2024 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include <string>
#include <iostream>
#include <sstream>

#include <QApplication>
#include <QObject>
#include <QTimer>
#include <QStringList>
#include <QSettings>
#include <QMessageBox>

#include <libupnpp/upnpplib.hxx>
#include <libupnpp/upnpputils.hxx>
#include <libupnpp/log.hxx>

#include "application.h"
#include "upadapt/upputils.h"
#include "utils/pathut.h"
#include "utils/smallut.h"
#include "HelperStructs/Helper.h"

using namespace UPnPP;

Application *upplay_app;

const int subscription_timeout_secs = 60;
// 2 S should be ample for a healthy device to answer a subscribe/unsubscribe, and using a
// reasonably short timeout helps us avoiding too much asynchronicity in places.
const int subsops_timeout_ms = 3000; 
    
static const char *thisprog;
static const char usage [] =
    "upplay [--load-url|-u <url>] [-h] [-v]\n"
    " -h : help.\n"
    " [--load-url|-u] <url> : load url into playlist at startup.\n"
    " [--load-url-title|-t] <title> : title for loaded URL.\n"
    " [--load-playlist|-P] : load upplay playlist file (.pl)\n"
    " [--play|p] : start playing after load.\n"
    " -v : print version.\n"
    ;

static void
versionInfo(FILE *fp)
{
    fprintf(fp, "Upplay %s %s\n", UPPLAY_VERSION, LibUPnP::versionString().c_str());
}

static void
Usage(FILE *fp = stderr)
{
    fprintf(fp, "%s: Usage: %s", thisprog, usage);
    versionInfo(fp);
    exit(fp == stderr);
}

static QString libiniterror(LibUPnP *mylib)
{
#if LIBUPNPP_AT_LEAST(0,19,4)
    Q_UNUSED(mylib);
    return u8s2qs(LibUPnP::errAsString(" ", LibUPnP::getInitError()));
#else
    if (mylib) {
        return u8s2qs(mylib->errAsString("main", mylib->getInitError()));
    } else {
        return QString();
    }
#endif
}

static struct option long_options[] = {
    {"help", 0, 0, 'h'},
    {"play", 0, 0, 'p'},
    {"load-playlist", required_argument, 0, 'P'},
    {"load-url-title", required_argument, 0, 't'},
    {"load-url", required_argument, 0, 'u'},
    {"version", 0, 0, 'v'},
    {0, 0, 0, 0}
};

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    thisprog = argv[0];

    QCoreApplication::setOrganizationName("Upmpd.org");
    QCoreApplication::setApplicationName("upplay");
    QCoreApplication::setAttribute(Qt::AA_DontUseNativeDialogs);

    QString loadplaylist;
    QString loadurl;
    QString loadurltitle;
    bool dostart{false};
    int ret;
    while ((ret = getopt_long(argc, argv, "hP:pu:t:v", long_options, nullptr)) != -1) {
        switch (ret) {
        case 'h': Usage(stdout); break;
        case 'p': dostart = true;break;
        case 'P': loadplaylist = optarg;break;
        case 't': loadurltitle = optarg;break;
        case 'u': loadurl = optarg;break;
        case 'v': versionInfo(stdout); exit(0); break;
        default: Usage();
        }
    }

    if (optind != argc)
        Usage();
    if (!loadurl.isEmpty() && !loadplaylist.isEmpty())
        Usage();
    
    if (Logger::getTheLog("stderr") == 0) {
        std::cerr << "Can't initialize log" << "\n";
        return 1;
    }

    QSettings settings;
    int upnploglevel = settings.value("upnploglevel").toInt();
    std::string suplfn = qs2utf8s(settings.value("upnplogfilename").toString().trimmed());
    const char *upnplogfilename = suplfn.c_str();
    char *cp = getenv("UPPLAY_UPNPLOGFILENAME");
    if (cp)
        upnplogfilename = cp;
    cp = getenv("UPPLAY_UPNPLOGLEVEL");
    if (cp) {
        upnploglevel = atoi(cp);
    }
    upnploglevel = std::max(upnploglevel, 0);
    if (upnploglevel) {
        LibUPnP::setLogFileName(upnplogfilename,LibUPnP::LogLevel(upnploglevel));
    }

    std::string ifname = qs2utf8s(settings.value("netifname").toString().trimmed());
    if (!ifname.empty()) {
        std::cerr << "Initializing library with interface " << ifname << "\n";
    }

    // Note that the lib init may fail here if ifname is wrong.
    // The later discovery would call the lib init again (because
    // the singleton is still null), which would retry the init,
    // without an interface this time, which would probably succeed,
    // so that things may still mostly work, which is confusing and the
    // reason we do the retry here instead.
    std::string upproduct("Upplay");
    std::string upversion(UPPLAY_VERSION);
    unsigned int flags{0};

    std::string resanitizedchars;
#if LIBUPNPP_AT_LEAST(0,24,1)
    if (settings.value("resanitizeurls").toBool()) {
        flags |= LibUPnP::UPNPPINIT_FLAG_RESANITIZE_URLS;
        std::string resanfn = path_cat(qs2utf8s(Helper::getHomeDataPath()), "resanchars.txt");
        if (path_readable(resanfn)) {
            std::fstream rstream;
            if (path_streamopen(resanfn, std::ios::in, rstream)) {
                std::ostringstream sstr;
                sstr << rstream.rdbuf();
                resanitizedchars = sstr.str();
                trimstring(resanitizedchars, "\r\n");
            }
        }
    }
#endif

    for (int i = 0; i < 2; i++) {
        LibUPnP::init(flags, LibUPnP::UPNPPINIT_OPTION_IFNAMES, &ifname,
                      LibUPnP::UPNPPINIT_OPTION_SUBSCRIPTION_TIMEOUT, subscription_timeout_secs,
                      LibUPnP::UPNPPINIT_OPTION_CLIENT_PRODUCT, &upproduct,
                      LibUPnP::UPNPPINIT_OPTION_CLIENT_VERSION, &upversion,
                      LibUPnP::UPNPPINIT_OPTION_SUBSOPS_TIMEOUTMS, subsops_timeout_ms,
                      // Note: libupnpp will ignore an empty value here.
                      LibUPnP::UPNPPINIT_OPTION_RESANITIZED_CHARS, &resanitizedchars,
                      LibUPnP::UPNPPINIT_OPTION_END);

        LibUPnP *mylib = LibUPnP::getLibUPnP();

        if (mylib && mylib->ok())
            break;

        QString explain = libiniterror(mylib);
        if (i == 1 || ifname.empty()) {
            QMessageBox::warning(0, "Upplay",
                                 app.tr("Lib init failed Network configuration issue ? ") + explain);
            break;
        } else {
            QMessageBox::warning(0, "Upplay", app.tr("Lib init failed for ") + u8s2qs(ifname) +
                                 " " + explain + app.tr(". Retrying with null interface"));
            // retry with empty ifname
            ifname.clear();
        }
    }

    Application application(&app);
    upplay_app = &application;
    if (!loadurl.isEmpty()) {
        QTimer::singleShot(0, [&] {application.doLoadURL(loadurl, loadurltitle, true);});
    } else if (!loadplaylist.isEmpty()) {
        QTimer::singleShot(0, [&] {application.doLoadPlaylist(loadplaylist);});
    }
    if (dostart) {
        QTimer::singleShot(50, [&] {application.tryPlay();});
    }
    return app.exec();
}
