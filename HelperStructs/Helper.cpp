/* Copyright (C) 2011  Lucio Carreras
 * Copyright (C) 2017  J.F. Dockes
 *
 * This file is part of Upplay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>

#include <time.h>

#include <QDir>
#include <QString>
#include <QThread>

#include "HelperStructs/Helper.h"
#include "HelperStructs/MetaData.h"
#include "HelperStructs/globals.h"
#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Style.h"

#include "upadapt/upputils.h"

QByteArray Helper::readFileToByteArray(const QString& fn)
{
    QFile qf(fn);
    QByteArray ret;
    if (qf.open(QIODevice::ReadOnly)) {
        ret = qf.read(1000 * 1000);
    }
    return ret;
}

static QString cvtNum2String(int num, int digits)
{
    QString str = QString::number(num);
    while (str.size() < digits) {
        str.prepend("0");
    }

    return str;
}

QString Helper::cvtMsecs2TitleLengthString(long int msec, bool colon,
                                           bool show_days)
{
    QString sign;
    if (msec < 0) {
        msec = -msec;
        sign = "-";
    }
        
    bool show_hrs = false;

    int sec = msec / 1000;
    int min = sec / 60;

    int secs = sec % 60;
    int hrs = min / 60;
    int days = hrs / 24;

    QString final_str;

    if (days > 0 && show_days) {
        final_str += QString::number(days) + "d ";
        hrs = hrs % 24;
        show_hrs = true;
    }

    if (!show_days) {
        hrs += (days * 24);
    }

    if (hrs > 0 || show_hrs) {
        final_str += QString::number(hrs) + "h ";
        min = min % 60;
    }

    if (colon) {
        final_str +=  cvtNum2String(min, 2) + ":" + cvtNum2String(secs, 2);
    } else {
        final_str +=  cvtNum2String(min, 2) + "m " + cvtNum2String(secs, 2);
    }

    return sign + final_str;
}

#ifdef Q_OS_WIN
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

QString GetExecPath()
{
    TCHAR dest[MAX_PATH];
    DWORD length = GetModuleFileName(NULL, dest, MAX_PATH);
#ifdef NTDDI_WIN8_future
    PathCchRemoveFileSpec(dest, MAX_PATH);
#else
    PathRemoveFileSpec(dest);
#endif
    if (sizeof(TCHAR) == 2)
        return QString::fromUtf16((const ushort*)dest);
    else
        return QString::fromUtf8((const char*)dest);
}
#endif

QString Helper::getSharePath()
{
    QString path;

#if defined(Q_OS_WIN)
    QDir execdir(GetExecPath());
    QString rpath("Share");
    execdir.mkpath(rpath);
    path = execdir.absoluteFilePath(rpath);
#elif defined(Q_OS_MACOS)
    path = QCoreApplication::applicationDirPath() + QString("/..");
    path = QDir::cleanPath(path);
#else
    if (QFile::exists(PREFIX "/share/upplay")) {
        path = PREFIX "/share/upplay/";
    } else {
        path = "";
    }
#endif
    return path;
}

QString Helper::getCSSPath()
{
#if defined(Q_OS_MACOS) || defined(Q_OS_MAC)
    return Helper::getSharePath() + "/Resources/";
#else
    return Helper::getSharePath() + "/cdbrowser/";
#endif
}

static QString styleSubDir;
void Helper::setStyleSubDir(const QString& subd)
{
    styleSubDir = subd;
}

// We now use the qt resources instead of installed icons
QString Helper::getIconPath(const QString& icnm)
{
    QString path(":/icons/");
    if (!styleSubDir.isEmpty()) {
        QString pathdark = path + styleSubDir + "/" + icnm;
        QDir dir(":/");
        if (dir.exists(pathdark)) {
            return pathdark;
        }
    }
    return path + icnm;
}

QString Helper::getHomeDataPath()
{
    QDir homedir(QDir::home());

#ifndef Q_OS_WIN
    QString rpath = QString::fromUtf8(".local/share/upplay");
#else
    QString rpath = QString::fromUtf8("AppData/Local/upplay/");
#endif

    homedir.mkpath(rpath);
    return homedir.absoluteFilePath(rpath);
}

#define DARK_BLUE(x) QString("<font color=#0000FF>") + x + QString("</font>")
#define LIGHT_BLUE(x) QString("<font color=#8888FF>") + x + QString("</font>")

QString Helper::createLink(QString name, QString target, bool underline)
{

    int dark = Style::use_dark_colors();
    if (target.size() == 0) {
        target = name;
    }

    QString content;
    QString style = "";

    if (!underline) {
        style = " style: \"text-decoration=none;\" ";
    }

    if (dark) {
        content = LIGHT_BLUE(name);
    } else {
        content = DARK_BLUE(name);
    }

    return QString("<a href=\"") + target + "\"" + style + ">" + content + "</a>";
}

bool Helper::read_file_into_str(QString filename, QString* content)
{
    QFile file(filename);
    content->clear();
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }

    while (!file.atEnd()) {
        QByteArray arr = file.readLine();
        QString str = QString::fromLocal8Bit(arr);

        content->append(str);
    }

    file.close();

    if (content->size() > 0) {
        return true;
    }

    return false;
}

// Escape things that would look like HTML markup
std::string Helper::escapeHtml(const std::string &in)
{
    std::string out;
    for (std::string::size_type pos = 0; pos < in.length(); pos++) {
        switch(in.at(pos)) {
        case '<': out += "&lt;"; break;
        case '>': out += "&gt;"; break;
        case '&': out += "&amp;"; break;
        case '"': out += "&quot;"; break;
        default: out += in.at(pos); break;
        }
    }
    return out;
}

QString Helper::escapeHtml(const QString& in)
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    return in.toHtmlEscaped();
#else
    return Qt::escape(in);
#endif
}

std::string ivtos(const std::vector<int>& nids)
{
    std::string sids;
    for (unsigned int i = 0; i < nids.size(); i++) {
        char cbuf[30];
        snprintf(cbuf, 30, "%d", nids[i]);
        sids += std::string(cbuf) + " ";
    }
    return sids;
}

void mdlSetCurPlayTrack(MetaDataList& lst, int idx)
{
    for (int i = 0; i < int(lst.size()); i++) {
        lst[i].pl_playing = false;
    }
    if (idx >= 0 && idx < int(lst.size()))
        lst[idx].pl_playing = true;
}

bool mdlContains(MetaDataList& lst, const MetaData& md, bool cs, MetaData **found)
{
    QString url = cs ? md.url.trimmed() : md.url.toLower().trimmed();
    for(int i = 0; i < int(lst.size()); i++) {
        QString url2 = cs ? lst[i].url.trimmed() : lst[i].url.toLower().trimmed();
        if(!url.compare(url2)) {
            if (found) 
                *found = &(lst[i]);
            return true;
        }
    }
    return false;
}

bool mdlSerialize(MetaDataList& lst, const QString& fn)
{
    QFile file(fn);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    for (int i = 0; i < int(lst.size()); i++) {
        lst[i].serialize(out);
    }
    return true;
}

bool mdlUnSerialize(MetaDataList& lst, const QString& fn)
{
    QFile file(fn);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);
    MetaData md;
    while (md.unSerialize(in)) {
        lst.push_back(md);
    }
    return true;
}
