/* Copyright (C) 2012  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT SANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GLOBALS_H_
#define GLOBALS_H_
#include <QObject>

enum AudioState {
    AUDIO_UNKNOWN,
    AUDIO_STOPPED,
    AUDIO_PLAYING,
    AUDIO_PAUSED
};

Q_DECLARE_METATYPE(AudioState);

enum PlaylistMode {PLM_repAll=0x1, PLM_shuffle=0x2, PLM_append=0x4, PLM_replace=0x8,
    PLM_playAdded=0x10};

inline void copyplmbit(int& out, int in, PlaylistMode bit) {
    if (in & bit) {
        out |= bit;
    } else {
        out &= ~bit;
    }
}
#endif /* GLOBALS_H_ */
