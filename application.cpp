/* 
 * Copyright (C) 2017-2023  J.F. Dockes
 * Copyright (C) 2013  Lucio Carreras
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

#include <QApplication>
#include <QMessageBox>
#include <QDir>
#include <QFileDialog>
#include <QThread>

#ifdef HAVE_QTMPRIS
#include <MprisPlayer>
#endif /* HAVE_QTMPRIS */

#include "libupnpp/upnpplib.hxx"
#include "libupnpp/log.hxx"
#include "libupnpp/control/discovery.hxx"
#include "libupnpp/control/mediarenderer.hxx"
#include "libupnpp/control/conman.hxx"

#include "GUI/bareurl/bareurl.h"
#include "GUI/mainw/mainw.h"
#include "GUI/playlist/GUI_Playlist.h"
#include "GUI/prefs/prefs.h"
#include "GUI/renderchoose/renderchoose.h"
#include "GUI/sourcechoose/sourcechoose.h"
#include "GUI/songcast/songcastdlg.h"
#include "notifications/notifications.h"
#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Helper.h"
#include "HelperStructs/Style.h"
#include "HelperStructs/globals.h"
#include "dirbrowser/dirbrowser.h"
#include "playlist/playlist.h"
#include "playlist/playlistavt.h"
#include "playlist/playlistnull.h"
#include "playlist/playlistohpl.h"
#include "playlist/playlistohrcv.h"
#include "playlist/playlistohrd.h"
#include "upadapt/avtadapt.h"
#include "upadapt/ohpladapt.h"
#include "upadapt/songcast.h"
#include "upadapt/rendererlist.h"
#include "upadapt/vvolume.h"
#include "upadapt/vtime.h"
#include "upqo/ohproduct_qo.h"
#include "upqo/ohradio_qo.h"
#include "upqo/ohtime_qo.h"
#include "upqo/ohvolume_qo.h"
#include "upqo/renderingcontrol_qo.h"
#include "utils/writedescription.h"
#include "utils/smallut.h"

#include "application_p.h"

using namespace UPnPClient;


// Run the renderer thread. When the event loop exits (because the main thread called its exit()),
// delete the upnp objects and commit suicide. Note that deleting the objects can take a long time
// if the renderer is unresponsive, because of the unsubscribe tries.
// Note that the playlist object belongs to us, but it's running in the main thread.
void RdrThread::run()
{
    exec();
    LOGDEB("RdrThread: exec() returned, cleaning up\n");

    // Clean up. Of course not all of the following will be non-null. The deletions can take a very
    // long time because of the unsubscription attempts.
    m_playlist.reset();

    // vtime and vvol must be deleted *before* the objects they borrow. 
    delete m_vtime;
    delete m_vvol;

    // Then these because they are connected to the watchdog timer. Only one will be non-null.
    if (m_ohplo) {
        m_ohplo->udiscon();
        delete m_ohplo;
    }
    if (m_ohrdo) {
        m_ohrdo->udiscon();
        delete m_ohrdo;
    }
    if (m_avto) {
        m_avto->udiscon();
        delete m_avto;
    }
    if (m_ohtmo) {
        m_ohtmo->udiscon();
        delete m_ohtmo;
    }
    if (m_ohinf) {
        m_ohinf->udiscon();
        delete m_ohinf;
    }
    if (m_ohpro) {
        m_ohpro->udiscon();
        delete m_ohpro;
    }
    if (m_ohvlo) {
        m_ohvlo->udiscon();
        delete m_ohvlo;
    }
    if (m_rdco) {
        m_rdco->udiscon();
        delete m_rdco;
    }

    this->deleteLater();
}

void RdrThrSourceController::delrdsource()
{
    LOGDEB("RdrThrSourceController::delrdsource()\n");
    auto o = m_rdrthr->m_ohrdo;
    m_rdrthr->m_ohrdo = nullptr;
    o->udiscon();
    delete o;
}
void RdrThrSourceController::delplsource()
{
    LOGDEB("RdrThrSourceController::delplsource()\n");
    auto o = m_rdrthr->m_ohplo;
    m_rdrthr->m_ohplo = nullptr;
    o->udiscon();
    delete o;
}

class Application::Internal {
public:
    Internal(QApplication *qapp)
        : m_settings(CSettingsStorage::getInstance()), m_app(qapp) {}

    GUI_Player *m_player{nullptr};

    RdrThread  *m_rdrlink{nullptr};
    RdrThrSourceController *m_rdrthrctl{nullptr};

    SongcastTool *m_sctool{nullptr};
    UpplayNotifications *m_notifs{nullptr};
#ifdef HAVE_QTMPRIS
    MprisPlayer *m_mpris{nullptr};
#endif /* HAVE_QTMPRIS */
    CSettingsStorage *m_settings;
    QApplication     *m_app;

    bool             m_initialized{false};
    // Can we send titles into the playlist (e.g. not OHradio).
    bool             m_playlistIsPlaylist{false};
    OHProductQO::SourceType m_ohsourcetype{OHProductQO::OHPR_SourceUnknown};
    QString          m_renderer_friendly_name;
    std::string      m_renderer_uid;
    QTimer m_polltimer;
};


#define CONNECT(a,b,c,d) m->m_app->connect(a, SIGNAL(b), c, SLOT(d), Qt::UniqueConnection)

static UPPrefs *g_prefs;

static UPnPDeviceDirectory *superdir;

Application::Application(QApplication* qapp, QObject *parent)
    : QObject(parent)
{
    m = new Internal(qapp);
    qRegisterMetaType<MetaData>();
    qRegisterMetaType<MetaDataList>();
    qRegisterMetaType<AudioState>();
    QString version = UPPLAY_VERSION;
    m->m_settings->setVersion(version);
    
    // This sets up the message log. Do it as early as possible
    onPrefsChanged();
    m->m_player = new GUI_Player(this);
    g_prefs = new UPPrefs(m->m_player);
    connect(this, SIGNAL(sig_renderer_selected(const QString&)),
            g_prefs, SLOT(onCurrentRendererChanged(const QString&)));

    m->m_notifs = new UpplayNotifications(this);
    m->m_polltimer.start(1000);

#ifdef HAVE_QTMPRIS
    m->m_mpris = new MprisPlayer(this);
    m->m_mpris->setServiceName("Upplay");
    m->m_mpris->setCanControl(true);
    m->m_mpris->setCanGoNext(true);
    m->m_mpris->setCanGoPrevious(true);
    m->m_mpris->setCanPause(true);
    m->m_mpris->setCanPlay(true);
#endif /* HAVE_QTMPRIS */

    superdir = UPnPDeviceDirectory::getTheDir(1);
    if (superdir == 0) {
        LOGERR("Can't create UPnP discovery object" << "\n");
        std::cerr << "Can't create UPnP discovery object" << "\n";
        exit(1);
    }

    auto value = m->m_settings->value("previousRdrURL");
    if (!value.isNull()) {
        auto url = qs2utf8s(value.toString());
        if (!url.empty()) {
            LOGINF("Initializing unicast search for " << url << "\n");
            superdir->uniSearch(url);
            millisleep(100);
        }
    }
    
    init_connections();
    std::string uid = qs2utf8s(m->m_settings->getPlayerUID());
    if (uid.empty()) {
        QTimer::singleShot(0, this, SLOT(chooseRenderer()));
    } else {
        if (!setupRenderer(uid)) {
            LOGERR("Can't connect to previous media renderer" << "\n");
            std::cerr << "Can't connect to previous media renderer" << "\n";
            QTimer::singleShot(0, this, SLOT(chooseRenderer()));
        }
    }

    onPrefsChanged();
    m->m_player->show();
    m->m_initialized = true;
}

Application::~Application()
{
    delete m->m_player;
}

// Used for testing the AVT interface on upmpdcli running with oh+avt
static bool avtonly = false;

void Application::chooseRenderer()
{
    bool ohonly = m->m_settings->value("ohonly").toBool();
    int flags{0};
    if (ohonly) {
        flags |= int(RSF_OHONLY);
    }
    if (avtonly) {
        flags |= int(RSF_AVTONLY);
    }
    emit sig_openhome_renderer(false, QString());
    if (rendererList(flags).empty()) {
        QMessageBox::warning(0, "Upplay", tr("No Media Renderers found."));
        return;
    }
    RenderChooseDLG dlg(flags, m->m_renderer_friendly_name, m->m_player);
    if (ohonly) {
        dlg.setWindowTitle(tr("Select Renderer (OpenHome Only)"));
    }
    if (!dlg.exec()) {
        return;
    }

    int row = dlg.rndsLW->currentRow();
    if (row < 0 || row >= int(dlg.getdevices().size())) {
        LOGERR("Internal error: bad row after renderer choose dlg" << "\n");
        std::cerr << "Internal error: bad row after renderer choose dlg" << "\n";
        return;
    }

    const UPnPDeviceDesc& chosen(dlg.getdevices()[row]);

    MetaDataList curmeta;
    if (hasrdr() && m->m_rdrlink->m_playlist) {
        m->m_rdrlink->m_playlist->get_metadata(curmeta);
    }

    m->m_renderer_friendly_name = u8s2qs(chosen.friendlyName);
    emit sig_renderer_selected(m->m_renderer_friendly_name);
    
    if (!setupRenderer(chosen.UDN)) {
        QMessageBox::warning(0, "Upplay", tr("Can't connect to ") + m->m_renderer_friendly_name);
        m->m_renderer_friendly_name = "";
        emit sig_renderer_selected(m->m_renderer_friendly_name);
        return;
    }
    m->m_settings->setPlayerUID(u8s2qs(chosen.UDN));

    if (m->m_rdrlink->m_playlist && !dlg.keepRB->isChecked()) {
        if (dlg.replRB->isChecked()) {
            m->m_rdrlink->m_playlist->psl_clear_playlist();
        }
        m->m_rdrlink->m_playlist->psl_add_tracks(curmeta);
    }
}

void Application::chooseSource()
{
    if (hasrdr() && m->m_rdrlink->m_ohpro) {
        // OpenHome renderer
        chooseSourceOH();
    } else {
        // Not ready yet
        return;
    }
}

void Application::switchToRadio(bool onoff)
{
    if (!hasrdr() || !m->m_rdrlink->m_ohpro) {
        return;
    }
    std::string cursrcname =  m->m_rdrlink->m_ohpro->getSourceName();
    // Using the well known names for upmpdcli songcast sources, this
    // is brittle, but at worse, we'll switch to the non-songcast
    // source.
    bool isSongcast = cursrcname.find("Songcast") != std::string::npos;
    if (onoff) {
        if (isSongcast) {
            m->m_rdrlink->m_ohpro->setSourceIndexByName("RD-to-Songcast");
        } else {
            m->m_rdrlink->m_ohpro->setSourceIndexByName("Radio");
        }
    } else {
        if (isSongcast) {
            m->m_rdrlink->m_ohpro->setSourceIndexByName("PL-to-Songcast");
        } else {
            m->m_rdrlink->m_ohpro->setSourceIndexByName("Playlist");
        }
    }
}


// Load bare URL into playlist (no Media Server necessary).
void Application::loadURL()
{
    // Open a dialog to get the URL and possibly title from the user
    BareURLDLG dlg(m->m_player);
    if (!dlg.exec()) {
        return;
    }
    auto url = dlg.urlLE->text();
    auto title = dlg.titleLE->text();
}

void Application::doLoadURL(const QString& url, const QString& _title, bool nodups)
{
    if (!hasrdr() && !m->m_rdrlink->m_playlist) {
        return;
    }
    QString title(_title);
    if (title.isEmpty()) {
        title = url;
    }

    // Create a minimal MetaDataList for psl_add_tracks(). Some renderers would accept an empty
    // metadata part (the didl fragment), but, e.g. upmpdcli in openhome mode does not like it. So
    // we synthetize a minimum DIDL string. Actually, this probably could be done instead in
    // psl_add_tracks when the didl field is found empty, but this is a special case anyway.
    MetaDataList mdl;
    mdl.resize(1);
    mdl[0].url = url;
    mdl[0].title = title;
    mdl[0].didl =
        QString("<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                "<DIDL-Lite xmlns:dc=\"http://purl.org/dc/elements/1.1/\" "
                "xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" "
                "xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\" "
                "xmlns:dlna=\"urn:schemas-dlna-org:metadata-1-0/\">");
    
    mdl[0].didl += QString("<item><dc:title>") +
        u8s2qs(SoapHelp::xmlQuote(qs2utf8s(title))) + "</dc:title>" +
        "<upnp:class>object.item.audioItem.musicTrack</upnp:class></item></DIDL-Lite>";
    
    m->m_rdrlink->m_playlist->psl_add_tracks(mdl, nodups);
}

void Application::doLoadPlaylist(const QString& fn)
{
    if (!hasrdr() && !m->m_rdrlink->m_playlist) {
        return;
    }
    m->m_rdrlink->m_playlist->psl_clear_playlist();
    m->m_rdrlink->m_playlist->psl_do_load_playlist(fn);
}

void Application::chooseSourceOH()
{
    std::vector<UPnPClient::OHProduct::Source> srcs;
    if (!m->m_rdrlink->m_ohpro->getSources(srcs)) {
        return;
    }
    LOGDEB("Application::chooseSource: got " << srcs.size() << " sources\n");
    int cur = -1;
    m->m_rdrlink->m_ohpro->getSourceIndex(&cur);

    std::vector<int> rowtoidx;
    SourceChooseDLG dlg(m->m_player);
    for (unsigned int i = 0; i < srcs.size(); i++) {
        // Receiver can't be usefully selected (no way to specify the
        // sender). Old versions of upmpdcli made Receiver not
        // visible, but the linn device have it visible and this is
        // necessary for kazoo, so just skip Receiver-type sources.
        if (!srcs[i].visible || srcs[i].type == "Receiver")
            continue;
        QString stype = u8s2qs(srcs[i].type + "\t(" + srcs[i].name + ")");
        if (int(i) == cur) {
            QListWidgetItem *item = new QListWidgetItem(stype);
            QFont font = dlg.rndsLW->font();
            font.setBold(true);
            item->setFont(font);
            dlg.rndsLW->addItem(item);
        } else {
            dlg.rndsLW->addItem(stype);
        }
        rowtoidx.push_back(i);
    }
    if (!dlg.exec()) {
        return;
    }

    int row = dlg.rndsLW->currentRow();
    if (row < 0 || row >= int(rowtoidx.size())) {
        LOGERR("Internal error: bad row after source choose dlg\n");
        return;
    }
    int idx = rowtoidx[row];
    if (idx != cur) {
        emit sig_openhome_set_source_index(idx);
    }
}


void Application::openSongcast()
{
    SongcastDLG *scdlg;
    if (!m->m_sctool) {
        scdlg = new SongcastDLG(m->m_player);
        m->m_sctool = new SongcastTool(scdlg, this);
    } else {
        scdlg = m->m_sctool->dlg();
        m->m_sctool->initControls();
    }
    if (scdlg) {
        scdlg->hide();
        scdlg->show();
    }
}

bool Application::hasrdr()
{
    return m && m->m_rdrlink;
}

void Application::clearRenderer()
{
    LOGDEB("Application::clearRenderer()\n");
    if (!hasrdr()) {
        LOGDEB("Application::clearRenderer: no renderer\n");
        return;
    }
    // Do this before exiting the thread to sort of ensure that the actual playlist deletion occurs
    // in the thread run routine.
    m->m_player->library()->setPlaylist(std::shared_ptr<Playlist>());

    if (m->m_rdrlink->m_rdco && m->m_rdrlink->m_rdco->polling()) {
        disconnect(&m->m_polltimer, SIGNAL(timeout()), m->m_rdrlink->m_rdco, SLOT(update()));
    }

    // This has a ref to the rdrlink, delete it first.
    delete m->m_rdrthrctl;
    m->m_rdrthrctl = nullptr;

    // Tell the renderer thread event loop to exit. The run routine will clean up everything before
    // returning
    auto o = m->m_rdrlink;
    m->m_rdrlink = nullptr;
    o->exit();
    
    m->m_ohsourcetype = OHProductQO::OHPR_SourceUnknown;
}

void Application::newSubs()
{
    bool ok{false};
    if (hasrdr() && m->m_rdrlink->m_rdr) {
#if LIBUPNPP_AT_LEAST(0,21,0)
        ok = m->m_rdrlink->m_rdr->reSubscribeAll();
#endif
        if (!ok) {
            const UPnPDeviceDesc *desc = m->m_rdrlink->m_rdr->desc();
            if (desc) {
                m->m_renderer_uid = desc->UDN;
                QTimer::singleShot(0, this, SLOT(setupRenderer()));
            }
        }
    }
}

void Application::onConnectionLost()
{
    MetaData md;
    getIdleMeta(&md, true);
    md.artist += QString(" : Connection lost");
    m->m_player->update_track(md);
}

void Application::setupRenderer()
{
    if (m)
        setupRenderer(m->m_renderer_uid);
}

bool Application::setupRenderer(std::string uid)
{
    OHPRH ohpr;
    RDCH rdc;
    bool needavt = true;
    
    // Start deletion of the upnp objects. This can take a long time, runs in background in the
    // current renderer thread
    clearRenderer();

    // Allocate new renderer object/thread
    m->m_rdrlink = new RdrThread();
    m->m_rdrthrctl = new RdrThrSourceController(m->m_rdrlink);
    m->m_rdrthrctl->moveToThread(m->m_rdrlink);
    connect(this, SIGNAL(sig_thr_delrd()), m->m_rdrthrctl, SLOT(delrdsource()));
    connect(this, SIGNAL(sig_thr_delpl()), m->m_rdrthrctl, SLOT(delplsource()));
        
    bool needs_playlist = true;
    
    // The media renderer object is not used directly except for
    // providing handles to the services. Note that the lib will
    // return anything implementing either renderingcontrol or
    // ohproduct
    m->m_rdrlink->m_rdr = getRenderer(uid, false);
    if (!m->m_rdrlink->m_rdr) {
        LOGERR("Renderer " << uid << " not found" << "\n");
        std::cerr << "Renderer " << uid << " not found" << "\n";
        goto error;
    }
    m->m_renderer_friendly_name = u8s2qs(m->m_rdrlink->m_rdr->desc()->friendlyName);
    emit sig_renderer_selected(m->m_renderer_friendly_name);

    // Use either renderingControl or ohvolume for volume control.
    rdc = m->m_rdrlink->m_rdr->rdc();
    if (rdc) {
        LOGDEB("Application::setupRenderer: using Rendering Control\n");
        m->m_rdrlink->m_rdco = new RenderingControlQO(rdc);
        if (!rdc->ok()) {
            LOGERR("Could not connect to rendering control service\n");
            goto error;
        }
        m->m_rdrlink->m_rdco->moveToThread(m->m_rdrlink);
        if (m->m_rdrlink->m_rdco->polling()) {
            connect(&m->m_polltimer, SIGNAL(timeout()), m->m_rdrlink->m_rdco, SLOT(update()));
        }
        m->m_rdrlink->m_vvol = new VirtualVolume(m->m_rdrlink->m_rdco);
    } else {
        OHVLH ohvl = m->m_rdrlink->m_rdr->ohvl();
        if (!ohvl) {
            LOGERR("Device implements neither RenderingControl nor OHVolume\n");
            goto error;
        }
        LOGDEB("Application::setupRenderer: using OHVolume\n");
        m->m_rdrlink->m_ohvlo = new OHVolumeQO(ohvl);
        if (!ohvl->ok()) {
            LOGERR("Could not connect to ohvolume service\n");
            goto error;
        }
        m->m_rdrlink->m_ohvlo->moveToThread(m->m_rdrlink);
        m->m_rdrlink->m_vvol = new VirtualVolume(m->m_rdrlink->m_ohvlo);
    }
    if (m->m_rdrlink->m_vvol)
        m->m_rdrlink->m_vvol->moveToThread(m->m_rdrlink);

    ohpr = m->m_rdrlink->m_rdr->ohpr();
    if (!avtonly && ohpr) {
        // This is an OpenHome media renderer
        m->m_rdrlink->m_ohpro = new OHProductQO(ohpr);
        m->m_rdrlink->m_ohpro->moveToThread(m->m_rdrlink);
        connect(m->m_rdrlink->m_ohpro, SIGNAL(sourceTypeChanged(int)),
                this, SLOT(onSourceTypeChanged(int)));
        connect(this, SIGNAL(sig_openhome_set_source_index(int)),
                m->m_rdrlink->m_ohpro, SLOT(setSourceIndex(int)));

        int scratch;
        if (!m->m_rdrlink->m_ohpro->getSourceIndex(&scratch)) {
            // nobody there.
            LOGERR("setupRenderer: can't connect to OpenHome renderer " <<
                   qs2utf8s(m->m_renderer_friendly_name) << "\n");
            goto error;
        }

        // Try to use the time service
        OHTMH ohtm = m->m_rdrlink->m_rdr->ohtm();
        if (ohtm) {
            LOGDEB("Application::setupRenderer: OHTm ok, no need for avt\n");
            m->m_rdrlink->m_ohtmo = new OHTimeQO(ohtm);
            m->m_rdrlink->m_ohtmo->moveToThread(m->m_rdrlink);
            m->m_rdrlink->m_vtime = new VirtualTime(m->m_rdrlink->m_ohtmo);
            m->m_rdrlink->m_vtime->moveToThread(m->m_rdrlink);
            // no need for AVT then
            needavt = false;
        }

        // Create appropriate Playlist object depending on type of source. Some may need m->m_ohtmo,
        // so keep this behind its creation.
        createPlaylistForOpenHomeSource();
        needs_playlist = false;

        // Move this out of the if when avt radio is ready
        emit sig_openhome_renderer(true, m->m_renderer_friendly_name);
    } else {
        emit sig_openhome_renderer(false, m->m_renderer_friendly_name);
    }

    // It would be possible in theory to be connected to an openhome
    // playlist without a time service?? and use avt for time updates
    // instead.
    if (needavt) {
        AVTH avt = m->m_rdrlink->m_rdr->avt();
        if (!avt) {
            LOGERR("Renderer: AVTransport missing but we need it\n");
            goto error;
        }
        m->m_rdrlink->m_avto = new AVTPlayer(avt, m->m_rdrlink->m_rdr->conman());
        if (!m->m_rdrlink->m_avto || !m->m_rdrlink->m_avto->checkConnection()) {
            goto error;
        }
        m->m_rdrlink->m_avto->moveToThread(m->m_rdrlink);
        connect(&m->m_polltimer, SIGNAL(timeout()), m->m_rdrlink->m_avto, SLOT(update()));
        m->m_rdrlink->m_vtime = new VirtualTime(m->m_rdrlink->m_avto);
        m->m_rdrlink->m_vtime->moveToThread(m->m_rdrlink);
    }

    // Keep this after avt object creation !
    if (needs_playlist) {
        LOGDEB("Application::setupRenderer: using AVT playlist\n");
        m->m_rdrlink->m_playlist = std::make_shared<PlaylistAVT>(m->m_rdrlink->m_avto,
                                                                 m->m_rdrlink->m_rdr->desc()->UDN);
        m->m_playlistIsPlaylist = true;
    }

    
    renderer_connections();
    playlist_connections();

    // Set up a few things which sometimes don't get events at startup
    m->m_player->setVolumeUi(m->m_rdrlink->m_vvol->volume());
    onVolumeChanged(m->m_rdrlink->m_vvol->volume());
    m->m_player->setCurrentPosition(m->m_rdrlink->m_vtime->secsInSong());

    {
        auto description = m->m_rdrlink->m_rdr->desc();
        m->m_settings->setValue("previousRdrURL", u8s2qs(description->descURL));
    }

    m->m_rdrlink->start();
    return true;

error:
    // The renderer thread was not started so we can't just call clearrenderer()
    delete m->m_rdrthrctl;
    m->m_rdrthrctl = nullptr;
    connect(m->m_rdrlink, SIGNAL(started()), m->m_rdrlink, SLOT(quit()));
    auto o = m->m_rdrlink;
    m->m_rdrlink = nullptr;
    o->start();
    return false;
}

void Application::createPlaylistForOpenHomeSource()
{
    m->m_ohsourcetype = OHProductQO::SourceType(m->m_rdrlink->m_ohpro->getSourceType());

    if (m->m_rdrlink->m_ohrdo) {
        disconnect(&m->m_polltimer, SIGNAL(timeout()), m->m_rdrlink->m_ohrdo, SLOT(testconn()));
        emit sig_thr_delrd();
    }
    if (m->m_rdrlink->m_ohplo) {
        disconnect(&m->m_polltimer, SIGNAL(timeout()), m->m_rdrlink->m_ohplo, SLOT(testconn()));
        emit sig_thr_delpl();
    }
    
    switch (m->m_ohsourcetype) {

    case OHProductQO::OHPR_SourceRadio:
    {
        OHRDH ohrd = m->m_rdrlink->m_rdr->ohrd();
        if (!ohrd) {
            LOGERR("Application::createPlaylistForOpenHomeSource: can't connect radio mode\n");
            return;
        }
        m->m_rdrlink->m_ohrdo = new OHRadioQA(ohrd);
        m->m_rdrlink->m_ohrdo->moveToThread(m->m_rdrlink);
        connect(&m->m_polltimer, SIGNAL(timeout()), m->m_rdrlink->m_ohrdo, SLOT(testconn()));
        OHIFH ohif = m->m_rdrlink->m_rdr->ohif();
        if (ohif) {
            m->m_rdrlink->m_ohinf = new OHInfoQA(ohif);
            m->m_rdrlink->m_ohinf->moveToThread(m->m_rdrlink);
        }
        m->m_rdrlink->m_playlist =
            std::make_shared<PlaylistOHRD>(m->m_rdrlink->m_ohrdo, m->m_rdrlink->m_ohinf);
        m->m_playlistIsPlaylist = false;
        emit sig_openhome_source_radio(true);
    }
    break;

    case OHProductQO::OHPR_SourceReceiver:
    {
        OHRCH ohrc = m->m_rdrlink->m_rdr->ohrc();
        if (!ohrc) {
            LOGERR("App::createPlaylistForOpenHomeSource: receiver mode, but can't connect\n");
            return;
        }
        m->m_rdrlink->m_playlist =
            std::make_shared<PlaylistOHRCV>(ohrc, u8s2qs(m->m_rdrlink->m_rdr->desc()->friendlyName));
        m->m_playlistIsPlaylist = false;
        emit sig_openhome_source_radio(false);
    }
    break;

    case OHProductQO::OHPR_SourcePlaylist:
    {
        OHPLH ohpl = m->m_rdrlink->m_rdr->ohpl();
        if (ohpl) {
            m->m_rdrlink->m_ohplo = new OHPlayer(ohpl);
            m->m_rdrlink->m_ohplo->moveToThread(m->m_rdrlink);
            connect(&m->m_polltimer, SIGNAL(timeout()), m->m_rdrlink->m_ohplo, SLOT(testconn()));
            m->m_rdrlink->m_playlist =
                std::make_shared<PlaylistOHPL>(m->m_rdrlink->m_ohplo, m->m_rdrlink->m_ohtmo);
            m->m_playlistIsPlaylist = true;
        }
        emit sig_openhome_source_radio(false);
    }
    break;

    default:
    {
        m->m_rdrlink->m_playlist = std::make_shared<PlaylistNULL>();
        m->m_playlistIsPlaylist = false;
        emit sig_openhome_source_radio(false);
    }
    break;
    }

    if (!m->m_rdrlink->m_playlist) {
        LOGERR("Application::createPlaylistForOpenHomeSource: could not create playlist object\n");
    }
}

void Application::onDirSortOrder()
{
    g_prefs->onShowPrefs(UPPrefs::PTAB_DIRSORT);
}

static std::vector<CharFlags> sourceTypeNames{
    {OHProductQO::OHPR_SourceUnknown, "Unknown"}, {OHProductQO::OHPR_SourcePlaylist, "Playlist"},
    {OHProductQO::OHPR_SourceRadio, "Radio"}, {OHProductQO::OHPR_SourceReceiver, "Receiver"}};

void Application::onSourceTypeChanged(int tp)
{
    LOGDEB("Application::onSourceTypeChanged: new " << valToString(sourceTypeNames, tp) <<
           " was " << valToString(sourceTypeNames, m->m_ohsourcetype) << '\n');
    if (tp == m->m_ohsourcetype) {
        LOGDEB1("Application::onSourceTypeChanged: same type\n");
        return;
    }
    
    if (!m->m_rdrlink->m_ohpro) {
        // Not possible cause ohpro is the sender of this signal.. anyway
        LOGERR("Application::onSourceTypeChanged: no OHProduct!!\n");
        return;
    }
    createPlaylistForOpenHomeSource();
    playlist_connections();
}

void Application::getIdleMeta(MetaData* mdp, bool connlost)
{
    QString sourcename;
    if (!connlost && m->m_rdrlink && m->m_rdrlink->m_ohpro) {
        sourcename = u8s2qs(m->m_rdrlink->m_ohpro->getSourceName());
    }
    mdp->title = QString::fromUtf8("Upplay ") + UPPLAY_VERSION;
    if (m->m_renderer_friendly_name.isEmpty()) {
        mdp->artist = "No renderer connected";
    } else {
        mdp->artist = tr("Renderer: ") + m->m_renderer_friendly_name;
        if (!sourcename.isEmpty()) {
            mdp->artist += QString::fromUtf8(" (") + sourcename + ")";
        }
    }
}

// We may switch the playlist when an openhome renderer switches sources. So
// set the playlist connections in a separate function
void Application::playlist_connections()
{
    if (m->m_playlistIsPlaylist)
        m->m_player->library()->setPlaylist(m->m_rdrlink->m_playlist);
    else
        m->m_player->library()->setPlaylist(std::shared_ptr<Playlist>());

    // Use either ohtime or avt for time updates
    CONNECT(m->m_rdrlink->m_vtime, secsInSongChanged(quint32),
            m->m_rdrlink->m_playlist.get(), onRemoteSecsInSong(quint32));
    CONNECT(m->m_rdrlink->m_vtime, secsInSongChanged(quint32),
            m->m_notifs, songProgress(quint32));

#ifdef HAVE_QTMPRIS
    if (m->m_mpris) {
        CONNECT(m->m_mpris, playRequested(), m->m_rdrlink->m_playlist.get(), psl_play());
        CONNECT(m->m_mpris, pauseRequested(), m->m_rdrlink->m_playlist.get(), psl_pause());
        CONNECT(m->m_mpris, playPauseRequested(), this,  onPlayPause());
        CONNECT(m->m_mpris, stopRequested(), m->m_rdrlink->m_playlist.get(), psl_stop());
        CONNECT(m->m_mpris, nextRequested(), m->m_rdrlink->m_playlist.get(), psl_forward());
        CONNECT(m->m_mpris, previousRequested(), m->m_rdrlink->m_playlist.get(), psl_backward());
        CONNECT(m->m_mpris, volumeRequested(double), this, onMprisSetVolume(double));
    }
#endif /* HAVE_QTMPRIS */

    CONNECT(m->m_player, play(), m->m_rdrlink->m_playlist.get(), psl_play());
    CONNECT(m->m_player, pause(), m->m_rdrlink->m_playlist.get(), psl_pause());
    CONNECT(m->m_player, stop(), m->m_rdrlink->m_playlist.get(), psl_stop());
    CONNECT(m->m_player, forward(), m->m_rdrlink->m_playlist.get(), psl_forward());
    CONNECT(m->m_player, backward(), m->m_rdrlink->m_playlist.get(), psl_backward());
    CONNECT(m->m_player, sig_load_playlist(), m->m_rdrlink->m_playlist.get(), psl_load_playlist());
    CONNECT(m->m_player, sig_save_playlist(), m->m_rdrlink->m_playlist.get(), psl_save_playlist());
    CONNECT(m->m_player, sig_seek(int), m->m_rdrlink->m_playlist.get(), psl_seek(int));

    CONNECT(m->m_rdrlink->m_playlist.get(), connectionLost(), this, onConnectionLost());
    CONNECT(m->m_rdrlink->m_playlist.get(), renewSubscriptions(), this, newSubs());
    CONNECT(m->m_rdrlink->m_playlist.get(), playlistModeChanged(int),
            m->m_player->playlist(), setPlayerMode(int));
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_track_metadata(const MetaData&),
            m->m_player, update_track(const MetaData&));

    CONNECT(m->m_rdrlink->m_playlist.get(), sig_track_metadata(const MetaData&),
            m->m_notifs, notify(const MetaData&));
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_stopped(),  m->m_notifs, onStopped());
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_paused(),  m->m_notifs, onPaused());
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_playing(),  m->m_notifs, onPlaying());
    CONNECT(m->m_notifs, notifyNeeded(const MetaData&), m->m_player, onNotify(const MetaData&));

#ifdef HAVE_QTMPRIS
    if (m->m_mpris) {
        CONNECT(m->m_rdrlink->m_playlist.get(), sig_stopped(), this, onStopped());
        CONNECT(m->m_rdrlink->m_playlist.get(), sig_paused(), this, onPaused());
        CONNECT(m->m_rdrlink->m_playlist.get(), sig_playing(), this, onPlaying());
    }
#endif /* HAVE_QTMPRIS */

    CONNECT(m->m_rdrlink->m_playlist.get(), sig_stopped(),  m->m_player, stopped());
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_paused(),  m->m_player, paused());
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_playing(),  m->m_player, playing());
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_playing_track_changed(int),
            m->m_player->playlist(), track_changed(int));
    CONNECT(m->m_rdrlink->m_playlist.get(), sig_playlist_updated(MetaDataList&, int, int),
            m->m_player->playlist(), fillPlaylist(MetaDataList&, int, int));
    CONNECT(m->m_player->playlist(), selected_list(const QList<int>&),
            m->m_rdrlink->m_playlist.get(), psl_selected_list(const QList<int>&));
    CONNECT(m->m_player->playlist(), playlist_mode_changed(int),
            m->m_rdrlink->m_playlist.get(), psl_change_mode(int));
    CONNECT(m->m_player->playlist(), dropped_tracks(const MetaDataList&, int),
            m->m_rdrlink->m_playlist.get(), psl_insert_tracks(const MetaDataList&, int));
    CONNECT(m->m_player->playlist(), sig_rows_removed(const QList<int>&, bool),
            m->m_rdrlink->m_playlist.get(), psl_remove_rows(const QList<int>&, bool));
    CONNECT(m->m_player->playlist(), sig_sort_tno(), m->m_rdrlink->m_playlist.get(), psl_sort_by_tno());
    CONNECT(m->m_player->playlist(), row_activated(int),
            m->m_rdrlink->m_playlist.get(), psl_change_track(int));
    CONNECT(m->m_player->playlist(), clear_playlist(),
            m->m_rdrlink->m_playlist.get(), psl_clear_playlist());

    m->m_rdrlink->m_playlist->update_state();
}

// Direct renderer-Player connections (not going through the Playlist):
// volume and time mostly.
void Application::renderer_connections()
{
    CONNECT(m->m_player, sig_dumpDescription(), this, writeDescription());
    CONNECT(m->m_rdrlink->m_vtime, secsInSongChanged(quint32),
            m->m_player, setCurrentPosition(quint32));
    CONNECT(m->m_player, sig_volume_changed(int), m->m_rdrlink->m_vvol, setVolume(int));
    CONNECT(m->m_player, sig_mute(bool), m->m_rdrlink->m_vvol, setMute(bool));
    CONNECT(m->m_rdrlink->m_vvol, volumeChanged(int), m->m_player, setVolumeUi(int));
    CONNECT(m->m_rdrlink->m_vvol, volumeChanged(int), this, onVolumeChanged(int));
    CONNECT(m->m_rdrlink->m_vvol, muteChanged(bool), m->m_player, setMuteUi(bool));
}

void Application::tryPlay()
{
    if (!hasrdr())
        return;
    auto pl = m->m_rdrlink->m_playlist;
    if (pl)
        pl->psl_play();
}
        
// MPRIS commands which are not direct signal/slot connections
void Application::onPlayPause()
{
    LOGDEB1("Application::onPlayPause()\n");
    if (!hasrdr() || !m->m_rdrlink->m_playlist)
        return;
    auto pl = m->m_rdrlink->m_playlist;
    if (pl) {
        switch (pl->getTpState()) {
        case AUDIO_PLAYING:
            pl->psl_pause();
            break;
        default:
            pl->psl_play();
            break;
        }
    }
}

void Application::onMprisSetVolume(double v)
{
    Q_UNUSED(v);
#ifdef HAVE_QTMPRIS
    LOGDEB1("Application::onMprisSetVolume: volume " << float(v) << "\n");
    if (v > 1.0) {
        v = 1.0;
    }
    if (v < 0) {
        v = 0;
    }
    int volume = 100 * v;
    if (m->m_rdrlink->m_vvol) {
        if (volume == 0) {
            m->m_rdrlink->m_vvol->setMute(true);
        } else {
            m->m_rdrlink->m_vvol->setMute(false);
            m->m_rdrlink->m_vvol->setVolume(volume);
        }
    }
#endif
}

// MPRIS status updates
int Application::currentVolume()
{
    if (m->m_rdrlink->m_vvol) {
        if (m->m_rdrlink->m_vvol->mute()) {
            return 0;
        }
        return m->m_rdrlink->m_vvol->volume();
    }
    return 0;
}

void Application::onVolumeChanged(int)
{
#ifdef HAVE_QTMPRIS
    LOGDEB1("Application::onVolumeChanged\n");
    if (m->m_mpris) {
        m->m_mpris->setVolume(double(currentVolume())/100.0);
    }
#endif /* HAVE_QTMPRIS */
}

void Application::onPaused()
{
#ifdef HAVE_QTMPRIS
    LOGDEB1("Application::onPaused\n");
    if (m->m_mpris) {
        m->m_mpris->setPlaybackStatus(Mpris::Paused);
    }
#endif /* HAVE_QTMPRIS */
}

void Application::onStopped()
{
    LOGDEB1("Application::onStopped\n");
#ifdef HAVE_QTMPRIS
    if (m->m_mpris) {
        m->m_mpris->setPlaybackStatus(Mpris::Stopped);
    }
#endif /* HAVE_QTMPRIS */
}

void Application::onPlaying()
{
    LOGDEB1("Application::onPlaying\n");
#ifdef HAVE_QTMPRIS
    if (m->m_mpris) {
        m->m_mpris->setPlaybackStatus(Mpris::Playing);
    }
#endif /* HAVE_QTMPRIS */
}

void Application::onPrefsChanged()
{
    static std::string oldlogfilename;
    std::string logfilename("stderr");
    const char *cp;
    if ((cp = getenv("UPPLAY_LOGFILENAME"))) {
        logfilename = cp;
    } else if (m->m_settings->contains("logfilename")) {
        logfilename = qs2utf8s(m->m_settings->value("logfilename").toString());
    }
    int loglevel = Logger::LLINF;
    if ((cp = getenv("UPPLAY_LOGLEVEL"))) {
        loglevel = atoi(cp);
    } else if (m->m_settings->contains("loglevel")) {
        loglevel = m->m_settings->value("loglevel").toInt();
    }
    if (oldlogfilename.compare(logfilename)) {
        Logger::getTheLog(logfilename)->reopen(logfilename);
        oldlogfilename = logfilename;
    }
    Logger::getTheLog(logfilename)->setLogLevel(Logger::LogLevel(loglevel));

    int upnploglevel = m->m_settings->value("upnploglevel").toInt();
    std::string upnplogfilename = qs2utf8s(
        m->m_settings->value("upnplogfilename").toString().trimmed());
    if (upnploglevel >= 0) {
        LibUPnP::setLogFileName(upnplogfilename, LibUPnP::LogLevel(upnploglevel));
    }

    if (m->m_player)
        m->m_player->onPrefsChanged();
}

// Connections which are possible without a renderer.
void Application::init_connections()
{
    CONNECT(m->m_player, sig_choose_renderer(), this, chooseRenderer());
    CONNECT(m->m_player, sig_open_songcast(), this, openSongcast());
    CONNECT(m->m_player, sig_choose_source(), this, chooseSource());
    CONNECT(m->m_player, sig_load_url(), this, loadURL());
    CONNECT(m->m_player, sig_quitting(),      this, clearRenderer());
    CONNECT(m->m_player, sig_preferences(), g_prefs, onShowPrefs());
    CONNECT(g_prefs, sig_prefsChanged(),     this, onPrefsChanged());
    CONNECT(m->m_player->library(), sig_sort_order(), this, onDirSortOrder());
    // This connects the library search close button to the main view action.
    CONNECT(m->m_player->library(), sig_hide_search(), m->m_player, hideDirSearch());
    CONNECT(m->m_player->library(), sig_show_search(), m->m_player, showDirSearch());
    CONNECT(this, sig_openhome_renderer(bool, const QString&),
            m->m_player, psl_openhome_renderer(bool, const QString&));
    CONNECT(this, sig_openhome_source_radio(bool),
            m->m_player->playlist(), psl_openhome_source_radio(bool));
    CONNECT(m->m_player->playlist(), sig_radio(bool), this, switchToRadio(bool));
    connect(qApp, SIGNAL(focusChanged(QWidget *, QWidget *)),
            this, SLOT(onFocusChange(QWidget *, QWidget *)));
}

static bool is_ancestor_of(const QObject *descendant, const QObject *ancestor)
{
    if (!ancestor)
        return false;
    while (descendant) {
        if (descendant == ancestor)
            return true;
        descendant = descendant->parent();
    }
    return false;
}

void Application::onFocusChange(QWidget *, QWidget *now)
{
    bool inprefs = false;
    auto w = g_prefs->window();
    if (w) {
        inprefs = is_ancestor_of(now, w);
    }
    if (inprefs) {
        m->m_player->disablePlayControl(true);
    } else {
        m->m_player->disablePlayControl(false);
    }
}

void Application::writeDescription()
{
    if (!superdir || !m || !m->m_rdrlink || !m->m_rdrlink->m_rdr) {
        return;
    }

    std::string deviceXML;
    std::unordered_map<std::string, std::string> srvsXML;
    const std::string& devname(m->m_rdrlink->m_rdr->desc()->UDN);
    if (!superdir->getDescriptionDocuments(devname, deviceXML, srvsXML)) {
        QMessageBox::warning(0, "Upplay", tr("Could not retrieve description documents"));
        return;
    }
    QString dir = QFileDialog::getExistingDirectory(
        m->m_player, tr("Directory for description files"));
    if (dir.isEmpty()) {
        return;
    }
    if (!writeDescriptionFiles(qs2utf8s(dir), devname, deviceXML, srvsXML)) {
        QMessageBox::warning(0, "Upplay", tr("Could not write description files."));
    }
}

QString Application::currentRendererFN() const
{
    return m->m_renderer_friendly_name;
}
