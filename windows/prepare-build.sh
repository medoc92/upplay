#!/bin/sh
fatal()
{
    echo $*
    exit 1
}
Usage()
{
    fatal prepare-build.sh
}

test $# -eq 0 || Usage

# Script to copy the pre-cooked windows config file in place of the autoconf ones

TOP=c:/users/bill/documents/upnp
UPP=${TOP}/upplay
LIBUPNPP=${TOP}/libupnpp
LIBNPUPNP=${TOP}/npupnp

cp -p ${LIBNPUPNP}/windows/autoconfig-windows.h ${LIBNPUPNP}/autoconfig.h || fatal autoconfig
test -d ${LIBNPUPNP}/inc/upnp || mkdir ${LIBNPUPNP}/inc/upnp
cp -p ${LIBNPUPNP}/windows/upnpconfig-windows.h ${LIBNPUPNP}/inc/upnpconfig.h || fatal upnpconfig
cp -p ${LIBNPUPNP}/inc/*.* ${LIBNPUPNP}/inc/upnp/

cp -p ${LIBUPNPP}/windows/config_windows.h ${LIBUPNPP}/config.h || fatal libupnpp config

