#!/bin/sh
fatal()
{
    echo $*
    exit 1
}
Usage()
{
    fatal mkinstdir.sh targetdir
}

test $# -eq 1 || Usage

DESTDIR=$1

test -d $DESTDIR || mkdir $DESTDIR || fatal cant create $DESTDIR

# Script to make an upplay install directory from locally compiled
# software.

################################
# Local values (to be adjusted)

# We have two set of values on a w10 with msvc (webengine) and a w7
# with an older qt and mingw (which can build with webkit). MSVC can
# now build with Webkit too (thanks to the github maintenance
# project), but only up to qt 5.14. So we now use webengine.
MSVC=yes
WEBKIT=no
# 64 bits build? Only with MSVC at the moment
BUILD64=yes

# Upplay src tree
TOP=c:/users/bill/documents/upnp

UPP=${TOP}/upplay
LIBUPNPP=${TOP}/libupnpp
LIBNPUPNP=${TOP}/npupnp

if test "$MSVC" = "yes"; then
    if test "$BUILD64" = "yes"; then 
        QTBIN=C:/Qt/6.7.3/msvc2019_64/bin
        QTA=Desktop_Qt_6_7_3_MSVC2019_64bit
        MHTDLL=$UPP/../libmicrohttpd-0.9.65-w32-bin/x86_64/VS2019/Release-dll/libmicrohttpd-dll.dll
        EXPATDLL=$UPP/../expat.v140.2.4.1.1/build/native/bin/x64/Release/libexpat.dll
    else
        QTBIN=C:/Qt/5.15.2/msvc2019/bin
        QTA=Desktop_Qt_5_15_2_MSVC2019_32bit
        MHTDLL=$UPP/../libmicrohttpd-0.9.65-w32-bin/x86/VS2017/Release-dll/libmicrohttpd-dll.dll
        EXPATDLL=$UPP/../expat-2.2.9/bin/libexpat.dll
        # QTOPENSSL is only needed for a webkit build (we don't really do this any more).
        QTOPENSSL=C:/Qt/Tools/OpenSSL/Win_x86/bin
    fi
    GUIBIN=$UPP/build/${QTA}-Release/release/upplay.exe
else
    # Old mingw build. Kept around but unused
    QTBIN=C:/Qt/Qt5.8.0/5.8/mingw53_32/bin
    QTOPTBIN=C:/Qt/Qt5.8.0/Tools/mingw530_32/opt/bin/
    QTA=Desktop_Qt_5_8_0_MinGW_32bit
    GUIBIN=$UPP/../build-upplay-${QTA}-Debug/release/upplay.exe
fi

PATH=$QTBIN:$PATH
export PATH

# Check that the versions in libupnpp are consistent
checkconsistency()
{
    LIBH=$LIBUPNPP/libupnpp/upnpplib.hxx
    CONFH=$LIBUPNPP/qmk/config.h
    MAJOR=`grep '#define LIBUPNPP_VERSION_MAJOR' $LIBH | awk '{print $3}'`
    MINOR=`grep '#define LIBUPNPP_VERSION_MINOR' $LIBH | awk '{print $3}'`
    REVISION=`grep '#define LIBUPNPP_VERSION_REVISION' $LIBH | awk '{print $3}'`
    LIBSTRING='"'${MAJOR}.${MINOR}.${REVISION}'"'
    CONFSTRING=`grep '#define LIBUPNPP_VERSION' $CONFH | awk '{print $3}'`
    echo LIBSTRING $LIBSTRING CONFSTRING $CONFSTRING
    if test "$CONFSTRING" != "$LIBSTRING" ; then
        fatal inconsistent versions in $CONFH and $LIBH
    fi
}

# checkcopy. 
chkcp()
{
    cp $@ || fatal cp $@ failed
}

copyupplay()
{
    chkcp $UPP/dirbrowser/cdbrowser.css $DESTDIR/share/cdbrowser
    chkcp $UPP/dirbrowser/standard.css $DESTDIR/share/cdbrowser
    chkcp $UPP/dirbrowser/dark.css $DESTDIR/share/cdbrowser
    chkcp $UPP/dirbrowser/containerScript.js $DESTDIR/share/cdbrowser
    chkcp $UPP/dirbrowser/qwebchannel.js $DESTDIR/share/cdbrowser
    cp -rp $UPP/GUI/icons $DESTDIR/share
    chkcp $UPP/GUI/standard.qss $DESTDIR/share
    chkcp $UPP/GUI/dark.qss $DESTDIR/share
    chkcp $UPP/GUI/common.qss $DESTDIR/share
    chkcp $UPP/GUI/icons/upplay.ico $DESTDIR
    chkcp -rp $UPP/GUI/icons $DESTDIR
    chkcp $GUIBIN $DESTDIR
}

copyqt()
{
    cd $DESTDIR
    $QTBIN/windeployqt upplay.exe

    # Apparently because of the webkit graft (always on old mingw, conditionally with MSVC,
    # we need to copy some DLLs explicitely?
    if test "$MSVC" != "yes"; then
        addlibs="Qt5Core.dll Qt5Multimedia.dll Qt5MultimediaWidgets.dll Qt5OpenGL.dll \
          Qt5Positioning.dll Qt5PrintSupport.dll Qt5Sensors.dll Qt5Sql.dll icudt57.dll icuin57.dll \
          icuuc57.dll libQt5WebKit.dll libQt5WebKitWidgets.dll libxml2-2.dll libxslt-1.dll"
        for i in $addlibs;do
            chkcp $QTBIN/$i $DESTDIR
        done
        chkcp $QTBIN/libwinpthread-1.dll $DESTDIR
        chkcp $QTBIN/libstdc++-6.dll $DESTDIR
        chkcp $QTOPTBIN/ssleay32.dll $DESTDIR
        chkcp $QTOPTBIN/libeay32.dll $DESTDIR
    elif test "$WEBKIT" = "yes"; then
        # MSVC and Webkit
        addlibs="icudt65.dll icuin65.dll icuuc65.dll libxml2.dll libxslt.dll \
          Qt5WebKit.dll Qt5WebKitWidgets.dll"
        for i in $addlibs;do
            chkcp $QTBIN/$i $DESTDIR
        done
        chkcp  $QTOPENSSL/libcrypto-1_1.dll $DESTDIR
        chkcp  $QTOPENSSL/libssl-1_1.dll $DESTDIR
    fi
}

copymht()
{
    if test "$MSVC" = "yes"; then
       chkcp $MHTDLL $DESTDIR
    fi
}
copyexpat()
{
    if test "$MSVC" = "yes"; then
       chkcp $EXPATDLL $DESTDIR
    fi
}

test -d $DESTDIR/share/cdbrowser || mkdir -p $DESTDIR/share/cdbrowser || exit 1
checkconsistency
copyupplay
copyqt
copymht
copyexpat
