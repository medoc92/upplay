/* Copyright (C) 2014 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "dirbrowser.h"

#include <string>
#include <sstream>
#include <memory>
#include <iostream>

#include <QTabWidget>

#include <json/json.h>

std::string DirBrowser::stateToJson()
{
    Json::Value root;

    for (int i = m_tabs_base_index; i < m_tabs->count(); i++) {
        CDBrowser *cdb = cdbFromTabIndex(i);
        if (nullptr == cdb) {
            return std::string();
        }
        Json::Value tab;
        std::string udn = cdb->getServerUDN();
        if (udn.empty()) {
            continue;
        }
        tab["UDN"] = udn;

        Json::Value path;
        for (const auto& pe : cdb->getcurpath()) {
            Json::Value jspe;
            jspe["objid"] = pe.objid;
            jspe["title"] = pe.title;
            jspe["isPlaylist"] = pe.isPlaylist;
            jspe["searchStr"] = pe.searchStr;
            jspe["scrollposX"] = pe.scrollpos.x();
            jspe["scrollposY"] = pe.scrollpos.y();
            // The scrollpos is only set when browsing a subdir. So get it directly for the last
            // element, because it's not set there.
            // Also, if the last path elt was an album, restore the data we need to display the
            // album entry
            if (&pe == &cdb->getcurpath().back()) {
                QPoint sp = cdb->scrollpos();
                jspe["scrollposX"] = sp.x();
                jspe["scrollposY"] = sp.y();
            }
            auto upnpclass = pe.dirent.getprop("upnp:class");
            if (upnpclass == CDBrowser::albContainerClass ||
                upnpclass == CDBrowser::plContainerClass) {
                auto& dirent = pe.dirent;
                jspe["upnp:class"] = upnpclass;
                for (const auto& key : {"dc:date", "upnp:artist", "upnp:albumArtURI"}) {
                    jspe[key] = dirent.getprop(key);
                }
            }
            path.append(jspe);
        }
        tab["path"] = path;

        root.append(tab);
    }

    // Was: Json::FastWriter fastWriter;string out = fastWriter.write(root);
    // But, deprecated, why keep things simple ?
    std::ostringstream ostr;
    Json::StreamWriterBuilder builder;
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
    writer->write(root, &ostr);
    // std::cerr << "JSON: " << ostr.str() << "\n";
    return ostr.str();
}

std::vector<DirBrowser::CdbJsonStateElt> DirBrowser::stateFromJson(const std::string& jsonstr)
{
    std::vector<DirBrowser::CdbJsonStateElt> ret;

    Json::Value root;
    Json::CharReaderBuilder builder;
    std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
    std::string errs;
    const char *cp = jsonstr.c_str();
    bool ok = reader->parse(cp, cp + jsonstr.size(), &root, &errs);
    if (!ok) {
        LOGERR("DirBrowser::stateFromJson: could not parse json string: " << errs << "\n");
        return ret;
    }
    for (Json::ArrayIndex tabidx = 0; tabidx < root.size(); tabidx++) {
        std::string udn = root[tabidx]["UDN"].asString();
        ret.push_back(DirBrowser::CdbJsonStateElt(udn, std::vector<CDBrowser::CtPathElt>()));
        CDBrowser::CtPathElt pe;
        Json::Value& jspath = root[tabidx]["path"];
        for (Json::ArrayIndex pathidx = 0; pathidx < jspath.size(); pathidx++) {
            Json::Value& jspe = jspath[pathidx];
            pe.objid =        jspe["objid"].asString();
            pe.title =        jspe["title"].asString();
            pe.isPlaylist =   jspe["isPlaylist"].asBool();
            pe.searchStr =    jspe["searchStr"].asString();
            pe.scrollpos = QPoint(jspe["scrollposX"].asInt(), jspe["scrollposY"].asInt());

            // If the last path elt was an album, restore the data we need to display the album
            // entry
            auto upnpclass = jspe["upnp:class"].asString();
            if (upnpclass == CDBrowser::albContainerClass ||
                upnpclass == CDBrowser::plContainerClass) {
                auto& dirent = pe.dirent;
                dirent.m_id = pe.objid;
                dirent.m_title = pe.title;
                dirent.m_type = UPnPClient::UPnPDirObject::container;
                dirent.m_props["upnp:class"] = upnpclass;
                for (const auto& key : {"dc:date", "upnp:artist", "upnp:albumArtURI"}) {
                    auto v = jspe[key];
                    if (v)
                        dirent.m_props[key] = v.asString();
                }
            }
            ret.back().second.push_back(pe);
        }
    }
#if 0
    std::cerr << "stateFromJson: return: ";
    for (const auto& elt : ret) {
        std::cerr << "UDN [" << elt.first << "] ";
        for (const auto& pe : elt.second) {
            std::cerr << ":" << pe.title <<;
        }
    }
    std::cerr << "\n";
#endif
    return ret;
}
