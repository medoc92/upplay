/* Copyright (C) 2005-2024 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "cdbrowser.h"

#ifdef _MSC_VER
#include <io.h>
#else
#include <unistd.h>
#endif

#include <fcntl.h>

#include <iostream>
#include <functional>

#include <QtWidgets/QVBoxLayout>
#include <QGesture>

#include <QUrl>
#ifdef USING_WEBENGINE
// Notes for WebEngine
// - All links must begin with http:// for acceptNavigationRequest to be
//   called. 
// - The links passed to acceptNav.. have the host part 
//   lowercased -> we change S0 to http://h/S0, not http://S0
// As far as I can see, 2016-09, two relatively small issues remaining:
//  - No way to know when page load is done (see mySetHtml())

#include <QWebEnginePage>
#include <QWebEngineSettings>
#include <QWebEngineProfile>
#include <QtWebEngineWidgets>
#include <QLoggingCategory>
#else
#include <QWebFrame>
#include <QWebSettings>
#include <QWebElement>
#include <QNetworkDiskCache>
#endif

#include <QMessageBox>
#include <QMenu>
#include <QApplication>
#include <QByteArray>
#include <QProgressDialog>
#include <QClipboard>
#include <QSettings>
#include <QGesture>

#include "libupnpp/log.hxx"
#include "libupnpp/upnpplib.hxx"
#include "libupnpp/control/discovery.hxx"

#include "GUI/widgets/browserdialog.h"
#include "GUI/widgets/filedialog.h"
#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Helper.h"
#include "HelperStructs/Style.h"
#include "upadapt/upputils.h"
#include "upqo/cdirectory_qo.h"
#include "utils/md5.h"
#include "utils/pathut.h"
#include "utils/smallut.h"

#include "dirbrowser.h"
#include "rreaper.h"

using namespace std::placeholders;
using namespace UPnPP;
using namespace UPnPClient;

static const std::string minimFoldersViewPrefix("0$folders");

const std::string CDBrowser::plContainerClass{"object.container.playlistContainer"};
const std::string CDBrowser::albContainerClass{"object.container.album.musicAlbum"};

void CDWebPage::javaScriptConsoleMessage(
#ifdef USING_WEBENGINE
    JavaScriptConsoleMessageLevel,
#endif
    const QString& msg, int lineNum, const QString&)
{
    Q_UNUSED(msg);
    Q_UNUSED(lineNum);
    LOGERR("JAVASCRIPT: "<< qs2utf8s(msg) << " at line " << lineNum << "\n");
}

CDBrowser::CDBrowser(QWidget* parent, DirBrowser *tabs)
    : QWidget(parent), m_browsers(tabs)
{
    m_view =  new QWEBVIEW(this);
    QVBoxLayout *verticalLayout = new QVBoxLayout(this);
    verticalLayout->setSpacing(0);
    verticalLayout->setContentsMargins(0, 0, 0, 0);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout_2"));
    verticalLayout->addWidget(m_view);

    init_HTML();

    int cachembs{500};
    QSettings settings;
    if (settings.contains("maxcovercachesizembs")) {
        cachembs = settings.value("maxcovercachesizembs").toInt();
    }
    
    CDWebPage *webpage{nullptr};
#ifdef USING_WEBENGINE
    if (cachembs > 0) {
        auto profile = new QWebEngineProfile("upplay", this);
        profile->setHttpCacheType(QWebEngineProfile::DiskHttpCache);
        profile->setHttpCacheMaximumSize(cachembs * 1024 * 1024);
        webpage = new CDWebPage(profile, this);
    } else {
        webpage = new CDWebPage(this);
    }
#else
    webpage = new CDWebPage(this);
    if (cachembs > 0) {
        QNetworkAccessManager *netman = webpage->networkAccessManager();
        if (netman) {
            QNetworkDiskCache *cache = new QNetworkDiskCache(parent);
            cache->setCacheDirectory(u8s2qs(path_cat(path_cachedir(), "upplay")));
            cache->setMaximumCacheSize(cachembs * 1024 * 1024);
            netman->setCache(cache);
        }
    }
#endif

    grabGesture(Qt::TapAndHoldGesture);
    m_view->setPage(webpage);
    m_view->installEventFilter(this);
    
#ifdef USING_WEBENGINE
    connect(m_view, SIGNAL(loadFinished(bool)), this, SLOT(onLoadFinished(bool)));
#if (QT_VERSION >= QT_VERSION_CHECK(6, 2, 0))
    connect(m_view->page(), SIGNAL(newWindowRequested(QWebEngineNewWindowRequest&)),
            this, SLOT(onNewWindowRequested(QWebEngineNewWindowRequest&)));
#endif // 6.2.0
#else
    connect(m_view, SIGNAL(linkClicked(const QUrl &)), this, SLOT(onLinkClicked(const QUrl &)));
    // Not available and not sure that this is needed with webengine ?
    connect(m_view->page()->mainFrame(), SIGNAL(contentsSizeChanged(const QSize&)),
            this, SLOT(onContentsSizeChanged(const QSize&)));
    m_view->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
#endif

    m_view->settings()->setAttribute(QWEBSETTINGS::JavascriptEnabled, true);
    m_view->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_view, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(createPopupMenu(const QPoint&)));
    m_timer.setSingleShot(1);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(initialPage()));
    m_timer.start(0);
}

CDBrowser::~CDBrowser()
{
    deleteReaders("~CDBrowser");
}

void CDBrowser::takeFocus()
{
    if (m_view)
        m_view->setFocus();
}

void CDBrowser::setElidePath(bool onoff)
{
    m_elidePath = onoff;
}

void CDBrowser::mySetHtml(const QString& html)
{
    LOGDEB0("CDBrowser::mySetHtml:\n");
    LOGDEB1("            HTML :\n"<< qs2utf8s(html) << "\n");

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    const static QUrl baseUrl("file:///");
#else
    // QT 6 needs an http base url, else it won't load HTTP image Urls...
    const static QUrl baseUrl("http://localhost");
#endif
    
    m_view->setHtml(html, baseUrl);

#ifdef USING_WEBENGINE
    LOGDEB0("CDBrowser::setHtml returned, waiting for loaded event\n");
    m_pageloaded = false;
    while (!m_pageloaded) {
        qApp->processEvents(QEventLoop::AllEvents, 10);
    }
    LOGDEB0("CDBrowser::setHtml: page is loaded\n");
#if (QT_VERSION < QT_VERSION_CHECK(5, 11, 0))
    // For some reason this is needed to ensure that the js is inited.
    // Else we get the dreaded:
    // 'Uncaught ReferenceError: Base64Decode is not defined at line 2'
    Helper::msleep(100);
#endif
#endif
}

void CDBrowser::findText(const QString& text, bool reverse)
{
#ifdef USING_WEBENGINE
    QWebEnginePage::FindFlags options{};
    if (reverse) {
        options |= QWebEnginePage::FindBackward;
    }
    m_view->findText(text, options);
#else
    QWebPage::FindFlags options = QWebPage::FindWrapsAroundDocument;
    if (reverse)
        options |= QWebPage::FindBackward;

    m_view->findText(text, options);
#endif
}


#ifdef USING_WEBENGINE
void CDBrowser::onLoadFinished(bool ok)
{
    LOGDEB0("ONLOADFINISHED: " << ok << "\n");
    // If ok is false, this is just a consequence of delegating a link click, and has nothing to do
    // with our setting HTML. See:
    // https://doc.qt.io/qt-5/qwebenginepage.html#acceptNavigationRequest
    if (ok) {
        m_pageloaded = true;
    }
}

std::vector<CharFlags> navreqtypes{
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeLinkClicked),
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeTyped),
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeFormSubmitted),
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeBackForward),
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeReload),
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeRedirect),
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeOther),
};

bool CDWebPage::acceptNavigationRequest(const QUrl& url, NavigationType navtype, bool)
{
    LOGDEB1("CDWebPage::acceptNavigationRequest: type: "<<valToString(navreqtypes, navtype)<<"\n");

    if (navtype == QWebEnginePage::NavigationTypeLinkClicked) {
        QTimer::singleShot(0, [this, url]() {this->m_browser->onLinkClicked(url);});
        return false;
    }
    return true;
}

void CDBrowser::onContentsSizeChanged(const QSize&)
{
    LOGDEB0("CDBrowser::onContentsSizeChanged\n");
    QString js =
        QString("window.scrollBy(%1, %2);").arg(m_savedscrollpos.x()).arg(m_savedscrollpos.y());
    runJS(js);
}

static QString base64_encode(QString string)
{
    return QByteArray(string.toUtf8()).toBase64();
}

void CDBrowser::startScript()
{
    m_view->page()->runJavaScript(m_js, [this](const QVariant& v) {
                                  if (m_vop) *m_vop = v;
                                  emit sig_script_ran();});
}
// See:
// https://stackoverflow.com/questions/45330481/qtwebengine-synchronously-execute-javascript-to-read-function-result
// The previous version did not call the exec through a timer and blocked if there were no user
// events (for some unclear reason actually).
void CDBrowser::runJS(const QString& js, QVariant* vop)
{
    LOGDEB0("CDBrowser::runJS: " << qs2utf8s(js).substr(0, 50) << "\n");
    m_jsbusy = true;
    m_js = js;
    m_vop = vop;
    QEventLoop loop;
    QObject::connect(this, SIGNAL(sig_script_ran()), &loop, SLOT(quit()));
    QTimer::singleShot(0, this, SLOT(startScript()));
    loop.exec();
    m_jsbusy = false;
    LOGDEB0("runJS: done: " << qs2utf8s(js).substr(0, 50) << "\n");
}

#else // WEBKIT->

void CDBrowser::onContentsSizeChanged(const QSize&)
{
    m_view->page()->mainFrame()->setScrollPosition(m_savedscrollpos);
}

void CDBrowser::runJS(const QString& js, QVariant*)
{
    LOGDEB0("CDBrowser::runJS: " << qs2utf8s(js).substr(0,80) << "\n");
    m_view->page()->mainFrame()->evaluateJavaScript(js);
}

#endif // WEBKIT

bool CDBrowser::event(QEvent *event)
{
    if (event->type() == QEvent::Gesture) {
        return gestureEvent(static_cast<QGestureEvent*>(event));
    }
    return QWidget::event(event);
}

bool CDBrowser::gestureEvent(QGestureEvent *event)
{
    if (QTapAndHoldGesture *taphold =
        dynamic_cast<QTapAndHoldGesture *>(event->gesture(Qt::TapAndHoldGesture))) {
        LOGDEB("CDBRowser: got taphold, state: " << taphold->state()  << " has hotspot " <<
               taphold->hasHotSpot() << "\n");
        if (taphold->state() == Qt::GestureFinished) {
            QPointF point = taphold->position();
            QPoint relpos = mapFromGlobal(QPoint(point.x(), point.y()));
            LOGDEB("CDBrowser: taphold location: (" << relpos.x() << ", "  << relpos.y() <<
                   ") my w " << width() << " h " << height() << "\n");
            // Avoid gestures in possible scrollbar areas.
            if ((relpos.x() > width() - 30) || (relpos.y() > height() - 20))
                return false;
            createPopupMenu(relpos);
        }
    }
    return true;
}

#if 0
    std::string butname;
    switch (m_lastbutton) {
    case Qt::LeftButton: butname = "left";break;
    case Qt::RightButton: butname = "right";break;
    case Qt::MiddleButton: butname = "middle";break;
    case Qt::BackButton: butname = "back";break;
    default: butname = lltodecstr(m_lastbutton);break;
    }
#endif

bool CDBrowser::eventFilter(QObject *object, QEvent *event)
{
    LOGDEB1("CDBrowser::eventFilter: event type " << event->type() << '\n');
    if (object != m_view)
        return false;
    switch (event->type()) {
    case QEvent::MouseButtonRelease:
    {
        const QMouseEvent *mouseEvent(static_cast<QMouseEvent*>(event));
        if (mouseEvent) {
            m_lastbutton = mouseEvent->button();
            if (m_lastbutton == Qt::BackButton) {
                back(0);
                return true;
            }
        }
    }
    break;
    case  QEvent::KeyPress:
    {
        const QKeyEvent *ev(static_cast<QKeyEvent*>(event));
        if((ev->key() == Qt::Key_C) && (ev->modifiers().testFlag(Qt::ControlModifier))) {
            qApp->clipboard()->setText(m_view->selectedText());
            return true;
        }
        /* We happen to know that the Key_X event value is the ascii character */
        unsigned char val =(unsigned char)ev->key();
        if (val >= 'A' && val <= 'Z') {
            QString surl("http://h/a");
            surl += QString(QChar(val));
            QUrl url(surl);
            onLinkClicked(url);
            return true;
        }
    }
    break;
    default:
        break;
    }
    return false;
}



void CDBrowser::appendHtml(const QString& elt_id, const QString& html)
{
    LOGDEB0("CDBrowser::appendHtml: elt_id [" << qs2utf8s(elt_id) << "]\n");
    LOGDEB1("CDBrowser::appendHtml: HTML [" << qs2utf8s(html) << "]\n");

#ifdef USING_WEBENGINE
    // With webengine, we can't access the qt object from the page, so
    // we do everything in js. The data is encoded as base64 to avoid
    // quoting issues, which induces complicated stuff for converting
    // to dom string, see containerscript.js for references.
    QString js = QString("var morehtml = '%1';\n"
                         "var decoded = Base64Decode(morehtml);\n")
        .arg(base64_encode(html));
    if (elt_id.isEmpty()) {
        js += QString("document.body.innerHTML += decoded;\n");
    } else {
        js += QString("document.getElementById('%1').innerHTML += decoded;\n").arg(elt_id);
    }
#else
    // With webkit we connect a Qt object with the browser and access
    // it directly from javascript.
    QWebFrame *mainframe = m_view->page()->mainFrame();
    StringObj morehtml(html);

    mainframe->addToJavaScriptWindowObject("morehtml", &morehtml, SCRIPTOWNERSHIP);
    QString js;
    if (elt_id.isEmpty()) {
        js = QString("document.body.innerHTML += morehtml.text");
    } else {
        js = QString("document.getElementById(\"%1\").innerHTML += morehtml.text").arg(elt_id);
    }
#endif
    runJS(js);
}

bool CDBrowser::newCds(int cdsidx)
{
    if (cdsidx > int(m_msdescs.size())) {
        LOGERR("CDBrowser::newCds: bad link index: " << cdsidx 
               << " cd count: " << m_msdescs.size() << "\n");
        return false;
    }
    MSRH ms = MSRH(new MediaServer(m_msdescs[cdsidx]));
    if (!ms) {
        LOGERR("CDBrowser::newCds: MediaServer connect failed\n");
        return false;
    }
    CDSH cds = ms->cds();
    if (!cds) {
        LOGERR("CDBrowser::newCds: null cds" << "\n");
        return false;
    }
    m_cds = std::shared_ptr<ContentDirectoryQO>(new ContentDirectoryQO(cds));
    m_sysUpdId = 0;
    connect(m_cds.get(), SIGNAL(systemUpdateIDChanged(int)), this, SLOT(onSysUpdIdChanged(int)));
    m_cds->srv()->getSearchCapabilities(m_searchcaps);
    emit sig_searchcaps_changed();
    return true;
}

void CDBrowser::onSysUpdIdChanged(int id)
{
    LOGDEB("CDBrowser::onSysUpdIdChanged: mine " << m_sysUpdId << "server" << id << "\n");

    if (!QSettings().value("monitorupdateid").toBool()) {
        return;
    }

    // 1st time is free
    if (!m_sysUpdId) {
        m_sysUpdId = id;
        return;
    }

    // We should try to use the containerUpdateIDs to make sure of
    // what needs to be done, instead of dumping the problem on the
    // user.
    if (m_sysUpdId != id) {

        m_sysUpdId = id;

        // CDSKIND_BUBBLE, CDSKIND_MEDIATOMB,
        // CDSKIND_MINIDLNA, CDSKIND_MINIM, CDSKIND_TWONKY
        ContentDirectory::ServiceKind kind = m_cds->srv()->getKind();
        switch (kind) {
            // Not too sure which actually invalidate their
            // tree. Pretty sure that Minim does not (we might just
            // want to reload the current dir).
        case ContentDirectory::CDSKIND_MINIDLNA: break;
        case ContentDirectory::CDSKIND_MEDIATOMB: break;
            // By default, don't do anything by default because some
            // cds keep changing their global updateid. We'd need to
            // check the containerUpdateID.
        case ContentDirectory::CDSKIND_MINIM:
        default: return;
        }

        QMessageBox::Button rep = 
            QMessageBox::question(0, "Upplay",
                                  tr("Content Directory Server state changed, "
                                     "some references may be invalid. (some "
                                     "playlist elements may be invalid too). "
                                     "<br>Reset browse state ?"),
                                  QMessageBox::Ok | QMessageBox::Cancel);
        if (rep == QMessageBox::Ok) {
            curpathClicked(1);
            m_sysUpdId = 0;
        }
    }
}

QPoint CDBrowser::scrollpos()
{
#ifdef USING_WEBENGINE
    QString js = "window.pageYOffset";
    QVariant v;
    runJS(js, &v);
    return QPoint(0, v.toInt());
#else
    return m_view->page()->mainFrame()->scrollPosition();
#endif
}

// This is connected to GUI signals. We need to separate the
// implementation in processOnLinkClicked() for counting automatic
// calls (when seeing a container with a single container entry for
// example, and to avoid unlimited recursion on a pathologic tree).
void CDBrowser::onLinkClicked(const QUrl &url)
{
    m_autoclickcnt = 0;
    if ((m_reader && m_reader->isEmitting()) || m_reaper) {
        return;
    }
    deleteReaders("onLinkClicked event");
#ifdef USING_WEBENGINE
    m_clickedurl = url;
    QTimer::singleShot(100, this, SLOT(processOnLinkClicked()));
#else
    processOnLinkClicked(url);
#endif
}

static std::string alphaId(char c)
{
    return std::string("alphaId_") + (char)::toupper(c);
}

void CDBrowser::processOnLinkClicked()
{
    processOnLinkClicked(m_clickedurl);
}
void CDBrowser::processOnLinkClicked(const QUrl &url)
{
    m_timer.stop();
    std::string scurl = qs2utf8s(url.toString());
    LOGDEB("CDBrowser::processOnLinkClicked: " << scurl << 
           " button " <<  m_lastbutton << " mid " << Qt::MiddleButton << "\n");
    // Get rid of http://
    if (scurl.find("http://h/") != 0) {
        LOGERR("CDBrowser::onLinkClicked: bad link ! : " << qs2utf8s(url.toString()));
        return;
    }
    scurl = scurl.substr(9);
    LOGDEB0("CDBrowser::onLinkClicked: corrected url: " << scurl << "\n");

    int what = scurl[0];

    switch (what) {

    case 'a':
    {
        char initial = scurl[1];
        auto it = m_alphamap.find(initial);
        if (it != m_alphamap.end()) {
            auto js = QString(
                "var elt = document.getElementById('%1');\n"
                "elt.scrollIntoView();\n"
                "r = elt.getBoundingClientRect();\n"
                // ClientRect {} bottom, height, left, right, top, width
                "if (r.top < 80) {window.scrollBy(0, -80);}\n"
                "document.getSelection().selectAllChildren(elt);\n"
                ).arg(u8s2qs(alphaId(it->first)));
            runJS(js);
        }
    }
    break;

    case 'C':
    {
        // Directory listing container link clicked: browse subdir.
        int idx = atoi(scurl.c_str() + 1);
        if (idx == -1) {
            // Click on header album entry. Equivalent to a click on the last curpath elt.
            curpathClicked(m_curpath.size()-1);
            return;
        }
        m_curpath.back().scrollpos = scrollpos();
        if (idx > (int)m_entries.size()) {
            LOGERR("onLinkClicked: bad idx: "<<idx<<" entries size: "<<m_entries.size()<<'\n');
            return;
        }
        UPnPClient::UPnPDirObject& e{m_entries[idx]};
        bool isPlaylist = e.getprop("upnp:class") == plContainerClass;
        if (m_lastbutton == Qt::MiddleButton) {
            // Open in new tab
            std::vector<CtPathElt> npath(m_curpath);
            npath.push_back(CtPathElt(e.m_id, e.m_title, isPlaylist, e));
            emit sig_browse_in_new_tab(u8s2qs(m_cds->srv()->getDeviceId()), npath);
        } else {
            browseContainer(e.m_id, e.m_title, isPlaylist, e);
        }
        return;
    }
    break;

    case 'I':
    {
        // Directory listing item link clicked: add to playlist
        unsigned int i = atoi(scurl.c_str()+1);
        if (i > m_entries.size()) {
            LOGERR("CDBrowser::onLinkClicked: bad objid index: " << i 
                   << " id count: " << m_entries.size() << "\n");
            return;
        }
        MetaDataList mdl;
        mdl.resize(1);
        if (!udirentToMetadata(&m_entries[i], &mdl[0], m_browsers->get_pl())) {
            QMessageBox::warning(0, "Upplay", "No compatible audio format");
        }
        emit sig_tracks_to_playlist(mdl);
    }
    break;

    case 'L':
    {
        // Click in curpath section.
        unsigned int i = atoi(scurl.c_str()+1);
        curpathClicked(i);
        return;
    }
    break;

    case 'S':
    {
        // Servers page server link click: browse clicked server root
        unsigned int cdsidx = atoi(scurl.c_str()+1);
        m_curpath.clear();
        m_curpath.push_back(CtPathElt("0", "(servers)", false));
        if (!newCds(cdsidx)) {
            return;
        }
        browseContainer("0", m_msdescs[cdsidx].friendlyName, false);
    }
    break;

    default:
        LOGERR("CDBrowser::onLinkClicked: bad link type: " << what << "\n");
        return;
    }
}

void CDBrowser::curpathClicked(unsigned int i)
{
    LOGDEB1("CDBrowser::curpathClicked: " << i << " pathsize " << m_curpath.size() << "\n");
    if (m_reader || m_reaper) {
        LOGDEB("CDBrowser::curpathClicked: already active\n");
        return;
    }
    if (i >= m_curpath.size()) {
        LOGERR("CDBrowser::curPathClicked: bad curpath index: " << i 
               << " path count: " << m_curpath.size() << "\n");
        return;
    }

    if (i == 0) {
        m_curpath.clear();
        m_msdescs.clear();
        m_cds.reset();
        initialPage(true);
    } else {
        CtPathElt e{m_curpath[i]};
        if (m_lastbutton == Qt::MiddleButton) {
            std::vector<CtPathElt> npath(m_curpath);
            npath.erase(npath.begin() + i, npath.end());
            npath.push_back(e);
            emit sig_browse_in_new_tab(u8s2qs(m_cds->srv()->getDeviceId()), npath);
        } else {
            m_curpath.erase(m_curpath.begin() + i, m_curpath.end());
            if (e.searchStr.empty()) {
                browseContainer(e.objid, e.title, e.isPlaylist, e.dirent, e.scrollpos);
            } else {
                search(e.objid, e.searchStr, e.scrollpos);
            }
        }
    }
}

int CDBrowser::serverFromUDN(const std::string& UDN)
{
    for (unsigned int i = 0; i < m_msdescs.size(); i++) {
        if (m_msdescs[i].UDN == UDN) {
            return int(i);
        }
    }
    return -1;
}

// This is called for browsing a specific container in a newly created
// tab.  We let initialPage handle the hard work.
void CDBrowser::browseInNewTab(QString UDN, std::vector<CtPathElt> path)
{
    LOGDEB1("CDBrowser::browseinnewtab: " << qs2utf8s(UDN) << "\n");

    m_curpath = path;
    m_initUDN = UDN;
    m_timer.stop();
    m_timer.start(0);
    return;
}

// Re-browse (because sort criteria changed, or other prefs)
// if m_cds is not set, let initialPage() do its thing
void CDBrowser::refresh()
{
    LOGDEB("CDBrowser::refresh\n");
    setStyleSheet(Style::use_dark_colors(), false);
    if (m_cds && m_curpath.size() >= 1) {
        curpathClicked(int(m_curpath.size() - 1));
    } else {
        m_forceRedisplay = true;
    }
}

void CDBrowser::updateFormatParams()
{
    QSettings settings;
    m_formatparams.artistWithAlbum = settings.value("showartwithalb", true).toBool();
    m_formatparams.artistWithAlbumBytes = settings.value("artwithalblen", 20).toInt();
    m_formatparams.coverWithAlbum = settings.value("showcoverwithalb", false).toBool();
    m_formatparams.coverWithAlbumWidth = settings.value("coverwithalbwidth", 20).toInt();
    m_formatparams.albumsAsCovers = settings.value("albumsascovers", false).toBool();
    m_formatparams.albumsAsCoversWidth = settings.value("albumsascoverswidth", 100).toInt();
    m_formatparams.genericAlbumImageURI = QString("qrc:/icons/generic-cover.jpg");
    m_formatparams.scalemultiplier = Style::get_ui_scale();
    if (settings.contains("autotraversemaxdepth")) {
        m_autotravmaxdepth = settings.value("autotraversemaxdepth").toInt();
    }
    if (settings.value("truncateartistindir").toBool()) {
        m_maxartlen = settings.value("truncateartistlen").toInt();
    } else {
        m_maxartlen = 0;
    }
}

void CDBrowser::browseContainer(std::string ctid, std::string cttitle, bool ispl,
                                const UPnPClient::UPnPDirObject& dirent, QPoint scrollpos)
{
    LOGDEB("CDBrowser::browseContainer: " << " ctid " << ctid << "\n");
    if (m_reader || m_reaper) {
        LOGDEB("CDBrowser::browseContainer: already active\n");
        return;
    }
    m_residhtml = "";
    m_alphamsg = "Reading...";

    updateFormatParams();

    emit sig_now_in(this, u8s2qs(cttitle));

    m_savedscrollpos = scrollpos;
    if (!m_cds) {
        LOGERR("CDBrowser::browseContainer: server not set" << "\n");
        return;
    }

    // Keep before clearing the entries because dirent maybe a ref into current array
    m_curpath.push_back(CtPathElt(ctid, cttitle, ispl, dirent));

    m_entries.clear();

    initContainerHtml();
    
    m_reader = new CDBrowseQO(m_cds->srv(), ctid, std::string(), this);

    // Note: using Qt::BlockingQueued connection here is necessary to avoid recursive calls to
    // onSliceAvailable() when it calls appendHtml() and runJS() which give back control to the
    // event loop. onSliceAvailable()->runJS()->eventloop->usenextqueuedslice->onSliceAvailable()
    connect(m_reader, SIGNAL(sliceAvailable(UPnPClient::UPnPDirContent*)),
            this, SLOT(onSliceAvailable(UPnPClient::UPnPDirContent *)),
            Qt::BlockingQueuedConnection);
    connect(m_reader, SIGNAL(done(int)), this, SLOT(onBrowseDone()));
    m_reader->start();
}

void CDBrowser::search(const std::string& iss)
{
    search(m_curpath.back().objid, iss);
}

void CDBrowser::search(const std::string& objid, const std::string& iss, QPoint scrollpos)
{
    deleteReaders("search");
    if (iss.empty())
        return;
    if (!m_cds) {
        LOGERR("CDBrowser::search: server not set" << "\n");
        return;
    }

    updateFormatParams();

    m_savedscrollpos = scrollpos;
    m_entries.clear();
    m_curpath.push_back(CtPathElt(objid, "(search)", false, UPnPClient::UPnPDirObject(), iss));
    initContainerHtml(iss);

    m_reader = new CDBrowseQO(m_cds->srv(), m_curpath.back().objid, iss, this);

    // Note: using Qt::BlockingQueued connection here is necessary to avoid recursive calls to
    // onSliceAvailable() when it calls appendHtml() and runJS() which give back control to the
    // event loop. onSliceAvailable()->runJS()->eventloop->usenextqueuedslice->onSliceAvailable()
    connect(m_reader, SIGNAL(sliceAvailable(UPnPClient::UPnPDirContent*)),
            this, SLOT(onSliceAvailable(UPnPClient::UPnPDirContent *)),
            Qt::BlockingQueuedConnection);
    connect(m_reader, SIGNAL(done(int)), this, SLOT(onBrowseDone()));
    m_reader->start();
}


void CDBrowser::deleteReaders(const QString& who)
{
    Q_UNUSED(who);
    LOGDEB0("deleteReaders(): from " << qs2utf8s(who) << "\n");
    if (m_reader) {
        m_reader->setCancel();
        m_reader->wait();
        delete m_reader;
        m_reader = 0;
    }
    if (m_reaper) {
        m_reaper->setCancel();
        m_reaper->wait();
        delete m_reaper;
        m_reaper = 0;
    }
    alphaMessage("");
    // Give a chance to queued signals (a thread could have queued something before/while we're
    // cancelling it). Else, typically onSliceAvailable could be called at a later time and mess the
    // display.
    qApp->processEvents(QEventLoop::AllEvents, 10);
}

// Note that we used to use a search for the title to move to the right place. We now use id
// attributes, and the title part of the map is not useful any more.
bool CDBrowser::updateAlphamap(char& curinitial, const std::string& tt)
{
    char ninit = ::toupper(tt[0]);
    if (ninit != curinitial && ninit >= 'A' && ninit <= 'Z') {
        if (ninit < curinitial) {
            // Unexpected sort. Update the map anyway in case the user has the dont care pref set
            m_alphamap[ninit] = tt;
            curinitial = ninit;
            return false;
        }
        m_alphamap[ninit] = tt;
        curinitial = ninit;
    }
    return true;
}

// There are 2 instances of the alphalink because of the fixed header
// variable height double div trick (see cdb_html.cpp and the css).
static const char *setalphalinks_js =
    "var x = document.getElementsByClassName('alphalist');\n"
    "var i;\n"
    "var l = x.length;\n"
    "for (i = 0; i < l; i++) {\n"
    "  x[i].style.visibility = \"%1\";\n"
    "  x[i].outerHTML = \"%2\";\n"
    "}\n";

void CDBrowser::maybeShowAlphamap(unsigned int nct)
{
    LOGDEB1("CDBrowser::maybeShowAlphamap\n");
    if (nct >= 30 && m_alphamap.size() > 1) {
        std::string initials;
        for (auto& ent: m_alphamap) {
            if  (ent.first >= 'A' && ent.first <= 'Z') {
                initials += ent.first;
            }
        }
        runJS(QString(setalphalinks_js).arg("visible").arg(alphalinks(initials)));
    } else {
        runJS(QString(setalphalinks_js).arg("hidden").arg(alphalinks("&nbsp;", true)));
    }
}

// Display message in alphalist area
void CDBrowser::alphaMessage(const std::string& msg)
{
    runJS(QString(setalphalinks_js).arg("visible").arg(alphalinks(msg, true)));
}

// Receive data from the mediaserver through the reader. If we are not displaying a covers grid, we
// use the new upnpporder property from libupnpp to exactly preserve the original order instead of
// listing containers first. In the covers grid case, we call another function.
void CDBrowser::onSliceAvailable(UPnPDirContent *dc)
{
    LOGDEB0("CDBrowser::onSliceAvailable\n");
    if (!m_reader) {
        LOGERR("CDBrowser::onSliceAvailable: no reader\n");
        // Cancelled.
        delete dc;
        return;
    }
    m_alphamsg += ".";
    alphaMessage(m_alphamsg);

    if (m_formatparams.albumsAsCovers) {
        onSliceAvailableCovers(dc);
        delete dc;
        return;
    }

    auto basesize = m_entries.size();
    auto addsize = dc->m_containers.size() + dc->m_items.size();
    m_entries.reserve(basesize + addsize);
#if LIBUPNPP_AT_LEAST(0,26,7)
    m_entries.resize(basesize + addsize);
#endif

    QString html;
    std::string sidx;
    
    for (auto& entry: dc->m_containers) {
        LOGDEB1("Container: " << entry.m_title << '\n');
#if LIBUPNPP_AT_LEAST(0,26,7)
        entry.getprop("upnpporder", sidx);
        auto idx = atoi(sidx.c_str());
        if (basesize + idx < m_entries.size()) {
            m_entries[basesize + idx] = entry;
        }
#else
        m_entries.push_back(entry);
#endif
    }

    for (const auto& entry : dc->m_items) {
        LOGDEB1("Item: " << entry.m_title << '\n');
#if LIBUPNPP_AT_LEAST(0,26,7)
        entry.getprop("upnpporder", sidx);
        auto idx = atoi(sidx.c_str());
        if (basesize + idx < m_entries.size()) {
            m_entries[basesize + idx] = entry;
        }
#else
        m_entries.push_back(entry);
#endif
    }

    bool noalb{false};
    auto& pardirent =  m_curpath.back().dirent;
    auto upnpclass = pardirent.getprop("upnp:class");
    if (upnpclass == albContainerClass || upnpclass == plContainerClass) {
        if (basesize == 0) {
            // First slice: possibly create album header
            // The parent is an album: add a container entry for it at the top. The -1 will tell
            // CTToHtml to display it differently.
            html += CTToHtml(-1, pardirent);
        }
        // Always for an album: no album title column
        noalb = upnpclass == albContainerClass;
    }
    
    for (int i = basesize; i < (int)m_entries.size(); i++) {
        if (m_entries[i].m_type == UPnPDirObject::container) {
            html += CTToHtml(i, m_entries[i], alphaId(m_entries[i].m_title[0]));
        } else {
            html += ItemToHtml(i, m_entries[i], noalb);
        }
    }
    // We only insert the first time. Going on sending HTML slices for big
    // lists would be very slow (quadratic). We'll send the rest of the HTML (m_residhtml) when
    // we're done reading.
    // Note: appendHtml() calls runJS which will give control back to the event loop. If there are
    // queued slice signals this will cause a *recursive* call to this method, and at the very
    // least, the order of the list will be garbled. The only way to avoid this is to use a
    // Qt::BlockingQueuedConnection connection type for the slice signal
    if (basesize == 0)
        appendHtml("entrylist", html);
    else
        m_residhtml += html;
    delete dc;
}

// Receive directory data, covers grid case.
void CDBrowser::onSliceAvailableCovers(UPnPDirContent *dc)
{
    LOGDEB0("CDBrowser::onSliceAvailableCovers\n");
    bool firstslice = m_entries.empty();
        
    QString html;

    bool noalb{false};
    auto& pardirent =  m_curpath.back().dirent;
    auto upnpclass = pardirent.getprop("upnp:class");
    if (upnpclass == albContainerClass || upnpclass == plContainerClass) {
        if (firstslice) {
            // First slice: possibly create album header
            // The parent is an album: add a container entry for it at the top. The -1 will tell
            // CTToHtml to display it differently.
            html += CTToHtml(-1, pardirent);
        }
        // Always for an album: no album title column
        noalb = upnpclass == albContainerClass;
    }

    for (auto& entry: dc->m_containers) {
        m_entries.push_back(entry);
        html += CTToHtml(int(m_entries.size()-1), entry, alphaId(entry.m_title[0]));
    }

    appendHtml("thumbnailsdiv", html);
    html = "";

    
    for (const auto& entry : dc->m_items) {
        m_entries.push_back(entry);
        html += ItemToHtml(int(m_entries.size()-1), entry, noalb);
    }
    if (firstslice)
        appendHtml("entrylist", html);
    else
        m_residhtml += html;
}

class DirObjCmp {
public:
    DirObjCmp(const std::vector<std::string>& crits) : m_crits(crits) {}
    bool operator()(const UPnPDirObject& o1, const UPnPDirObject& o2) {
        int rel;
        std::string s1, s2;
        for (unsigned int i = 0; i < m_crits.size(); i++) {
            const std::string& crit = m_crits[i];
            rel = dirobgetprop(o1, crit, s1).compare(dirobgetprop(o2, crit, s2));
            if (rel < 0)
                return true;
            else if (rel > 0)
                return false;
        }
        return false;
    }
    static const std::string& dirobgetprop(const UPnPDirObject& o, const std::string& nm,
                                           std::string& storage) {
        if (!nm.compare("dc:title")) {
            return o.m_title;
        } else if (!nm.compare("uri")) {
            if (o.m_resources.size() == 0)
                return nullstr;
            return o.m_resources[0].m_uri;
        } 
        const std::string& prop = o.getprop(nm);
        if (!nm.compare("upnp:originalTrackNumber")) {
            storage = prop;
            leftzeropad(storage, 10);
            return storage;
        } else if (!nm.compare("upplay:ctpath")) {
            LOGDEB1("TTPATH: " << RecursiveReaper::ttpathPrintable(prop).c_str() << "\n");
            return prop;
        } else {
            return prop;
        }
    }
    std::vector<std::string> m_crits;
    static std::string nullstr;
};
std::string DirObjCmp::nullstr;

void CDBrowser::onBrowseDone()
{
    LOGDEB0("CDBrowser::onBrowseDone: " << m_entries.size() << " entries. Depth: " <<
            m_curpath.size() << " autotravdepth " << m_autotravmaxdepth << "\n");

#ifdef USING_WEBENGINE
    if (m_jsbusy) {
        QTimer::singleShot(100, this, SLOT(onBrowseDone()));
        return;
    }
#endif

    bool inPlaylist = m_curpath.back().isPlaylist;
    if (!m_reader) {
        LOGDEB("CDBrowser::onBrowseDone(int) no reader: cancelled\n");
        return;
    }
    m_reader->wait();
    auto readerKind = m_reader->getKind();
    auto readerObjid = m_reader->getObjid();
    deleteReaders("onBrowseDone");

    // If we find a single entry, initiate automatic descent. This is mostly to skip in the common
    // case of a single entry at an upmpdcli media server top level (e.g. single Qobuz or uprcl
    // entry). We now limit this to this top level (depth < 3), to avoid skipping, e.g. a single
    // album entry with custom art deeper down. Maybe be make the depth a parameter at some point?
    if ((int)m_curpath.size() <= m_autotravmaxdepth && m_autoclickcnt++ < 3 &&
        m_entries.size() == 1 && m_entries[0].m_type == UPnPDirObject::container) {
        alphaMessage(m_alphamsg);
        QUrl cturl(QString("http://h/C%1").arg(0));
        processOnLinkClicked(cturl);
        return;
    }

    std::vector<std::string> sortcrits;
    int sortkind = CSettingsStorage::getInstance()->getSortKind();
    if (sortkind == CSettingsStorage::SK_MINIMFNORDER && m_curpath.back().searchStr.empty() &&
        readerKind == ContentDirectory::CDSKIND_MINIM && 
        readerObjid.compare(0, minimFoldersViewPrefix.size(), minimFoldersViewPrefix) == 0) {
        sortcrits.push_back("uri");
    } else if (sortkind == CSettingsStorage::SK_CUSTOM) {
        QStringList qcrits = CSettingsStorage::getInstance()->getSortCrits();
        for (const auto& crit : qcrits) {
            sortcrits.push_back(qs2utf8s(crit));
        }
    }
    int nct = 0;
    char curinitial = 0;
    m_alphamap.clear();
    if (!inPlaylist && !sortcrits.empty()) {
        m_alphamsg = "Sorting/Displaying...";
        alphaMessage(m_alphamsg);
        m_alphamsg = "";
        // Sorting locally. Have to re-generate the HTML produced by onSliceAvalaible() now that we
        // have the full list.
        DirObjCmp cmpo(sortcrits);
        sort(m_entries.begin(), m_entries.end(), cmpo);
        initContainerHtml();

        QString tmbhtml, lsthtml;
        bool noalb{false};
        // Possible album header
        {
            auto& pardirent =  m_curpath.back().dirent;
            auto upnpclass = pardirent.getprop("upnp:class");
            if (upnpclass == albContainerClass || upnpclass == plContainerClass) {
                // The parent is an album: add a container entry for it at the top. The -1 will tell
                // CTToHtml to display it differently.
                lsthtml += CTToHtml(-1, pardirent);
                noalb = upnpclass == albContainerClass;
            }
        }
        for (int i = 0; i < (int)m_entries.size(); i++) {
            if (m_entries[i].m_type == UPnPDirObject::container) {
                std::string sortval = DirObjCmp::dirobgetprop(m_entries[i], sortcrits[0], sortval);
                if (sortval.empty()) { // ??
                    sortval = "'";
                }
                updateAlphamap(curinitial, sortval);
                if (m_formatparams.albumsAsCovers) {
                    tmbhtml += CTToHtml(i, m_entries[i], alphaId(sortval[0]));
                } else {
                    lsthtml += CTToHtml(i, m_entries[i], alphaId(sortval[0]));
                }
                nct++;
            } else {
                lsthtml += ItemToHtml(i, m_entries[i], noalb);
            }
        }
        if (!tmbhtml.isEmpty()) {
            appendHtml("thumbnailsdiv", tmbhtml);
        }
        if (!lsthtml.isEmpty()) {
            appendHtml("entrylist", lsthtml);
        }
    } else {
        // No local sorting. The HTML generation was done by onSliceAvailable().
        m_alphamsg = "Displaying...";
        alphaMessage(m_alphamsg);
        m_alphamsg = "";
        if (!m_residhtml.isEmpty()) {
            appendHtml("entrylist", m_residhtml);
            m_residhtml = "";
        }
        
        // Alphamap: the probable thing is that containers are sorted by title, but maybe they are
        // not (sorted by artist, or by albumsort, who knows). So, if not disabled by a pref, we
        // check the sort and avoid generating the jump map if things appear to be out or order.
        QSettings settings;
        bool badsortnomap = settings.value("browserbadsortnoalphamap").toBool();
        for (unsigned i = 0; i < m_entries.size(); i++) {
            if (m_entries[i].m_type == UPnPDirObject::container) {
                if (!updateAlphamap(curinitial, m_entries[i].m_title) && badsortnomap) {
                    m_alphamap.clear();
                    break;
                }
                nct++;
            }
        }
    }
    maybeShowAlphamap(nct);

#ifdef USING_WEBENGINE
    // Don't see a way to set the scroll position in the page from within WEBENGINE, thus do it by
    // javascript
    QString js =
        QString("window.scrollTo(%1, %2);").arg(m_savedscrollpos.x()).arg(m_savedscrollpos.y());
    runJS(js);
#else
    m_view->page()->mainFrame()->setScrollPosition(m_savedscrollpos);
#endif
}


void CDBrowser::initialPage(bool redisplay)
{
    if (m_forceRedisplay) {
        redisplay = true;
        m_forceRedisplay = false;
    }
    LOGDEB0("CDBrowser::initialPage. redisplay " << redisplay <<
            " m_initUDN [" << qs2utf8s(m_initUDN) << "] curpath size " << m_curpath.size() << "\n");

    deleteReaders("initialPage");
    emit sig_now_in(this, tr("Servers"));
    m_searchcaps.clear();
    emit sig_searchcaps_changed();

    std::vector<UPnPDeviceDesc> msdescs;
    if (!MediaServer::getDeviceDescs(msdescs)) {
        LOGERR("CDBrowser::initialPage: getDeviceDescs failed" << "\n");
    }
    if (msdescs.size() == 0) {
        LOGDEB("CDBrowser::initialPage: calling setHtml with 'looking for'\n");
        mySetHtml(initialServersPage());
        m_timer.start(1000);
        return;
    }
        
    LOGDEB1("CDBrowser::initialPage: " << msdescs.size() << " servers\n");

    // Check if servers list changed
    bool same = msdescs.size() == m_msdescs.size();
    if (same) {
        for (unsigned i = 0; i < msdescs.size(); i++) {
            if (msdescs[i].UDN.compare(m_msdescs[i].UDN)) {
                same = false;
                break;
            }
        }
    }
    if (!same) {
        LOGDEB("CDBrowser::initialPage: servers list changed\n");
        m_msdescs = msdescs;
    }
    // If the list did not change and a redisplay was not requested,
    // do nothing
    if (same && !redisplay && m_initUDN.isEmpty()) {
        LOGDEB1("CDBrowser::initialPage: not redisplaying\n");
        m_timer.start(1000);
        return;
    }

    // Displaying stuff

    if (!m_initUDN.isEmpty() && m_curpath.size() > 1) {
        LOGDEB("CDBrowser::initialPage: auto-navigate to server\n");
        // Called from browseinnewtab, with specified server and path
        // (either midclick or restoring from stored state): show
        // appropriate container
        int i = serverFromUDN(qs2utf8s(m_initUDN));
        LOGDEB1("initialPage: serverFromUDN returned " << i << "\n");
        if (i >= 0) {
            if (newCds(i)) {
                m_initUDN = "";
                curpathClicked(int(m_curpath.size() - 1));
                return;
            }
        }
    }

    // No initUDN or not found: show servers list
    mySetHtml(emptyServersPage());
    for (unsigned i = 0; i < msdescs.size(); i++) {
        // Alphamap does not really work for servers, which are not in
        // alphabetic order at the moment
        // updateAlphamap(curinitial, msdescs[i].friendlyName);
        appendHtml("", DSToHtml(i, msdescs[i]));
    }
    m_timer.start(1000);
}

enum PopupMode {
    PUP_ADD,
    PUP_ADD_ALL,
    PUP_ADD_FROMHERE,
    PUP_BACK,
    PUP_OPEN_IN_NEW_TAB,
    PUP_RAND_PLAY_TRACKS,
    PUP_RAND_PLAY_GROUPS,
    PUP_RAND_STOP,
    PUP_SORT_ORDER,
    PUP_COPY_URL,
    PUP_AS_CSV,
    PUP_SHOW_META,
};

#ifdef USING_WEBENGINE

#if (QT_VERSION >= QT_VERSION_CHECK(6, 2, 0))
void CDBrowser::onNewWindowRequested(QWebEngineNewWindowRequest& request)
{
    QUrl url = request.requestedUrl();
    m_lastbutton = Qt::MiddleButton;
    LOGDEB1("CDBrowser::onNewWindowRequested: url " << qs2utf8s(url.toString()) << '\n');
    QTimer::singleShot(0, [this, url]() {this->onLinkClicked(url);});
}
#endif

#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
#define QTSKIPEMPTYPARTS QString::SkipEmptyParts
#else
#define QTSKIPEMPTYPARTS Qt::SkipEmptyParts
#endif

void CDBrowser::onPopupJsDone(const QVariant &jr)
{
    QString qs(jr.toString());
    LOGDEB("onPopupJsDone: parameter: " << qs2utf8s(qs) << "\n");
    QStringList qsl = qs.split("\n", QTSKIPEMPTYPARTS);
    for (int i = 0 ; i < qsl.size(); i++) {
        int eq = qsl[i].indexOf("=");
        if (eq > 0) {
            QString nm = qsl[i].left(eq).trimmed();
            QString value = qsl[i].right(qsl[i].size() - (eq+1)).trimmed();
            if (!nm.compare("objid")) {
                m_popupobjid = qs2utf8s(value);
            } else if (!nm.compare("title")) {
                m_popupobjtitle = qs2utf8s(value);
            } else if (!nm.compare("objidx")) {
                m_popupidx = value.toInt();
            } else if (!nm.compare("otype")) {
                m_popupotype = qs2utf8s(value);
            } else if (!nm.compare("ispl")) {
                m_popupispl = value.toInt();
            } else {
                LOGERR("onPopupJsDone: unknown key: " << qs2utf8s(nm) << "\n");
            }
        }
    }
    LOGDEB1("onPopupJsDone: class [" << m_popupotype <<
            "] objid [" << m_popupobjid <<
            "] title [" <<  m_popupobjtitle <<
            "] objidx [" << m_popupidx << "]\n");
    doCreatePopupMenu();
}
#endif

void CDBrowser::createPopupMenu(const QPoint& pos)
{
    if (m_browsers->insertActive()) {
        LOGDEB("CDBrowser::createPopupMenu: no popup: insert active\n");
        return;
    }
    LOGDEB("CDBrowser::createPopupMenu\n");
    m_popupobjid = m_popupobjtitle = m_popupotype = "";
    m_popupidx = -1;
    m_popupos = pos;
    
#ifdef USING_WEBENGINE
    QString js("window.locDetails;");
    CDWebPage *mypage = dynamic_cast<CDWebPage*>(m_view->page());
    mypage->runJavaScript(js, [this](const QVariant &v) { onPopupJsDone(v); });
#else
    QWebHitTestResult htr = m_view->page()->mainFrame()->hitTestContent(pos);
    if (htr.isNull()) {
        LOGDEB("CDBrowser::createPopupMenu: no popup: no hit\n");
        return;
    }
    QWebElement el = htr.enclosingBlockElement();
    while (!el.isNull() && !el.hasAttribute("objid"))
        el = el.parent();
    if (!el.isNull()) {
        m_popupobjid = qs2utf8s(el.attribute("objid"));
        m_popupobjtitle = qs2utf8s(el.toPlainText());
        if (el.hasAttribute("objidx"))
            m_popupidx = el.attribute("objidx").toInt();
        else
            m_popupidx = -1;
        if (el.hasAttribute("ispl"))
            m_popupispl = el.attribute("ispl").toInt();
        else
            m_popupispl = false;
        m_popupotype = qs2utf8s(el.attribute("class"));
        LOGDEB("Popup: " << " class " << m_popupotype << " objid " << 
               m_popupobjid << "\n");
    }
    doCreatePopupMenu();
#endif
}

void CDBrowser::popupaddaction(QMenu *popup, const QString& txt, int value)
{
    QAction *act = new QAction(txt, this);
    act->setData(value);
    popup->addAction(act);
}

void CDBrowser::doCreatePopupMenu()
{
    LOGDEB1("CDBrowser::doCreatePopupMenu: objid [" << m_popupobjid << "] title [" <<
            m_popupobjtitle << "] type [" <<  m_popupotype << "] IDX " << m_popupidx <<
            " ISPL " << m_popupispl << '\n');

    if (m_browsers->insertActive() || m_reader || m_reaper) {
        LOGINF("CDBrowser::createPopupMenu: no popup: active read\n");
        return;
    }

    if (m_curpath.size() == 0) {
        // No popup on servers page at the moment.
        return;
    }

    QMenu *popup = new QMenu(this);

    // Click in blank area, or no playlist
    if (m_popupobjid.empty() || (!m_browsers || !m_browsers->have_playlist())) {
        popupaddaction(popup, tr("Back"), PUP_BACK);
        if (m_browsers->randPlayActive()) {
            popupaddaction(popup, tr("Stop random play"), PUP_RAND_STOP);
        }
        popupaddaction(popup, tr("Save track list as CSV"), PUP_AS_CSV);
        popup->connect(popup, SIGNAL(triggered(QAction *)), this, SLOT(popupOther(QAction *)));
        popup->popup(mapToGlobal(m_popupos));
        return;
    }

    if (m_popupidx == -1 && m_popupotype.compare("container")) {
        // Click in curpath. All path elements should be containers !
        LOGERR("doCreatePopupMenu: not container and no popupidx == -1 ??\n");
        return;
    }

    // Either item or container can be sent to playlist. Different kind of add though, see below
    popupaddaction(popup, tr("Send to playlist"), PUP_ADD);
    if (m_popupotype == "item") {
        popupaddaction(popup, tr("Send all to playlist"), PUP_ADD_ALL);
        popupaddaction(popup, tr("Send all from here to playlist"), PUP_ADD_FROMHERE);
        popupaddaction(popup, tr("Copy URL"), PUP_COPY_URL);
        popupaddaction(popup, tr("Show Metadata"), PUP_SHOW_META);
        popup->connect(popup, SIGNAL(triggered(QAction *)), this, SLOT(simpleAdd(QAction *)));
    } else if (m_popupotype == "container") {
        popupaddaction(popup, tr("Show Metadata"), PUP_SHOW_META);
        popupaddaction(popup, tr("Open in new tab"), PUP_OPEN_IN_NEW_TAB);
        popupaddaction(popup, tr("Random play by tracks"), PUP_RAND_PLAY_TRACKS);
        popupaddaction(popup, tr("Random play by groups"), PUP_RAND_PLAY_GROUPS);
        popup->connect(popup, SIGNAL(triggered(QAction *)), this, SLOT(recursiveAdd(QAction *)));
    } else {
        // ??
        LOGERR("CDBrowser::createPopup: obj type neither ct nor it: " << m_popupotype << "\n");
        return;
    }

    if (m_browsers->randPlayActive()) {
        popupaddaction(popup, tr("Stop random play"), PUP_RAND_STOP);
    }
    popupaddaction(popup, tr("Sort order"), PUP_SORT_ORDER);
    popupaddaction(popup, tr("Save track list as CSV"), PUP_AS_CSV);
    
    popup->popup(mapToGlobal(m_popupos));
}

void CDBrowser::back(QAction *)
{
    if (m_curpath.size() >= 2)
        curpathClicked(int(m_curpath.size() - 2));
}

bool CDBrowser::popupOther(QAction *act)
{
    m_popupmode = act->data().toInt();
    switch (m_popupmode) {
    case PUP_BACK:
        back(0);
        break;
    case PUP_AS_CSV:
        saveAsCSV();
        break;
    case PUP_RAND_STOP:
        emit sig_rand_stop();
        break;
    case PUP_SORT_ORDER:
        emit sig_sort_order();
        break;
    default:
        return false;
    }
    return true;
}

static void pup_show_meta(UPnPDirObject &e)
{
    auto html = metaDataToHtml(nullptr, e, true);
    auto d = new BrowserDialog();
    auto browser = d->browser;
    browser->setText(html);
    browser->setReadOnly(true);
    browser->setOpenExternalLinks(true);
    d->show();
    return;
}

// Do stuff for a single track or a section of the current container. This may be triggered by a
// link click or a popup on an item entry
void CDBrowser::simpleAdd(QAction *act)
{
    LOGDEB1("CDBrowser::simpleAdd\n");
    if (popupOther(act)) {
        // Not for us
        return;
    }
    if (m_popupidx < 0 || m_popupidx > int(m_entries.size())) {
        LOGERR("CDBrowser::simpleAdd: bad obj index: " << m_popupidx
               << " id count: " << m_entries.size() << "\n");
        return;
    }
    MetaDataList mdl;
    unsigned int starti = 0;
    switch (m_popupmode) {
    case PUP_COPY_URL:
    {
        UPnPDirObject& e(m_entries[m_popupidx]);
        if (e.m_resources.empty()) {
            
        }
        QString uri = QString::fromLocal8Bit(e.m_resources[0].m_uri.c_str());
        QApplication::clipboard()->setText(uri, QClipboard::Selection);
        QApplication::clipboard()->setText(uri, QClipboard::Clipboard);
        return;
    }
    case PUP_SHOW_META:
    {
        pup_show_meta(m_entries[m_popupidx]);
        return;
    }
    case PUP_ADD_FROMHERE: 
        starti = m_popupidx;
        /* FALLTHROUGH */
    case PUP_ADD_ALL:
    {
        bool allok{true};
        for (unsigned int i = starti; i < m_entries.size(); i++) {
            if (m_entries[i].m_type == UPnPDirObject::item && 
                m_entries[i].m_iclass == UPnPDirObject::ITC_audioItem_musicTrack) {
                mdl.resize(mdl.size()+1);
                if (!udirentToMetadata(&m_entries[i], &mdl[mdl.size()-1], m_browsers->get_pl())) {
                    allok = false;
                }
            }
        }
        if (!allok) {
            QMessageBox::warning(0, "Upplay",
                                 "Some tracks could not be loaded: no compatible audio format");
        }
    }        
    break;
    default:
        mdl.resize(1);
        if (!udirentToMetadata(&m_entries[m_popupidx], &mdl[0], m_browsers->get_pl())) {
            QMessageBox::warning(0, "Upplay", "No compatible audio format");
        }
    }

    emit sig_tracks_to_playlist(mdl);
}

void CDBrowser::saveAsCSV()
{
    std::string csv;
    for (const auto& e : m_entries) {
        std::vector<std::string> fields;
        std::string artist;
        e.getprop("upnp:artist", artist);
        if (e.m_type == UPnPDirObject::container) {
            fields = std::vector<std::string>{"CT", "", e.m_title, artist, "", ""};
        } else {
            std::string tno;
            e.getprop("upnp:originalTrackNumber", tno);
            std::string alb;
            e.getprop("upnp:album", alb);
            std::string sdur;
            e.getrprop(0, "duration", sdur);
            int seconds = upnpdurationtos(sdur);
            fields = std::vector<std::string>{"IT", tno, e.m_title, artist, alb,
                                    lltodecstr(seconds)};
        }
        auto s = stringsToCSV(fields);
        csv += s + "\n";
    }
    QString fileName = MedocUtils::FileDialog::getSaveFileName(this, tr("Save File"));
    if (!fileName.isEmpty()) {
        std::string fn = qs2utf8s(fileName);
        int fd;
        if ((fd = ::open(fn.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0644)) < 0) {
            QMessageBox::warning(0, "Upplay", "can't open/create");
            return;
        }
        int ret = ::write(fd, csv.c_str(), int(csv.size()));
        ((void)ret);
        ::close(fd);
    }
}

std::string CDBrowser::getServerUDN()
{
    if (m_cds) {
        return m_cds->srv()->getDeviceId();
    }
    return std::string();
}

// Recursive add. This is triggered popup on a container
void CDBrowser::recursiveAdd(QAction *act)
{
    LOGDEB1("CDBrowser::recursiveAdd\n");

    deleteReaders("recursiveAdd");
    
    if (popupOther(act)) {
        // Not for us
        return;
    }

    if (!m_cds) {
        LOGERR("CDBrowser::recursiveAdd: server not set\n") ;
        return;
    }

    if (m_popupmode == PUP_OPEN_IN_NEW_TAB) {
        std::vector<CtPathElt> npath(m_curpath);
        npath.push_back(CtPathElt(m_popupobjid, m_popupobjtitle, m_popupispl));
        emit sig_browse_in_new_tab(u8s2qs(m_cds->srv()->getDeviceId()), npath);
        return;
    } else if (m_popupmode == PUP_SHOW_META) {
        if (m_popupidx == -1) {
            pup_show_meta(m_curpath.back().dirent);
        } else if (m_popupidx < (int)m_entries.size()) {
            pup_show_meta(m_entries[m_popupidx]);
        }
        return;
    }
    
    updateFormatParams();
    
    m_browsers->setInsertActive(true);
    ContentDirectory::ServiceKind kind = m_cds->srv()->getKind();
    if (kind == ContentDirectory::CDSKIND_MINIM && m_popupmode != PUP_RAND_PLAY_GROUPS) {
        // Use search() rather than a tree walk for Minim, it is much more efficient, except for
        // rand play albums, where we want to preserve the paths (for discrimination)
        UPnPDirContent dirbuf;
        std::string ss("upnp:class = \"object.item.audioItem.musicTrack\"");
        int err = m_cds->srv()->search(m_popupobjid, ss, dirbuf);
        if (err != 0) {
            LOGERR("CDBrowser::recursiveAdd: search failed, code " << err);
            return;
        }

        m_recwalkentries = dirbuf.m_items;

        // If we are inside the folders view tree, and the option is set, sort by url (minim sorts
        // by tracknum tag even inside folder view)
        if (CSettingsStorage::getInstance()->getSortKind() == CSettingsStorage::SK_MINIMFNORDER && 
            m_popupobjid.compare(0, minimFoldersViewPrefix.size(), minimFoldersViewPrefix) == 0) {
            std::vector<std::string> crits;
            crits.push_back("uri");
            DirObjCmp cmpo(crits);
            sort(m_recwalkentries.begin(), m_recwalkentries.end(), cmpo);
        }

        rreaperDone(0);

    } else {
        delete m_progressD;
        m_progressD = new QProgressDialog("Exploring Directory          ", tr("Cancel"), 0,0, this);
        // can't get setMinimumDuration to work
        m_progressD->setMinimumDuration(2000);
        time(&m_progstart);
        
        m_recwalkentries.clear();
        m_recwalkdedup.clear();
        m_reaper = new RecursiveReaper(m_cds->srv(), m_popupobjid, this);
        // Note: using Qt::BlockingQueued connection here is necessary to avoid recursive calls to
        // onSliceAvailable() when it calls appendHtml() and runJS() which give back control to the
        // event loop.
        //   onSliceAvailable()->runJS()->eventloop->usenextqueuedslice->onSliceAvailable()
        connect(m_reaper, SIGNAL(sliceAvailable(UPnPClient::UPnPDirContent *)),
                this, SLOT(onReaperSliceAvailable(UPnPClient::UPnPDirContent *)),
                Qt::BlockingQueuedConnection);
        connect(m_reaper, SIGNAL(done(int)), this, SLOT(rreaperDone(int)));
        m_reaper->start();
    }
}

void CDBrowser::onReaperSliceAvailable(UPnPClient::UPnPDirContent *dc)
{
    LOGDEB("CDBrowser::onReaperSliceAvailable: got " << dc->m_items.size() << " items\n");
    if (!m_reaper) {
        LOGINF("CDBrowser::onReaperSliceAvailable: cancelled\n");
        delete dc;
        return;
    }
    for (unsigned int i = 0; i < dc->m_items.size(); i++) {
        if (dc->m_items[i].m_resources.empty()) {
            LOGDEB("CDBrowser::onReaperSlice: entry has no resources??"<< "\n");
            continue;
        }
        LOGDEB1("CDBrowser::onReaperSlice: uri: " << dc->m_items[i].m_resources[0].m_uri << "\n");
        std::string md5;
        MD5String(dc->m_items[i].m_resources[0].m_uri, md5);
        auto res = m_recwalkdedup.insert(md5);
        if (res.second) {
            m_recwalkentries.push_back(dc->m_items[i]);
        } else {
            LOGDEB("CDBrowser::onReaperSlice: dup found" << "\n");
        }
    }
    delete dc;
    if (m_progressD) {
        if (m_progressD->wasCanceled()) {
            delete m_progressD;
            m_progressD = 0;
            m_reaper->setCancel();
        } else {
            if (time(0) - m_progstart >= 1) {
                m_progressD->show();
            }
            m_progressD->setLabelText(
                tr("Exploring Directory: %1 tracks").
                arg(m_recwalkentries.size()));
        }
    }
}

void CDBrowser::rreaperDone(int status)
{
    LOGDEB("CDBrowser::rreaperDone: status: " << status << ". Entries: " <<
           m_recwalkentries.size() << "\n");
    deleteReaders("rreaperDone");
    delete m_progressD;
    m_progressD = 0;
    if (m_popupmode == PUP_RAND_PLAY_TRACKS || m_popupmode == PUP_RAND_PLAY_GROUPS) {
        m_browsers->setInsertActive(false);
        // Sort list
        if (m_popupmode == PUP_RAND_PLAY_GROUPS) {
            std::vector<std::string> sortcrits;
            sortcrits.push_back("upplay:ctpath");
            sortcrits.push_back("upnp:album");
            sortcrits.push_back("upnp:originalTrackNumber");
            DirObjCmp cmpo(sortcrits);
            sort(m_recwalkentries.begin(), m_recwalkentries.end(), cmpo);
        }
        RandPlayer::PlayMode mode = m_popupmode == PUP_RAND_PLAY_TRACKS ?
            RandPlayer::PM_TRACKS : RandPlayer::PM_GROUPS;
        LOGDEB("CDBrowser::rreaperDone: emitting sig_tracks_to_randplay, mode " << mode << "\n");
        emit sig_tracks_to_randplay(mode, m_recwalkentries);
    } else {
        int sortkind = CSettingsStorage::getInstance()->getSortKind();
        if (sortkind == CSettingsStorage::SK_CUSTOM) {
            QStringList qcrits = CSettingsStorage::getInstance()->getSortCrits();
            std::vector<std::string> sortcrits;
            for (int i = 0; i < qcrits.size(); i++) {
                sortcrits.push_back(qs2utf8s(qcrits[i]));
            }
            DirObjCmp cmpo(sortcrits);
            sort(m_recwalkentries.begin(), m_recwalkentries.end(), cmpo);
        }
        MetaDataList mdl;
        mdl.resize(m_recwalkentries.size());
        bool allok{true};
        for (unsigned int i = 0; i <  m_recwalkentries.size(); i++) {
            if (!udirentToMetadata(&m_recwalkentries[i], &mdl[i], m_browsers->get_pl())) {
                allok = false;
            }
        }
        if (!allok) {
            QMessageBox::warning(0, "Upplay",
                                 "Some tracks could not be loaded: no compatible audio format");
        }
        emit sig_tracks_to_playlist(mdl);
    }
}
