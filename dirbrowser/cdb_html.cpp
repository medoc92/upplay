/* Copyright (C) 2005-2025 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "cdbrowser.h"

#include <string>
#include <sstream>

#include <QSettings>
#ifdef USING_WEBENGINE
#include <QLoggingCategory>
#endif
#include <libupnpp/upnpavutils.hxx>

#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Style.h"
#include "upadapt/upputils.h"
#include "utils/utf8iter.h"
#include "utils/smallut.h"

using namespace UPnPClient;
using namespace UPnPP;

// Notes for WebEngine
// - All links must begin with http:// for acceptNavigationRequest to be
//   called. 
// - The links passed to acceptNav.. have the host part 
//   lowercased -> we change S0 to http://h/S0, not http://S0

// The const part + the js we need for webengine. This is logically
// const after initialization by the first constructor.
static QString const_html_top;

// The const part + the js + changeable style, computed as needed.
static QString html_top;

// Initial post-header content of the servers page. The servers list
// will be inserted in the body using Javascript
static const QString empty_server_page_bot(
    "</head><body>"
    "<h2 id='cdstitle'>Content Directory Services</h2>"
    "</body></html>"
    );

// Content while we look for servers. 
static const QString init_server_page_bot(
    "</head><body>"
    "<h2 id='cdstitle'>Content Directory Services</h2>"
    "<p>&nbsp;</p><p>Looking for servers...</p>"
    "</body></html>"
    );


// Once initialization of the constant part at the top of pages: read
// the javascript files.
static void init_const_html_top()
{
    const_html_top = u8s2qs(
        "<html><head><meta http-equiv='content-type' content='text/html; charset=utf-8'>");
#ifdef USING_WEBENGINE
    QLoggingCategory("js").setEnabled(QtDebugMsg, true);
    QString jsfn = Helper::getCSSPath() + "containerscript.js";
    QString wcfn = Helper::getCSSPath() + "qwebchannel.js";
    QString js = "<script type=\"text/javascript\">\n";
    js += QString::fromUtf8(Helper::readFileToByteArray(jsfn));
    js += "\n";
    js += QString::fromUtf8(Helper::readFileToByteArray(wcfn));
    js += "</script>\n";
    const_html_top += js;
#endif
}

void CDBrowser::init_HTML()
{
    if (const_html_top.isEmpty()) {
        init_const_html_top();
    }
    setStyleSheet(Style::use_dark_colors(), false);
}

// Complete the top of pages by reading the CSS from our
// configuration, possibly fixing it for the current scale value, and
// appending it to const_html_top
void CDBrowser::setStyleSheet(bool dark, bool redisplay)
{
    QString cssfn = Helper::getCSSPath() + "cdbrowser.css";
    QString cssdata = QString::fromUtf8(Helper::readFileToByteArray(cssfn));

    updateFormatParams();
    float multiplier = m_formatparams.scalemultiplier;

    // On some platforms (apple) we need to adjust the html font point
    // size because 10pt in the HTML part is bigger than 10pt in the GUI??? Used to be 1.5
#ifdef __APPLE__
    float cssPointAdjust = 1.1;
#else
    float cssPointAdjust = 1.0;
#endif
    QSettings settings;
    if (settings.contains("htmlfontscaledownfactor")) {
        cssPointAdjust = settings.value("htmlfontscaledownfactor").toFloat();
    }
    multiplier /= cssPointAdjust;
    
    cssdata = u8s2qs(Style::scale_fonts(qs2utf8s(cssdata), multiplier));
    if (dark) {
        cssfn = Helper::getCSSPath() + "dark.css";
    } else {
        cssfn = Helper::getCSSPath() + "standard.css";
    }
    cssdata +=  Helper::readFileToByteArray(cssfn);

    html_top = const_html_top;
    html_top += "<style>\n";
    html_top += cssdata;
    html_top += "</style>\n";
    if (redisplay) {
        refresh();
    }
}

// With message "looking for servers"
QString CDBrowser::initialServersPage()
{
    return QString(html_top + init_server_page_bot);
}

// To be filled with the current servers list
QString CDBrowser::emptyServersPage()
{
    return QString(html_top + empty_server_page_bot);
}

QString CDBrowser::DSToHtml(int idx, const UPnPDeviceDesc& dev)
{
    QString out;
    out += QString("<p class='cdserver' cdsid='%1'>").arg(idx);
    out += QString("<a href='http://h/S%1'>").arg(idx);
    out += u8s2qs(dev.friendlyName);
    out += QString("</a></p>");
    return out;
}

std::string CDBrowser::imgtag(const std::string& imgurl, int height)
{
    std::string url = imgurl.empty() ? qs2utf8s(m_formatparams.genericAlbumImageURI) : imgurl;
    // Crop the image without changing the aspect ratio
    // https://cloudinary.com/guides/automatic-image-cropping/5-ways-to-crop-images-in-html-css
    // Original approach: resize, lose aspect ratio
    //  return QString("<img height='%1' width='%1' src='%2' />").arg(height).arg(url);
    // Automatic centering (default object-position: 50% 50%:
    return std::string("<img style='object-fit: cover; height: ") +
        lltodecstr(height) + "; width: " + lltodecstr(height) + "' src='" + url + "' />";
}

// Set attributes on the top element of a container display area (either header or list element)
static std::string mkctattrs(
    const std::string& title, const std::string& objid, int idx, bool isPlaylist)
{
    return std::string("class='container' objid='") + objid + "' objidx='" + lltodecstr(idx) +
        "' ispl='" + (isPlaylist ? "1" : "0") + "'"  + "title='" + title + "'";
}

QString CDBrowser::CTToHtml(int idx, const UPnPDirObject& e, const std::string& alphaid)
{
    bool isPlaylist = !e.getprop("upnp:class").compare(plContainerClass);
    auto title = Helper::escapeHtml(e.m_title);
    auto ctattrs = mkctattrs(title, e.m_id, idx, isPlaylist);
    // Special entry for full album at the top of contents list
    bool isAlbumHeader = idx == -1;
    
    std::string albumArtURI;
    if (m_formatparams.albumsAsCovers || m_formatparams.coverWithAlbum || isAlbumHeader) {
        e.getprop("upnp:albumArtURI", albumArtURI);
    }

    std::string artist;
    if (m_formatparams.artistWithAlbum || isAlbumHeader) {
        e.getprop("upnp:artist", artist);
        if (!artist.empty()) {
            utf8truncate(artist, m_formatparams.artistWithAlbumBytes,
                         UTF8T_ATWORD|UTF8T_ELLIPSIS, "\xE2\x80\xA6", " \t\n\t,;.");
            artist = Helper::escapeHtml(artist);
        }
    }
    
    std::string alphidattr;
    if (!alphaid.empty()) {
        // This is used with getElementById() javascript call
        alphidattr = std::string("id='") + alphaid + "'";
    }

    auto date = e.getprop("dc:date");
    if (endswith(date, "-01-01"))
        date = date.substr(0, date.size()-6);
    date = Helper::escapeHtml(date);
    
    int imgw = m_formatparams.albumsAsCoversWidth * m_formatparams.scalemultiplier;
    std::map<std::string, std::string> props {
        {"alphidattr", alphidattr},
        {"artist", artist},
        {"colspan", ""},
        {"ctattrs", ctattrs},
        {"date", date},
        {"idx", lltodecstr(idx)},
        {"imgtag",  imgtag(albumArtURI, imgw)},
        {"imgw", lltodecstr(imgw)},
        {"title", title},
    };

    std::string tmpl;
    if (isAlbumHeader) {
        // Album entry displayed before its content list.
        // The entrylist row has a single cell spanning the whole row, with an internal table
        tmpl =
            R"S(
<tr %(ctattrs)>
  <td colspan='5'>
    <table width='100%' class='albumheader'>
      <tr>
        <td class='albumheader aheadimg' width='%(imgw)' rowspan='3'>%(imgtag)</td>
      </tr>
      <tr>
        <td class='albumheader'><span class='albheadertitle'>%(title)</span></td>
      </tr>
        <td class='albumheader'>%(artist)<br>%(date)</td>
      </tr>
    </table>
  </td>
</tr>
)S";

    } else if (m_formatparams.albumsAsCovers) {

        // Whole block: table with 3 rows: image, title, artist
        tmpl = R"S(
<table class='ct_albcovertable'>
  <tr %(ctattrs)>
    <td><a href='http://h/C%(idx)'>%(imgtag)</a></td>
  </tr>
  <tr %(ctattrs)>
    <td><div class='ct_albcovertt' style='width:%(imgw)px;'>
        <a class='ct_title' %(alphidattr) href='http://h/C%(idx)'>%(title)</a></div>
    </td>
  </tr>
  <tr %(ctattrs)>
    <td><div class='ct_albcoverartist' style='width:%(imgw)px;'>%(artist)</div></td>
  </tr>
</table>
)S";

    } else {
        // Displaying albums as lists, like tracks
        if (!m_formatparams.artistWithAlbum || artist.empty())
            props["colspan"] = "colspan='3'";
        if (m_formatparams.coverWithAlbum) {
            imgw = m_formatparams.coverWithAlbumWidth * m_formatparams.scalemultiplier;
            props["imgw"] = lltodecstr(imgw);
            props["imgtag"] = imgtag(albumArtURI, imgw);
        }

        tmpl = R"S(
<tr %(ctattrs)>
    )S";

        if (m_formatparams.coverWithAlbum) {
            tmpl = tmpl + R"S(
  <td width='%(imgw)'><a class='ct_title' href='http://h/C%(idx)' tabindex='-1'>%(imgtag)</a></td>
)S";
        } else {
            tmpl = tmpl + R"S(
  <td></td>
)S";
        }

        tmpl = tmpl + R"S(
  <td class='ct_title' %(colspan) %(alphidattr)><a href='http://h/C%(idx)'>%(title)</a></td>
)S";

        if (m_formatparams.artistWithAlbum && !artist.empty()) {
            tmpl = tmpl + R"S(
  <td class='ct_artist' colspan='2'>%(artist)</td>
)S";
        }
        tmpl = tmpl + R"S(
</tr>
)S";

    }
    
    std::string out;
    pcSubst(tmpl, out, props);
    LOGDEB1("CT [" << out << "]\n");
    return u8s2qs(out);
}

/** 
 * Show item as table row with 5 cells: tno, artist, title, album, duration
 * @arg idx index in entries array
 * @arg e array entry
*/
QString CDBrowser::ItemToHtml(int idx, const UPnPDirObject& e, bool noalb)
{
    std::string tracknum = Helper::escapeHtml(e.getprop("upnp:originalTrackNumber"));
    if (tracknum.empty())
        tracknum = "&nbsp;";

    std::string title = e.m_title.empty() ? "&nbsp;" : Helper::escapeHtml(e.m_title);

    std::string artist = e.getprop("upnp:artist");
    if (artist.empty()) {
        artist = "&nbsp;";
    } else {
        if (m_maxartlen > 0 && int(artist.size()) > m_maxartlen) {
            int len = m_maxartlen-3 >= 0 ? m_maxartlen-3 : 0;
            artist = artist.substr(0, len) + "...";
        }
        artist = escapeHtml(artist);
    }

    std::string duration;
    e.getrprop(0, "duration", duration);
    int seconds = upnpdurationtos(duration);
    // Format as mm:ss
    int mins = seconds / 60;
    int secs = seconds % 60;
    char sdur[100];
    snprintf(sdur, 100, "%02d:%02d", mins, secs);

    std::map<std::string, std::string> props {
        {"objid", e.m_id},
        {"idx", lltodecstr(idx)},
        {"tracknum", tracknum},
        {"title", title},
        {"artist", artist},
        {"album", Helper::escapeHtml(e.getprop("upnp:album"))},
        {"duration", sdur},
    };

    std::string tmpl = R"S(
<tr class='item' objid='%(objid)' objidx='%(idx)'>
  <td class='tk_tracknum'>%(tracknum)</td>
  <td class='tk_title'><a href='http://h/I%(idx)'>%(title)</a></td>
  <td class='tk_artist'>%(artist)</td>
)S";
    if (!noalb) {
        tmpl = tmpl + R"S(
  <td class='tk_album'>%(album)</td>)S";
    }    

    tmpl = tmpl + R"S(
  <td class='tk_duration'>%(duration)</td>
</tr>
)S";

    std::string out;
    pcSubst(tmpl, out, props);
    LOGDEB1("IT [" << out << "]\n");
    return u8s2qs(out);
}


// Container display alphabetic jump links
QString CDBrowser::alphalinks(const std::string& _initials, bool nolinks)
{
    std::string initials  = _initials;
    if (initials.empty()) {
        // Avoid the page jumping up later when we redisplay, e.g. after a popup. This is probably a
        // browser bug workaround.
        nolinks = true;
        initials = "&nbsp;";
    }
    QString html = QString("<div class='alphalist'>");
    html += "<br clear='all'/>";
    if (nolinks) {
        html += QString("<b>") + u8s2qs(initials) + "</b>";
    } else {
        for (unsigned int i = 0; i < initials.size(); i++) {
            QString letter(1, initials[i]);
            html += QString("<a href='http://h/a") + letter +
                QString("' tabindex='-1'>") + letter + QString("</a>");
        }
    }
    html += QString("</div>");
    return html;
}

// Container display page: header part before the thumbnails or rows: current
// browse path and default alphalinks
QString CDBrowser::bodyTopHtml()
{
    QString headerdata;
    QFontMetrics metrics(font());
    headerdata += "<div id='browsepath'>\n";
    headerdata += "  <ul>\n";
    bool current_is_search = false;

    bool elide{false};
    if (m_elidePath) {
        QString path;
        for (unsigned i = 0; i < m_curpath.size(); i++) {
            QString sep("&gt;");
            if (i == 0)
                sep = "";
            path += u8s2qs(m_curpath[i].title) + sep;
        }
        // This does not work well the first time we are called (when restoring from state), because
        // the width is wrong.
        if (metrics.size(0, path).width() > width()) {
            elide = true;
        }
        LOGDEB1("CDBrowser::bodyTopHtml: width " << width() << " header w " <<
                metrics.size(0, path).width()  << "\n");
    }

    for (unsigned i = 0; i < m_curpath.size(); i++) {
        CtPathElt& e{m_curpath[i]};
        QString sep("&gt;");
        if (!e.searchStr.empty()) {
            if (current_is_search) {
                // Indicate that searches are not nested by changing
                // the separator
                sep = "&lt;&gt;";
            }
            current_is_search = true;
        }
        if (i == 0)
            sep = "";
        std::string title;
        if (elide && e.title.length() > 5 && i != m_curpath.size()-1) {
            title = e.title.substr(0,1) + "..." + e.title.substr(e.title.length()-1, 1);
        } else {
            title = e.title;
        }
        headerdata += QString(
            "   <li class='container' objid='%3' ispl='%5'>"
            " %4 <a href='http://h/L%1'>%2</a></li>\n").
            arg(i).arg(u8s2qs(title)).arg(u8s2qs(e.objid)).arg(sep)
            .arg(e.isPlaylist);
    }

    headerdata += "  </ul>\n";
    headerdata += "</div>\n";

    headerdata += alphalinks("Reading...", true);
    
    return headerdata;
}

// Container display page, next after top: close head and add body tag, with some Javascript for
// webengine.
//
// We would like to use F10 for the context menu activation, because this appears to be the
// standard on MS Windows, but could not make it work, probably the desktop eats it.
static const QString init_container_pagemid = QString::fromUtf8(
    "</head><body>"
#ifdef USING_WEBENGINE
   R"(<script>
    document.addEventListener('contextmenu', saveLoc, true);
    document.addEventListener('focus', saveLoc, true);
    document.addEventListener('mousedown', saveLoc, true);
    new QWebChannel(qt.webChannelTransport, function (channel) {
                            document.addEventListener("keydown", function(e) {
                                //console.log("EventListener:keydown: " + e.key + " " + e.shiftKey);
                                if (e.key === "ContextMenu" || (e.key === "F9" && e.shiftKey)) {
                                    channel.objects.popup_helper.onTriggered();
                                }
                            });
                        });
    </script>)"
#endif
    );

// Container display page: thumbnails display area. Stays empty if we are in list mode and do not
// display album thumbnails.
static const QString init_container_thumbnails_div = QString::fromUtf8(
    "<div id='thumbnailsdiv'>"
    "</div>"
    );

// Container display page, bottom table for the tracks or container lines. The actual data is
// appended to the element with id 'entrylist' innerHTML (goes between </colgroup> and </table>)
static const QString init_container_empty_table = QString::fromUtf8(
    "<table id='entrylist'>"
    "<colgroup>"
    "<col class='coltracknumber'>"
    "<col class='coltitle'>"
    "<col class='colartist'>"
    "<col class='colalbum'>"
    "<col class='colduration'>"
    "</colgroup>"
    "</table></body></html>"
    );

// Container display page: assemble the whole thing.
void CDBrowser::initContainerHtml(const std::string& ss)
{
    LOGDEB1("CDBrowser::initContainerHtml\n");
    QString bodytop;
    bodytop += "<div id='fixedheader'>\n";
    bodytop += bodyTopHtml();
    bodytop += "</div>\n";
    // See comments in the CSS file
    bodytop += "<div id='hiddenheader'>\n";
    bodytop += bodyTopHtml();
    bodytop += "</div>\n";
    bodytop += "<br clear='all'/>";

    if (!ss.empty()) {
        bodytop += QString("<p style='padding-bottom:1em'>Search results for: ") + u8s2qs(ss);
    }

    QString html = html_top +            // <head>
        init_container_pagemid +         // </head><body>
        bodytop +                        // breadcrumbs, alphalinks
        // Album header goes into either #thumbnailsdiv or #entrylist dep. on mode
        init_container_thumbnails_div +  // Cover thumbnails div
        init_container_empty_table;      // Content entries list table

    LOGDEB1("initContainerHtml: initial content: " << qs2utf8s(html) << "\n");

    mySetHtml(html);
    LOGDEB1("CDBrowser::initContainerHtml done\n");
}
