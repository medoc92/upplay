/* Copyright (C) 2014-2021 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>
#include <vector>
#include <set>

#include <QToolButton>
#include <QTabBar>
#include <QIcon>
#include <QTimer>
#include <QShortcut>
#include <QLineEdit>
#include <QCompleter>

#include "ui_dirbrowser.h"

#include "libupnpp/log.h"
#include "dirbrowser.h"
#include "application.h"
#include "utils/smallut.h"
#include "HelperStructs/Helper.h"
#include "HelperStructs/Style.h"
#include "upadapt/upputils.h"
#include "GUI/prefs/sortprefs.h"
#include "HelperStructs/CSettingsStorage.h"

static const QString cstr_searchhistorykey("/Upplay/dirbrowser/searchhistory");
static const QString cstr_dirbrowserstate{"/Upplay/dirbrowser/tabstate"};

static const QString UPnPSearchCheat(
    "Album/Artist etc.: just enter a value.<br>"
    "UPnP search string example:<br>"
    "upnp:artist contains \"bach\" and "
    "dc:title contains \"allegro\""
    );
static std::vector<std::pair<std::string, std::string> > allSearchProps{
    {std::pair<std::string,std::string>{"upnp:artist", "Artist"}},
    {std::pair<std::string,std::string>{"upnp:album", "Album"}},
    {std::pair<std::string,std::string>{"dc:title", "Title"}},
    {std::pair<std::string,std::string>{"upnp:genre", "Genre"}}
};

DirBrowser::DirBrowser(QWidget *parent)
    : QWidget(parent)
{
    QSettings settings;
    m_pl = std::shared_ptr<Playlist>();

    ui = new Ui::DirBrowser();
    ui->setupUi(this);
    ui->searchFrame->hide();
    ui->cdBrowser->setDirBrowser(this);
    
    QShortcut *sc;
    sc = new QShortcut(QKeySequence("Ctrl+f"), this);
    connect(sc, SIGNAL (activated()), this, SIGNAL(sig_show_search()));
    sc = new QShortcut(QKeySequence("/"), this);
    connect(sc, SIGNAL (activated()), this, SIGNAL(sig_show_search()));
    // Esc does not work any more (with recent qt?). Preempted by something?
    sc = new QShortcut(QKeySequence("Esc"), this);
    connect(sc, SIGNAL (activated()), this, SIGNAL(sig_hide_search()));
    sc = new QShortcut(QKeySequence(Qt::Key_F3), this);
    connect(sc, SIGNAL(activated()), this, SLOT(searchNext()));
    QKeySequence seq;
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    seq = QKeySequence(Qt::SHIFT | Qt::Key_F3);
#else
    seq = QKeySequence(Qt::SHIFT + Qt::Key_F3);
#endif
    sc = new QShortcut(seq, this);
    connect(sc, SIGNAL(activated()), this, SLOT(searchPrev()));
    connect(ui->nextTB, SIGNAL(clicked()), this, SLOT(searchNext()));
    connect(ui->prevTB, SIGNAL(clicked()), this, SLOT(searchPrev()));
    seq = QKeySequence("Ctrl+s");
    sc = new QShortcut(seq, this);
    connect(sc, SIGNAL(activated()), this, SLOT(toggleSearchKind()));
    connect(ui->serverSearchCB, SIGNAL(stateChanged(int)), 
            this, SLOT(onSearchKindChanged(int)));
    connect(ui->execSearchPB, SIGNAL(clicked()), this, SLOT(serverSearch()));
    onSearchKindChanged(int(ui->serverSearchCB->checkState()));

    ui->searchCMB->setToolTip(UPnPSearchCheat);
    ui->searchCMB->setInsertPolicy(QComboBox::NoInsert);
    m_searchHistory = settings.value(cstr_searchhistorykey).toStringList();
    ui->searchCMB->addItems(m_searchHistory);
    ui->searchCMB->setEditText("");
    ui->searchCMB->completer()->setModel(0);
    connect(ui->searchCMB->lineEdit(), SIGNAL(textChanged(const QString&)), 
            this, SLOT(onSearchTextChanged(const QString&)));
    connect(ui->searchCMB->lineEdit(), SIGNAL(returnPressed()),
            this, SLOT(returnPressedInSearch()));

    QIcon icon(Helper::getIconPath("cross.png"));
    ui->closeSearchTB->setIcon(icon);
    connect(ui->closeSearchTB, SIGNAL(clicked()), this, SIGNAL(sig_hide_search()));

    ui->execSearchPB->hide();
}

// Called from the main ui setup to decide if we live in a tab from the main window (compact
// display), or have our own tab widget possibly with multiple instances of the browser. We also
// make other compactness adjustments while we are at it.
void DirBrowser::setTabsWidget(QTabWidget *tabs, int tabs_base_index)
{
    if (tabs) {
        m_tabs_base_index = tabs_base_index;
        m_tabs = tabs;
        m_cdb = cdbFromTabIndex(tabs_base_index);
        if (m_cdb) {
            m_cdb->setParent(this);
            m_cdb->setElidePath(true);
        }
        ui->tabs->close();
        ui->verticalLayout->insertWidget(0, m_cdb);
    } else {
        QKeySequence seq;
        QShortcut *sc;
        m_tabs = ui->tabs;
        m_tabs->setTabsClosable(true);
#ifdef Q_OS_MACOS
        m_tabs->setDocumentMode(true);
#else
        m_tabs->setStyleSheet("QTabBar::close-button {image: url(:/icons/cross.png)}");
#endif

        QToolButton *plusbut = new QToolButton(this);
        m_tabs->setCornerWidget(plusbut, Qt::TopRightCorner);
        plusbut->setCursor(Qt::ArrowCursor);
        plusbut->setAutoRaise(true);
        plusbut->setToolTip(tr("Add tab"));
        connect(plusbut, SIGNAL(clicked()), this, SLOT(addTab()));
        seq = QKeySequence("Ctrl+t");
        sc = new QShortcut(seq, this);
        connect(sc, SIGNAL (activated()), this, SLOT(addTab()));
        seq = QKeySequence("Ctrl+w");
        sc = new QShortcut(seq, this);
        connect(sc, SIGNAL (activated()), this, SLOT(closeCurrentTab()));

        connect(m_tabs, SIGNAL(currentChanged(int)), this, SLOT(onCurrentTabChanged(int)));
        connect(m_tabs, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));
    }

    std::string tabjs;
    QSettings settings;
    if (!settings.value("norestoretabs").toBool()) {
        tabjs = qs2utf8s(settings.value(cstr_dirbrowserstate).toString());
    }
    // Erase tab state. It will be saved again by a normal exit. If the program crashes because
    // of the tab state instead, the next startup should succeed because of the erased state.
    settings.remove(cstr_dirbrowserstate);
    if (!tabjs.empty()) {
        restoreTabs(tabjs);
    }
}

void DirBrowser::restoreTabs(const std::string& tabjs)
{
    auto state = stateFromJson(tabjs);

    if (m_tabs_base_index && !state.empty()) {
        // Part of main tabs. We only use one tab for now, so just activate the browser
        m_cdb->browseInNewTab(u8s2qs(state[0].first), state[0].second);
        return;
    }

    for (unsigned int tabidx = 0; tabidx < state.size(); tabidx++) {
        if (tabidx > 0) {
            // the first tab is created with the dirbrowser
            addTab();
        }
        CDBrowser *cdb = cdbFromTabIndex(m_tabs->count() -1);
        if (nullptr == cdb) {
            return;
        }
        cdb->browseInNewTab(u8s2qs(state[tabidx].first), state[tabidx].second);
    }
    
    m_tabs->setCurrentIndex(m_tabs_base_index);
}

void DirBrowser::onAboutToExit()
{
    std::string json = stateToJson();
    QSettings settings;
    settings.setValue(cstr_dirbrowserstate, json.c_str());
}

void DirBrowser::setPlaylist(std::shared_ptr<Playlist> pl)
{
    m_pl = pl;

    for (int i = m_tabs_base_index; i < m_tabs->count(); i++) {
        setupTabConnections(i);
    }
    if (m_pl)
        connect(m_pl.get(), SIGNAL(sig_insert_done()), this, SLOT(onInsertDone()));
}

void DirBrowser::refresh()
{
    float multiplier = Style::get_ui_scale();
    if (multiplier != 1.0) {
        int iheight = 10 * multiplier;
        ui->searchCMB->setIconSize(QSize(iheight, iheight));
        ui->execSearchPB->setIconSize(QSize(iheight, iheight));
        ui->prevTB->setIconSize(QSize(2*iheight, iheight));
        ui->nextTB->setIconSize(QSize(2*iheight, iheight));
        ui->serverSearchCB->setIconSize(QSize(iheight, iheight));
        ui->propsCMB->setIconSize(QSize(iheight, iheight));
        ui->closeSearchTB->setIconSize(QSize(1.5 * iheight, 1.5 * iheight));
    }    
    if (m_tabs == ui->tabs) {
        // Normal mode with separate tabs widget. Set up the "add tab" button
        auto plusbut = dynamic_cast<QToolButton*>(m_tabs->cornerWidget(Qt::TopRightCorner));
        if (plusbut) {
#if (defined(Q_OS_MACOS) && (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)))
            plusbut->setIcon(QIcon::fromTheme("addtab"));
#else
            QIcon icon(Helper::getIconPath("addtab.png"));
            plusbut->setIcon(icon);
#endif
        }
    }        

#if (defined(Q_OS_MACOS) && (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)))
    ui->closeSearchTB->setIcon(QIcon::fromTheme("cross"));
    ui->closeSearchTB->setStyleSheet("QToolButton {border : none;}");
#else
    QIcon icon(Helper::getIconPath("cross.png"));
    ui->closeSearchTB->setIcon(icon);
#endif

    for (int i = m_tabs_base_index; i < m_tabs->count(); i++) {
        CDBrowser *cdb = cdbFromTabIndex(i);
        if (cdb) {
            cdb->refresh();
        }
    }
}
    
void DirBrowser::addTab()
{
    QWidget *tab = new QWidget(this);
    tab->setObjectName(QString::fromUtf8("tab"));
    QVBoxLayout* vl = new QVBoxLayout(tab);

    CDBrowser *cdb = new CDBrowser(tab, this);
    cdb->setObjectName(QString::fromUtf8("cdBrowser"));
    setupTabConnections(cdb);

    vl->addWidget(cdb);
    m_tabs->addTab(tab, QString());
    m_tabs->setCurrentIndex(m_tabs->count() -1);
}

void DirBrowser::closeCurrentTab()
{
    closeTab(m_tabs->currentIndex());
}

int DirBrowser::tabsCount()
{
    return m_tabs->count() - m_tabs_base_index;
}

void DirBrowser::closeTab(int i)
{
    if (m_insertactive || i < m_tabs_base_index || tabsCount() <= 1 )
        return;
    QWidget *w = m_tabs->widget(i);
    m_tabs->removeTab(i);
    delete w;
}


// Adjust the search panel according the the server caps for the active tab
void DirBrowser::onCurrentTabChanged(int)
{
    CDBrowser *cdb = currentBrowser();
    if (nullptr == cdb)
        return;
    std::set<std::string> caps;
    if (cdb)
        cdb->getSearchCaps(caps);
    QString strcaps = u8s2qs("<i></i>Server searchable fields: ") +
        u8s2qs(stringsToString(caps));

    if (caps.empty()) {
        ui->serverSearchCB->setCheckState(Qt::Unchecked);
        ui->serverSearchCB->setEnabled(false);
    } else {
        QString prevChoice = ui->propsCMB->currentText();
        while(ui->propsCMB->count())
            ui->propsCMB->removeItem(0);
        ui->serverSearchCB->setEnabled(true);
        QSettings settings;
        if (settings.value("dfltserversearch").toBool()) {
            ui->serverSearchCB->setCheckState(Qt::Checked);
        } else {
            ui->serverSearchCB->setCheckState(Qt::Unchecked);
        }
        QString scs;
        for (const auto& prop : allSearchProps) {
            if (caps.find("*") != caps.end() ||
                caps.find(prop.first) != caps.end()) {
                ui->propsCMB->addItem(u8s2qs(prop.second),
                                      QVariant(u8s2qs(prop.first)));
            }
            scs += u8s2qs(prop.first.c_str()) + " ";
        }
        ui->propsCMB->addItem(tr("Raw UPnP"), QVariant(u8s2qs("*")));
        ui->propsCMB->setToolTip(strcaps);
        if (!prevChoice.isEmpty()) {
            int i = ui->propsCMB->findText(prevChoice);
            if (i >= 0) {
                ui->propsCMB->setCurrentIndex(i);
            }
        }
        onSearchKindChanged(ui->serverSearchCB->checkState());
    }
}

static QString escapeAmpersand(const QString& in)
{
    QString out(in);
    return out.replace(QChar('&'), QString::fromUtf8("&&"));
}

void DirBrowser::changeTabTitle(QWidget *w, const QString& tt)
{
    int i = m_tabs->indexOf((QWidget*)w->parent());
    if (i >= 0) {
        m_tabs->setTabText(i, escapeAmpersand(tt));
    } else {
        LOGDEB("changeTabTitle: Widget not found in tabs: " << w <<"\n");
    }
}

void DirBrowser::saveSearchHistory()
{
    // Search terms history:
    // We want to have the new text at the top and any older identical
    // entry to be erased. There is no standard qt policy to do this ? 
    // So do it by hand.
    QString txt = ui->searchCMB->currentText();
    QString txtt = txt.trimmed();
    int index = ui->searchCMB->findText(txtt);
    if (index > 0) {
        ui->searchCMB->removeItem(index);
    }
    if (index != 0) {
        ui->searchCMB->insertItem(0, txtt);
        ui->searchCMB->setEditText(txt);
    }

    // Limit saved size to reasonable value
    const int maxcount = 200;
    int count = ui->searchCMB->count();
    if (count > maxcount) {
        count = maxcount;
    }
    m_searchHistory.clear();
    for (int index = 0; index < count; index++) {
        m_searchHistory.push_back(ui->searchCMB->itemText(index));
    }
    QSettings settings;
    settings.setValue(cstr_searchhistorykey, m_searchHistory);
}

// Any of the tabs searchcaps changed. Update the current one just in case.
void DirBrowser::onSearchcapsChanged()
{
    onCurrentTabChanged(-1);
}

void DirBrowser::takeFocus()
{
    auto b = currentBrowser();
    if (b) {
        b->takeFocus();
    }
}

void DirBrowser::openSearchPanel()
{
    onCurrentTabChanged(-1);
    ui->searchFrame->show();
    ui->searchCMB->lineEdit()->setFocus();
    ui->searchCMB->lineEdit()->selectAll();
}

void DirBrowser::closeSearchPanel()
{
    QString text = ui->searchCMB->lineEdit()->text();
    if (ui->searchCMB->findText(text))
        ui->searchCMB->insertItem(0, text);
    ui->searchFrame->hide();
}

void DirBrowser::showSearchPanel(bool yes)
{
    if (yes)
        openSearchPanel();
    else
        closeSearchPanel();
}

void DirBrowser::toggleSearchKind()
{
    if (!ui->serverSearchCB->isEnabled())
        return;
    if (ui->serverSearchCB->checkState()) {
        ui->serverSearchCB->setCheckState(Qt::Unchecked);
    } else {
        ui->serverSearchCB->setCheckState(Qt::Checked);
    }
    onSearchKindChanged(ui->serverSearchCB->checkState());
}

void DirBrowser::onSearchKindChanged(int state)
{
    if (state == Qt::Unchecked) {
        ui->propsCMB->setEnabled(false);
        ui->execSearchPB->hide();
        ui->execSearchPB->setEnabled(false);
        ui->nextTB->show();
        ui->nextTB->setEnabled(true);
        ui->prevTB->show();
        ui->prevTB->setEnabled(true);
    } else {
        ui->propsCMB->setEnabled(true);
        ui->execSearchPB->show();
        ui->execSearchPB->setEnabled(true);
        ui->nextTB->hide();
        ui->nextTB->setEnabled(false);
        ui->prevTB->hide();
        ui->prevTB->setEnabled(false);
    }
}

void DirBrowser::onSearchTextChanged(const QString& text)
{
    // Auto-search on typing only for local searches.
    if (!ui->propsCMB->isEnabled()) {
        if (!text.isEmpty()) {
            doSearch(text, false);
        }
    }
}

void DirBrowser::searchNext()
{
    doSearch(ui->searchCMB->currentText().trimmed(), false);
}

void DirBrowser::searchPrev()
{
    doSearch(ui->searchCMB->currentText().trimmed(), true);
}

void DirBrowser::returnPressedInSearch()
{
    if (ui->propsCMB->isEnabled()) {
        serverSearch();
    } else {
        QString text = ui->searchCMB->lineEdit()->text();
        if (ui->searchCMB->findText(text))
            ui->searchCMB->insertItem(0, text);
        searchNext();
    }
    saveSearchHistory();
}

void DirBrowser::serverSearch()
{
    QString text = ui->searchCMB->lineEdit()->text().trimmed();
    int i = ui->propsCMB->currentIndex();
    std::string prop = qs2utf8s(ui->propsCMB->itemData(i).toString());
    if (text != "") {
        std::string iss = qs2utf8s(text);
        // prop can be either a field name (e.g. dc:date,
        // upnp:artist), or "*" in which case the user is responsible
        // for entering an appropriate search"
        std::string ss;
        if (prop == "*") {
            ss = iss;
        } else {
            ss.assign(prop);
            ss += " contains \"";
            for (unsigned i = 0; i < iss.size(); i++) {
                if (iss[i] == '"' || iss[i] == '\\')
                    ss += std::string("\\");
                ss += iss[i];
            }
            ss += '"';
        } 
        CDBrowser *cdb = currentBrowser();
        if (cdb)
            cdb->search(ss);
    }
}

void DirBrowser::onSortprefs()
{
    LOGDEB0("DirBrowser::onSortprefs()\n");
    refresh();
}

// Perform text search in current tab. 
void DirBrowser::doSearch(const QString& text, bool reverse)
{
    LOGDEB("DirBrowser::doSearch: text " << qs2utf8s(text) << " reverse " << reverse << "\n");
    CDBrowser *cdb = currentBrowser();
    if (cdb)
        cdb->findText(text, reverse);
}

void DirBrowser::setInsertActive(bool onoff)
{
    m_insertactive = onoff;
    if (!m_pl)
        m_insertactive = false;
}

bool DirBrowser::insertActive()
{
    return m_insertactive;
}

CDBrowser *DirBrowser::currentBrowser()
{
    LOGDEB1("CDBrowser *DirBrowser::currentBrowser(): currentindex " << m_tabs->currentIndex() <<
            " base_index " << m_tabs_base_index << "\n");
    if (m_tabs->currentIndex() < m_tabs_base_index) {
        return nullptr;
    }
    QWidget *tw = m_tabs->currentWidget();
    if (nullptr == tw)
        return nullptr;
    return tw->findChild<CDBrowser*>();
}

void DirBrowser::setupTabConnections(int i)
{
    CDBrowser *cdb = cdbFromTabIndex(i);
    if (cdb) {
        setupTabConnections(cdb);
    }
}

void DirBrowser::setupTabConnections(CDBrowser *cdb)
{
    disconnect(cdb, SIGNAL(sig_now_in(QWidget *, const QString&)), 0, 0);
    connect(cdb, SIGNAL(sig_now_in(QWidget *, const QString&)), 
            this, SLOT(changeTabTitle(QWidget *, const QString&)));

    disconnect(cdb, SIGNAL(sig_tracks_to_playlist(const MetaDataList&)), 0, 0);
    if (m_pl)
        connect(cdb, SIGNAL(sig_tracks_to_playlist(const MetaDataList&)),
                m_pl.get(), SLOT(psl_add_tracks(const MetaDataList&)));

    disconnect(cdb, SIGNAL(sig_searchcaps_changed()), 0, 0);
    connect(cdb, SIGNAL(sig_searchcaps_changed()), this, SLOT(onSearchcapsChanged()));

    disconnect(cdb, SIGNAL(sig_browse_in_new_tab(QString, std::vector<CDBrowser::CtPathElt>)), 0, 0);
    connect(cdb, SIGNAL(sig_browse_in_new_tab(QString, std::vector<CDBrowser::CtPathElt>)),
            this, SLOT(onBrowseInNewTab(QString, std::vector<CDBrowser::CtPathElt>)));

    disconnect(cdb, SIGNAL(sig_tracks_to_randplay(RandPlayer::PlayMode,
                                                  const std::vector<UPnPClient::UPnPDirObject>&)),
               0, 0);
    connect(cdb, SIGNAL(sig_tracks_to_randplay(RandPlayer::PlayMode,
                                               const std::vector<UPnPClient::UPnPDirObject>&)),
            this, SLOT(onEnterRandPlay(RandPlayer::PlayMode,
                                       const std::vector<UPnPClient::UPnPDirObject>&)));

    disconnect(cdb, SIGNAL(sig_rand_stop()), 0, 0);
    connect(cdb, SIGNAL(sig_rand_stop()), this, SLOT(onRandStop()));
    disconnect(cdb, SIGNAL(sig_sort_order()), 0, 0);
    connect(cdb, SIGNAL(sig_sort_order()), this, SIGNAL(sig_sort_order()));
}

void DirBrowser::onEnterRandPlay(RandPlayer::PlayMode mode, const
                                 std::vector<UPnPClient::UPnPDirObject>& ents)
{
    if (!m_pl)
        return;
    LOGDEB("DirBrowser::onEnterRandPlay: " << ents.size() << " tracks\n");
    if (m_randplayer) {
        delete m_randplayer;
        m_randplayer = 0;
    }
    if ((m_randplayer = new RandPlayer(mode, ents, this)) == 0) {
        LOGERR("Out of memory\n");
        return;
    }
    connect(m_randplayer, SIGNAL(sig_tracks_to_playlist(const MetaDataList&)),
            this, SLOT(onRandTracksToPlaylist(const MetaDataList&)));
    connect(m_pl.get(), SIGNAL(sig_playlist_done()), m_randplayer, SLOT(playNextSlice()));
    connect(m_randplayer, SIGNAL(sig_randplay_done()), this, SLOT(onRandDone()));
    connect(m_randplayer, SIGNAL(sig_next_group_html(QString)),
            this, SIGNAL(sig_next_group_html(QString)));
    m_randplayer->playNextSlice();
}

void DirBrowser::onRandTracksToPlaylist(const MetaDataList& mdl)
{
    if (!m_pl)
        return;

    // We'll get a playlist_done when the old list is replaced, don't
    // want this to trigger a new slice... We should distinguish a
    // playlist clear from the ui (which we want to trigger a new
    // slice) from this one, but temporarily disconnecting is just
    // simpler.
    disconnect(m_pl.get(), SIGNAL(sig_playlist_done()), m_randplayer, SLOT(playNextSlice()));
    
    int mode = m_pl->mode();
    int replace_saved = mode & PLM_replace;
    mode |= PLM_replace;
    m_pl->psl_change_mode(mode);

    m_pl->psl_add_tracks(mdl);

    copyplmbit(mode, replace_saved, PLM_replace);
    m_pl->psl_change_mode(mode);
    // not necessary for upmpdcli but helps mediaplayer
    m_pl->psl_change_track(0);
    m_pl->psl_play();

    connect(m_pl.get(), SIGNAL(sig_playlist_done()), m_randplayer, SLOT(playNextSlice()));
}

void DirBrowser::onRandDone()
{
    // Is it a good idea to delete randplay from here as we're
    // connected to its signal ? Could use a QTimer
    onRandStop();
}

void DirBrowser::onRandStop()
{
    delete m_randplayer;
    m_randplayer = 0;
}

void DirBrowser::onBrowseInNewTab(QString UDN, std::vector<CDBrowser::CtPathElt> path)
{
    LOGDEB1("onBrowseInNewTab(): " << UDN << "\n");
    addTab();
    CDBrowser *cdb = currentBrowser();
    if (cdb) 
        cdb->browseInNewTab(UDN, path);
}

CDBrowser *DirBrowser::cdbFromTabIndex(int i)
{
    CDBrowser *cdb{nullptr};
    QWidget *tw = m_tabs->widget(i);
    if (tw) {
        cdb = tw->findChild<CDBrowser*>();
    }
    if (nullptr == cdb) {
        LOGDEB("Tab " << i << " not found or has no browser child\n");
    }
    return cdb;
}
