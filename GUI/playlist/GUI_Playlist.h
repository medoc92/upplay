/* Copyright (C) 2011  Lucio Carreras
 * Copyright (C) 2014-2022 J.F. Dockes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GUI_PLAYLIST_H_
#define GUI_PLAYLIST_H_

#include <string>

#include <QWidget>
#include <QList>
#include <QFocusEvent>

#include "GUI/playlist/delegate/PlaylistItemDelegate.h"
#include "playlist/playlist.h"
#include "HelperStructs/MetaData.h"

namespace Ui {
class Playlist_Window;
}

class QEvent;
class QGestureEvent;
class QResizeEvent;
class QAction;
class QToolBar;

class GUI_Playlist : public QWidget {
    Q_OBJECT

public:
    GUI_Playlist(QWidget *parent = nullptr);
    // If sz is specified, we resize the buttons, else we keep their .ui-defined size
    void setupButtons(int sz = 0);
    void setupButtonIcons(bool dark);

    void setActions(QAction *repeat_all, QAction *shuffle, QAction *append, QAction *replace,
                    QAction *autoplay, QAction *show_numbers);
protected:
    bool event(QEvent *);
    
signals:
    // Emitted on dbl-click
    void row_activated(int);

    // Current selection list.
    void selected_list(const QList<int>&);

    void clear_playlist();
    void playlist_mode_changed(int);

    void dropped_tracks(const MetaDataList&, int);
    void sig_rows_removed(const QList<int>&, bool);

    void sig_sort_tno();
    // Radio,PL button clicked. Switch to radio if true (else playlist)
    void sig_radio(bool);
                     
public slots:
    void fillPlaylist(MetaDataList&, int, int);
    void track_changed(int);
    void setMode(int mode);
    void setPlayerMode(int mode);
    void psl_show_small_playlist_items(bool small_items);
    void psl_next_group_html(QString html);
    void psl_openhome_renderer(bool);
    void psl_openhome_source_radio(bool);
    void takeFocus();
                        
private slots:
    void double_clicked(int);
    void playlist_mode_changed_slot();
    void btn_numbers_changed(bool);
    void metadata_dropped(const MetaDataList&, int);
    void rows_removed(const QList<int>&, bool select_next_row);

private:
    Ui::Playlist_Window *ui;
    int _playlist_mode;
    qint64 _total_msecs;
    QToolBar *m_tb_order{nullptr};
    QToolBar *m_tb_mode{nullptr};
    QToolBar *m_tb_numbers{nullptr};
    QAction *m_repeat_all{nullptr};
    QAction *m_shuffle{nullptr};
    QAction *m_append{nullptr};
    QAction *m_replace{nullptr};
    QAction *m_autoplay{nullptr};
    QAction *m_show_numbers{nullptr};

    void set_total_time_label();
    void check_dynamic_play_button();
    void dragEnterEvent(QDragEnterEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    bool gestureEvent(QGestureEvent *event);
    void resizeEvent(QResizeEvent *ev);
    
protected:
    void focusInEvent(QFocusEvent *e);
};

#endif /* GUI_PLAYLIST_H_ */
