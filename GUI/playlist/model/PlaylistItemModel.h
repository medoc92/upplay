/* Copyright (C) 2011  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLAYLISTITEMMODEL_H_
#define PLAYLISTITEMMODEL_H_

#include "HelperStructs/MetaData.h"

#include <QObject>
#include <QList>
#include <QAbstractListModel>
#include <QModelIndex>

class PlaylistItemModel : public QAbstractListModel {
    Q_OBJECT
public:
    PlaylistItemModel(QObject* parent = 0)
        : QAbstractListModel(parent) {}
    virtual ~PlaylistItemModel() {};

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;
    bool insertRows(int position, int rows, const QModelIndex& index = QModelIndex()) override;
    bool removeRows(int position, int rows, const QModelIndex& index = QModelIndex()) override;

    void set_selected(const QList<int>& rows);
    bool is_selected(int row) const ;

protected:
    MetaDataList        m_meta;
};

#endif /* PLAYLISTITEMMODEL_H_ */
