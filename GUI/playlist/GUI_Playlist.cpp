/* Copyright (C) 2011  Lucio Carreras
 *
 * Copyright (C) 2014-2022 J.F.Dockes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QAction>
#include <QGesture>
#include <QResizeEvent>
#include <QAction>
#include <QToolBar>

#include <libupnpp/log.h>

#include "HelperStructs/Helper.h"
#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Style.h"
#include "HelperStructs/globals.h"
#include "GUI/playlist/GUI_Playlist.h"
#include "GUI/playlist/model/PlaylistItemModel.h"
#include "ui_GUI_Playlist.h"

GUI_Playlist::GUI_Playlist(QWidget *parent)
    : QWidget(parent)
{
    ui = new Ui::Playlist_Window();
    ui->setupUi(this);
    m_tb_order = new QToolBar("", this);
    ui->HLModeButtons->addWidget(m_tb_order);
    // avoid loss of the shuffle icon too early, no minimum width required    
    ui->HLModeButtons->addItem(
        new QSpacerItem(1, 20, QSizePolicy::Expanding, QSizePolicy::Minimum)); 
    m_tb_mode = new QToolBar("", this);
    ui->HLModeButtons->addWidget(m_tb_mode);
    // avoid loss of the dynamic icon too early, no minimum width required
    ui->HLModeButtons->addItem(
        new QSpacerItem(1, 20, QSizePolicy::Expanding, QSizePolicy::Minimum)); 
    m_tb_numbers = new QToolBar("", this);
    ui->HLModeButtons->addWidget(m_tb_numbers);
    setupButtons();

    QAction* clear_action = new QAction(this);
    clear_action->setShortcut(QKeySequence("Ctrl+."));
    clear_action->setShortcutContext(Qt::WidgetWithChildrenShortcut);
    connect(clear_action, SIGNAL(triggered()), this, SIGNAL(clear_playlist()));
    this->addAction(clear_action);

    CSettingsStorage* settings = CSettingsStorage::getInstance();
    bool small_playlist_items = settings->getShowSmallPlaylist();

    ui->listView->show_big_items(!small_playlist_items);

    setAcceptDrops(true);

    grabGesture(Qt::TapAndHoldGesture);

    connect(ui->btn_clear, SIGNAL(clicked()), this, SIGNAL(clear_playlist()));

    connect(ui->btn_rd, SIGNAL(toggled(bool)), SIGNAL(sig_radio(bool)));
    connect(ui->listView, SIGNAL(sig_metadata_dropped(const MetaDataList&, int)),
            this, SLOT(metadata_dropped(const MetaDataList&, int)));
    connect(ui->listView, SIGNAL(sig_rows_removed(const QList<int>&, bool)),
            this, SLOT(rows_removed(const QList<int>&, bool)));
    connect(ui->listView, SIGNAL(sig_sort_tno()), this, SIGNAL(sig_sort_tno()));
    connect(ui->listView, SIGNAL(sig_selected_list(const QList<int>&)),
            this, SIGNAL(selected_list(const QList<int>&)));
    connect(ui->listView, SIGNAL(sig_double_clicked(int)), this, SLOT(double_clicked(int)));
}


void GUI_Playlist::setActions(QAction *repeat_all, QAction *shuffle, QAction *append,
                              QAction *replace, QAction *autoplay, QAction *show_numbers)
{
    m_repeat_all = repeat_all;
    m_shuffle = shuffle;
    m_append = append;
    m_replace = replace;
    m_autoplay = autoplay;
    m_show_numbers = show_numbers;
    m_show_numbers->setChecked(CSettingsStorage::getInstance()->getPlaylistNumbers());
}


// This is for restoring from settings
void GUI_Playlist::setMode(int mode)
{
    _playlist_mode = mode;

    m_repeat_all->setChecked(_playlist_mode & PLM_repAll);
    m_shuffle->setChecked(_playlist_mode & PLM_shuffle);
    m_append->setChecked(_playlist_mode & PLM_append);
    m_replace->setChecked(_playlist_mode & PLM_replace);
    m_append->setDisabled(_playlist_mode & PLM_replace);
    m_autoplay->setChecked(_playlist_mode & PLM_playAdded);
}

// This is for setting the modes from an openhome player. Only the
// repeat and shuffle bits
void GUI_Playlist::setPlayerMode(int mode)
{
    copyplmbit(_playlist_mode, mode, PLM_repAll);
    m_repeat_all->setChecked(_playlist_mode & PLM_repAll);
    copyplmbit(_playlist_mode, mode, PLM_shuffle);
    m_shuffle->setChecked(_playlist_mode & PLM_shuffle);
}

bool GUI_Playlist::event(QEvent *event)
{
    if (event->type() == QEvent::Gesture) {
        return gestureEvent(static_cast<QGestureEvent*>(event));
    }
    return QWidget::event(event);
}

// Note that we only grab tapandhold at the moment, the rest is for possible future use, too lazy to
// erase it and risking having to retype it.
bool GUI_Playlist::gestureEvent(QGestureEvent *event)
{
    if (QGesture *tap = event->gesture(Qt::TapGesture)) {
        LOGDEB("GUI_Playlist: got tap, state: " << tap->state() << "\n");
    } else if (QGesture *taphold = event->gesture(Qt::TapAndHoldGesture)) {
        LOGDEB("GUI_Playlist: got taphold, state: " << taphold->state()  << " has hotspot " <<
               taphold->hasHotSpot() << "\n");
        if (taphold->state() == Qt::GestureFinished) {
            auto fpoint = taphold->hotSpot();
            auto point = QPoint(fpoint.x(), fpoint.y());
            // PlaylistView expects a relative position (QAbstractScrollArea is an exception to the
            // general case, see doc for customContextMenuRequested).
            ui->listView->show_context_menu(ui->listView->mapFromGlobal(point));
        }
    } else if (QGesture *pan = event->gesture(Qt::PanGesture)) {
        LOGDEB("GUI_Playlist: got pan, state: " << pan->state() << "\n");
    } else if (QGesture *pinch = event->gesture(Qt::PinchGesture)) {
        LOGDEB("GUI_Playlist: got pinch, state: " << pinch->state() << "\n");
    } else if (QGesture *swipe = event->gesture(Qt::SwipeGesture)) {
        LOGDEB("GUI_Playlist: got swipe, state: " << swipe->state() << "\n");
    }

    return true;
}

void GUI_Playlist::focusInEvent(QFocusEvent *)
{
    this->ui->listView->setFocus();
}

void GUI_Playlist::takeFocus()
{
    focusInEvent(nullptr);
}

void GUI_Playlist::psl_openhome_renderer(bool onoff)
{
    this->ui->btn_rd->setVisible(onoff);
    this->ui->btn_pl->setVisible(onoff);
}

void GUI_Playlist::psl_openhome_source_radio(bool onoff)
{
    // For some reason using btn_rd->setChecked(false) does not work, we have to setchecked(true) on
    // the other button. Looks like a Qt bug, but whatever...
    if (onoff) {
        this->ui->btn_rd->setChecked(onoff);
    } else {
        this->ui->btn_pl->setChecked(!onoff);
    }
}

void GUI_Playlist::psl_next_group_html(QString html)
{
    if (html.isEmpty()) {
        this->ui->btn_clear->setToolTip(tr("Clear Playlist"));
    } else {
        this->ui->btn_clear->setToolTip(html);
    }
}

void GUI_Playlist::setupButtons(int)
{
    if (m_tb_order && m_repeat_all) {
        m_tb_order->addAction(m_repeat_all);
        m_tb_order->addAction(m_shuffle);
        m_tb_mode->addAction(m_append);
        m_tb_mode->addAction(m_replace);
        m_tb_mode->addAction(m_autoplay);
        m_tb_numbers->addAction(m_show_numbers);
    }
}

#if (defined(Q_OS_MACOS) && (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0))) // The different light and dark icons could be used in general
void GUI_Playlist::setupButtonIcons(bool dark)
{
    if (dark) {
        QIcon::setThemeName("dark");
    } else {
        QIcon::setThemeName("light");
    }
    m_repeat_all->setIcon(QIcon::fromTheme("repAll"));
    m_shuffle->setIcon(QIcon::fromTheme("shuffle"));
    m_append->setIcon(QIcon::fromTheme("append"));
    m_autoplay->setIcon(QIcon::fromTheme("dynamic"));
    m_show_numbers->setIcon(QIcon::fromTheme("numbers"));
}
#endif

// Slot: comes from listview
void GUI_Playlist::metadata_dropped(const MetaDataList& v_md, int row)
{
    emit dropped_tracks(v_md, row);
}

// SLOT: fill all tracks in v_metadata into playlist
void GUI_Playlist::fillPlaylist(MetaDataList& v_metadata, int cur_play_idx, int)
{
    ui->listView->setStyleSheet(styleSheet());
    ui->listView->fill(v_metadata, cur_play_idx);
    _total_msecs = 0;

    int actions = ENTRY_REMOVE | ENTRY_SORT_TNO | ENTRY_INVERT_SELECTION |
        ENTRY_CLEAR_SELECTION | ENTRY_SELECT_ALL;

    ui->listView->set_context_menu_actions(actions);

    for(const auto& md: v_metadata) {
        _total_msecs += md.length_ms;
    }

    set_total_time_label();
}

void GUI_Playlist::double_clicked(int row)
{
    emit row_activated(row);
}

void GUI_Playlist::track_changed(int row)
{
    ui->listView->set_current_track(row);
}

// This is connected to the UI buttons
void GUI_Playlist::playlist_mode_changed_slot()
{
    _playlist_mode = 0;
    if (m_repeat_all->isChecked())
        _playlist_mode |= PLM_repAll;
    if (m_shuffle->isChecked())
        _playlist_mode |= PLM_shuffle;
    if (m_append->isChecked())
        _playlist_mode |= PLM_append;
    if (m_replace->isChecked()) 
        _playlist_mode |= PLM_replace;
    if (m_autoplay->isChecked()) 
        _playlist_mode |= PLM_playAdded;

    m_append->setDisabled(_playlist_mode & PLM_replace);

    emit playlist_mode_changed(_playlist_mode);
}

void GUI_Playlist::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void GUI_Playlist::dragEnterEvent(QDragEnterEvent* event)
{
    event->accept();
}

void GUI_Playlist::dragMoveEvent(QDragMoveEvent* event)
{
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    QPoint pos = event->position().toPoint();
#else
    QPoint pos = event->pos();
#endif
    if (pos.y() < ui->listView->y()) {
        ui->listView->scrollUp();
    } else if (pos.y() > ui->listView->y() + ui->listView->height()) {
        ui->listView->scrollDown();
    }
}

void GUI_Playlist::set_total_time_label()
{
    QString text = "";

    ui->lab_totalTime->setContentsMargins(0, 2, 0, 2);

    int n_rows = ui->listView->get_num_rows();
    QString playlist_string = text + QString::number(n_rows);

    if (n_rows == 1) {
        playlist_string += tr(" Track - ");
    } else {
        playlist_string += tr(" Tracks - ");
    }

    playlist_string += Helper::cvtMsecs2TitleLengthString(_total_msecs, false);

    ui->lab_totalTime->setText(playlist_string);
}

void GUI_Playlist::psl_show_small_playlist_items(bool small_playlist_items)
{
    ui->listView->show_big_items(!small_playlist_items);
}

void GUI_Playlist::btn_numbers_changed(bool b)
{
    CSettingsStorage::getInstance()->setPlaylistNumbers(b);
    ui->listView->reset();
}

void GUI_Playlist::rows_removed(const QList<int>& lst, bool select_next_row)
{
    emit sig_rows_removed(lst, select_next_row);
}

void GUI_Playlist::resizeEvent(QResizeEvent *event)
{
    int mywidth = event->size().width();
    //std::cerr << "GUI_Playlist::resizeEvent: width: " << mywidth << "\n";
    float scale = Style::get_ui_scale();
    if (mywidth / scale > 350) {
        ui->btn_pl->setText("Playlist");
        ui->btn_rd->setText("Radio");
    } else {
        ui->btn_pl->setText("PL");
        ui->btn_rd->setText("RD");
    }
    QWidget::resizeEvent(event);
}
