/* CustomMimeData.h */

/* Copyright (C) 2013  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CUSTOMMIMEDATA_H
#define _CUSTOMMIMEDATA_H

#include <QMimeData>

#include "HelperStructs/MetaData.h"

class CustomMimeData : public QMimeData {
private:
    MetaDataList _v_md;

public:
    CustomMimeData()
        : QMimeData() {
    }

    void setMetaData(const MetaDataList& v_md) {
        _v_md = v_md;
    }

    uint getMetaData(MetaDataList& v_md) const {
        if(_v_md.size()) {
            // Why not reset the output? Inherited from sayonara and
            // left alone.
            v_md = _v_md;
        }
        return _v_md.size();
    }

    bool hasMetaData() const {
        return _v_md.size();
    }
};

#endif
