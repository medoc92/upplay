/* Copyright (C) 2011  Lucio Carreras
 *
 * Copyright (C) 2014-2022 J.F.Dockes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QApplication>
#include <QUrl>
#include <QScrollBar>
#include <QTimer>
#include <QShortcut>

#include <libupnpp/log.h>

#include "CustomMimeData.h"
#include "HelperStructs/Helper.h"
#include "PlaylistView.h"
#include "GUI/playlist/delegate/PlaylistItemDelegate.h"

// We identify our special dnd data by its specific text content.
static const QString mydatatextname("upplaytracks");

// We avoid scrolling to make the currently playing track visible if the
// user is currently active (e.g. selecting tracks). Reset after 10 S
static const int user_activity_timeout_ms = 10 * 1000;

PlaylistView::PlaylistView(QWidget* parent)
    : QListView(parent), m_usertimer(new QTimer(this))
{
    m_usertimer->setSingleShot(true);
    m_usertimer->setInterval(user_activity_timeout_ms);

    setMouseTracking(true);

    _model = new PlaylistItemModel(this);
    setModel(_model);
    _delegate = new PlaylistItemDelegate(this, true);

    setDragEnabled(true);
    setAcceptDrops(true);
    setSelectionRectVisible(true);
    setAlternatingRowColors(true);
    setMovement(QListView::Free);

    setContextMenuPolicy(Qt::CustomContextMenu);
    init_rc_menu();
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(show_context_menu(const QPoint&)));

    connect(this, SIGNAL(pressed(const QModelIndex&)), this, SLOT(row_pressed(const QModelIndex&)));
    connect(this, SIGNAL(doubleClicked(const QModelIndex&)),
            this, SLOT(row_double_clicked(const QModelIndex&)));
    connect(horizontalScrollBar(), SIGNAL(sliderPressed()), m_usertimer, SLOT(start()));
    connect(verticalScrollBar(), SIGNAL(sliderPressed()), m_usertimer, SLOT(start()));
    QShortcut *sc = new QShortcut(QKeySequence("Esc"), this);
    connect(sc, SIGNAL (activated()), this, SLOT(clear_selection()));
}

PlaylistView::~PlaylistView()
{
}

void PlaylistView::mousePressEvent(QMouseEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);

    if (event->button() == Qt::LeftButton) {
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
        QPoint pos = event->position().toPoint();
#else
        QPoint pos = event->pos();
#endif
        m_drag_start_position = pos;
    }
    QListView::mousePressEvent(event);
}


void PlaylistView::show_context_menu(const QPoint& pos)
{
    // If no menu or dragndrop is active, do nothing
    if (!_rc_menu || _qDrag)
        return;
    auto gpos = mapToGlobal(pos);
    _rc_menu->exec(gpos);
}


void PlaylistView::mouseMoveEvent(QMouseEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);

    if (!(event->buttons() & Qt::LeftButton)) {
        return;
    }

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    QPoint pos = event->position().toPoint();
#else
    QPoint pos = event->pos();
#endif
    if ((pos - m_drag_start_position).manhattanLength() < QApplication::startDragDistance()) {
        return;
    }

    calc_selections();

    MetaDataList v_md;
    foreach(int row, _cur_selected_rows) {
        QVariant mdvariant = _model->data(_model->index(row), Qt::WhatsThisRole);
        MetaData md;
        if (!MetaData::fromVariant(mdvariant, md)) {
            continue;
        }
        v_md.push_back(md);
    }
    set_mimedata(v_md, mydatatextname);

    _qDrag->exec(Qt::CopyAction);
}


void PlaylistView::mouseReleaseEvent(QMouseEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);

    if (event->button() == Qt::LeftButton) {
        // Jf note: I don't think that this can happen as, if a dnd is ongoing the qdrag is managing
        // the release event and we don't get it.
        delete _qDrag;
        _qDrag = nullptr;
    }
    QListView::mouseReleaseEvent(event);
}


// get the min index of selected rows
int PlaylistView::get_min_selected()
{
    QModelIndexList lst = this->selectedIndexes();
    int min_row = 5000000;

    if (lst.size() == 0) {
        return 0;
    }

    foreach(QModelIndex i, lst) {
        if (i.row() < min_row) {
            min_row = i.row();
        }
    }

    return min_row;
}

// mark row as currently pressed
void PlaylistView::goto_row(int row)
{
    if ((row >= _model->rowCount()) || (row < 0)) {
        return;
    }

    this->clearSelection();

    QModelIndex idx = _model->index(row, 0);
    QList<int> lst_rows;
    lst_rows << row;
    this->select_rows(lst_rows);
    row_pressed(idx);
    this->scrollTo(idx);
}

void PlaylistView::keyPressEvent(QKeyEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);

    int key = event->key();

    Qt::KeyboardModifiers  modifiers = event->modifiers();
    int min_row = get_min_selected();
    int new_row = -1;

    switch (key) {
    case Qt::Key_Escape:
        clear_selection();
        break;
    case Qt::Key_A:
        if (modifiers & Qt::ControlModifier) {
            select_all();
        }
        break;

    case Qt::Key_Delete:
        remove_cur_selected_rows();
        break;

    case Qt::Key_Up:
        if (modifiers & Qt::ControlModifier) {
            break;
        }

        if (min_row > 0) {
            new_row = min_row - 1;
        } else {
            new_row = 0;
        }
        break;
    case Qt::Key_Down:
        if (modifiers & Qt::ControlModifier) {
            break;
        }

        if (min_row < _model->rowCount() - 1) {
            new_row = min_row + 1;
        } else {
            new_row = _model->rowCount() - 1;
        }
        break;

    case Qt::Key_PageUp:
        if (min_row > 10) {
            new_row = min_row - 10;
        } else {
            new_row = 0;
        }
        break;
    case Qt::Key_PageDown:
        if (min_row < _model->rowCount() - 10) {
            new_row = min_row + 10;
        } else {
            new_row =  _model->rowCount() - 1;
        }
        break;

    case Qt::Key_End:
        new_row = _model->rowCount() - 1;
        break;

    case Qt::Key_Home:
        new_row = 0;
        break;

    case Qt::Key_Return:
    case Qt::Key_Enter:
        this->sig_double_clicked(min_row);
        break;

    default:
        QListView::keyPressEvent(event);
        return;
    }

    if (new_row != -1) {
        goto_row(new_row);
        QListView::keyPressEvent(event);
    }
}

void PlaylistView::resizeEvent(QResizeEvent *e)
{
    this->set_delegate_max_width(_model->rowCount());
    e->accept();
}

void PlaylistView::init_rc_menu()
{
    _rc_menu = new ContextMenu(this);

    connect(_rc_menu, SIGNAL(sig_remove_clicked()), this, SLOT(remove_clicked()));
    connect(_rc_menu, SIGNAL(sig_sort_tno_clicked()), this, SIGNAL(sig_sort_tno()));
    connect(_rc_menu, SIGNAL(sig_invert_selection_clicked()), this, SLOT(invert_selection()));
    connect(_rc_menu, SIGNAL(sig_clear_selection_clicked()), this, SLOT(clear_selection()));
    connect(_rc_menu, SIGNAL(sig_select_all_clicked()), this, SLOT(select_all()));
}

void PlaylistView::set_context_menu_actions(int actions)
{
    if (!_rc_menu) {
        init_rc_menu();
    }
    _rc_menu->setup_entries(actions);
}

void PlaylistView::set_mimedata(MetaDataList& v_md, QString text)
{
    CustomMimeData* mimedata = new CustomMimeData();

    QList<QUrl> urls;
    for (const auto& md : v_md) {
        urls << md.url;
    }

    mimedata->setMetaData(v_md);
    mimedata->setText(text);
    mimedata->setUrls(urls);

    delete _qDrag;
    _qDrag = new QDrag(this);
    _qDrag->setMimeData(mimedata);

    connect(_qDrag, SIGNAL(destroyed()), this, SLOT(forbid_mimedata_destroyable()));
}

void PlaylistView::forbid_mimedata_destroyable()
{
    _qDrag = nullptr;
}

int PlaylistView::get_num_rows()
{
    return _model->rowCount();
}

#define CAN_AUTOSCROLL (!(m_usertimer->isActive() ||                    \
                          horizontalScrollBar()->isSliderDown() ||      \
                          verticalScrollBar()->isSliderDown()))

void PlaylistView::set_current_track(int row)
{
    for (int i = 0; i < _model->rowCount(); i++) {
        QModelIndex idx = _model->index(i);
        MetaData md;
        QVariant v = _model->data(idx, Qt::WhatsThisRole);
        if (!MetaData::fromVariant(v, md)) {
            continue;
        }
        md.pl_playing = (row == i);

        _model->setData(idx, md.toVariant(), Qt::EditRole);
    }

    QModelIndex new_idx = _model->index(row);

    // Don't move the playlist around if the user is doing something
    if (CAN_AUTOSCROLL) {
        scrollTo(new_idx,  QListView::EnsureVisible);
    }
}


void PlaylistView::remove_clicked()
{
    remove_cur_selected_rows();
}

void PlaylistView::clear()
{
    clear_selection();
    _model->removeRows(0, _model->rowCount());
}


void PlaylistView::fill(MetaDataList& v_metadata, int cur_play_idx)
{

    this->set_delegate_max_width((int) v_metadata.size());

    _model->removeRows(0, _model->rowCount());
    if (v_metadata.size() == 0) {
        return;
    }

    _model->insertRows(0, v_metadata.size());
    _cur_selected_rows.clear();

    QModelIndex idx_cur_playing = _model->index(0);
    for (int i = 0; i < int(v_metadata.size()); i++) {
        MetaData md = v_metadata[i];

        QModelIndex model_idx = _model->index(i, 0);

        md.pl_playing = (cur_play_idx == int(i));
        if (md.pl_playing) {
            idx_cur_playing = model_idx;
        }

        if (md.pl_selected) {
            _cur_selected_rows << i;
        }

        _model->setData(model_idx, md.toVariant(), Qt::EditRole);
    }

    _model->set_selected(_cur_selected_rows);
    this->select_rows(_cur_selected_rows);

    // Don't move the playlist around if the user is doing something
    if (CAN_AUTOSCROLL) {
        this->scrollTo(idx_cur_playing, QListView::EnsureVisible);
    }
}

void PlaylistView::row_pressed(const QModelIndex& idx)
{
    Q_UNUSED(idx);
    LOGDEB1("PlaylistView::row_pressed: row: " << idx.row() << "\n");
    calc_selections();
}

void PlaylistView::row_double_clicked(const QModelIndex& idx)
{
    if (idx.isValid()) {
        emit sig_double_clicked(idx.row());
    }
}

void PlaylistView::clear_selection()
{
    LOGDEB0("PlaylistView::clear_selection\n");
    this->selectionModel()->clearSelection();
    this->clearSelection();
    calc_selections();
}

void PlaylistView::select_rows(QList<int> lst)
{
    LOGDEB0("PlaylistView::select_rows: lstsz " << lst.size() << "\n");
    QItemSelectionModel* sm = this->selectionModel();
    QItemSelection sel;

    foreach(int row, lst) {
        QModelIndex idx = _model->index(row);

        sm->select(idx, QItemSelectionModel::Select);
        sel.merge(sm->selection(), QItemSelectionModel::Select);
    }

    sm->clearSelection();
    sm->select(sel, QItemSelectionModel::Select);

    calc_selections();
}

void PlaylistView::invert_selection()
{
    LOGDEB0("PlaylistView::invert_selection\n");
    for (int row = 0; row < model()->rowCount(); row++) {
        this->selectionModel()->select(_model->index(row), QItemSelectionModel::Toggle);
    }
    calc_selections();
}

void PlaylistView::select_all()
{
    LOGDEB0("PlaylistView::select_all\n");
    selectAll();
    calc_selections();
}

void PlaylistView::calc_selections()
{
    _cur_selected_rows.clear();

    foreach(QModelIndex model_idx, selectionModel()->selectedRows()) {
        _cur_selected_rows.push_back(model_idx.row());
    }

    _model->set_selected(_cur_selected_rows);

    emit sig_selected_list(_cur_selected_rows);
}


// remove the black line under the titles
void  PlaylistView::clear_drag_lines(int row)
{
    for (int i = row - 3; i <= row + 3; i++) {

        QModelIndex idx = _model->index(i, 0);
        if (!idx.isValid() || idx.row() < 0 || idx.row() >= _model->rowCount()) {
            continue;
        }

        QVariant mdVariant = _model->data(idx, Qt::WhatsThisRole);
        MetaData md;
        if (MetaData::fromVariant(mdVariant, md)) {
            md.pl_dragged = false;
            _model->setData(idx, md.toVariant(), Qt::EditRole);
        }
    }
}

int PlaylistView::calc_dd_line(QPoint pos)
{
    LOGDEB1("calc_dd_line: Y " << pos.y() << "\n");
    if (pos.y() < 0) {
        return -1;
    }
    int row = this->indexAt(pos).row();
    LOGDEB1("calc_dd_line: ROW from indexAt: " << row << "\n");

    // If we get row == -1 at this point, the position is beyond the last entry, use the row count
    if (row <= -1) {
        row = _model->rowCount();
    }

    // Else we want to insert before the dropped row (inserting after would make it difficult to
    // insert at the top
    row -= 1;

    LOGDEB1("calc_dd_line: returning row: " << row << "\n");
    return row;
}

// Have to accept the event, else we don't get the drag move ones.
void PlaylistView::dragEnterEvent(QDragEnterEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);
    event->accept();
}

void PlaylistView::dragMoveEvent(QDragMoveEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    QPoint pos = event->position().toPoint();
#else
    QPoint pos = event->pos();
#endif

    if (pos.y() < this->y()) {
        this->scrollUp();
    } else if (pos.y() > this->y() + this->height()) {
        this->scrollDown();
    }
    if (!event->mimeData()) {
        return;
    }
    event->accept();

    int row = calc_dd_line(pos);
    _last_known_drag_row = row;
    clear_drag_lines(row);

    // paint line
    QModelIndex cur_idx = _model->index(row, 0);
    QVariant mdVariant = _model->data(cur_idx, Qt::WhatsThisRole);
    MetaData md;
    if (!MetaData::fromVariant(mdVariant, md)) {
        return;
    }

    md.pl_dragged = true;
    _model->setData(cur_idx, md.toVariant(), Qt::EditRole);
}


// Drag leaves us, all lines has to be cleared
void PlaylistView::dragLeaveEvent(QDragLeaveEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);
    event->accept();

    clear_drag_lines(_last_known_drag_row);
}


void PlaylistView::dropEvent(QDropEvent* event)
{
    m_usertimer->start(user_activity_timeout_ms);
    event->accept();

    if (!event->mimeData()) {
        return;
    }
    handle_drop(event);
}

void PlaylistView::handle_drop(QDropEvent* event)
{
    if (_cur_selected_rows.empty()) {
        LOGDEB0("handle_drop: selection empty\n");
        return;
    }

    QList<int> affected_rows;

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    QPoint pos = event->position().toPoint();
#else
    QPoint pos = event->pos();
#endif
    
    // where did i drop?
    int row = calc_dd_line(pos);
    LOGDEB0("handle_drop: got row " << row << "\n");
    if (_cur_selected_rows.contains(row)) {
        event->ignore();
        clear_drag_lines(row);
        LOGDEB0("handle_drop: drop row within selection, cancel.\n");
        return;
    }
    QString text;
    if (event->mimeData()->hasText()) {
        text = event->mimeData()->text();
    }
    if (text != mydatatextname) {
        // Not ours ??
        LOGDEB0("handle_drop: data not ours\n");
        return;
    }

    if (_cur_selected_rows.first() < row) {
        row -= _cur_selected_rows.size();
    }

    const CustomMimeData* d = (const CustomMimeData*) event->mimeData();
    MetaDataList v_metadata;
    uint sz = d->getMetaData(v_metadata);
    if (sz == 0) {
        LOGDEB0("handle_drop: no data\n");
        return;
    }
    for (int i = 0; i < int(v_metadata.size()); i++) {
        affected_rows << i + row + 1;
    }
    if (!affected_rows.empty()) {
        remove_cur_selected_rows(true);
        emit sig_metadata_dropped(v_metadata, row);
    }
    clear_selection();
}


void PlaylistView::scrollUp()
{
    QPoint p(5, 5);
    int cur_row = this->indexAt(p).row();
    if (cur_row <= 0) {
        return;
    }

    this->scrollTo(_model->index(cur_row - 1));
}

void PlaylistView::scrollDown()
{
    QPoint p(5, this->y() + this->height() - 5);
    int cur_row = this->indexAt(p).row();
    if (cur_row <= 0) {
        return;
    }

    this->scrollTo(_model->index(cur_row - 1));
}

void PlaylistView::remove_cur_selected_rows(bool is_drag_drop)
{
    emit sig_rows_removed(_cur_selected_rows, is_drag_drop);
}


void PlaylistView::show_big_items(bool big)
{
    delete _delegate;
    _delegate = new PlaylistItemDelegate(this, !big);

    this->set_delegate_max_width(_model->rowCount());
    this->setItemDelegate(_delegate);

    this->reset();
}


void PlaylistView::set_delegate_max_width(int n_items)
{
    if (nullptr == _delegate)
        return;
    bool scrollbar_visible = ((n_items * _delegate->rowHeight()) >= this->height());

    int max_width = width();
    if (scrollbar_visible) {
        max_width -= verticalScrollBar()->width();
    }
    _delegate->setMaxWidth(max_width);
}

#if 0
// This was a trial to call the accessibility code explicitely. Kept around for ref, but it seems
// that the only way to get things working is to actually call QListView::keyPressEvent(), which
// we do from our own keypressEvent()
void PlaylistView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    if(QAccessible::isActive()){
        QVariant v = _model->data(current, Qt::AccessibleTextRole);
        if (v.isValid()) {
            QAccessibleValueChangeEvent ev(this, v);
            QAccessible::updateAccessibility(&ev);
        }
        LOGERR("PlaylistView::currentChanged: accessibility data NOT VALID\n");
    }
}
QListView::currentChanged(current, previous);
}
#endif

