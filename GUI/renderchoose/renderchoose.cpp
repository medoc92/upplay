/* Copyright (C) 2021 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include "renderchoose.h"

#include "upqo/discovery_qo.h"
#include "upadapt/upputils.h"
#include "upadapt/rendererlist.h"
#include "libupnpp/control/avtransport.hxx"
#include "libupnpp/control/ohproduct.hxx"

using namespace UPnPClient;

void RenderChooseDLG::init()
{
    auto discoqo = new DiscoveryQO(this);
    qRegisterMetaType<UPnPClient::UPnPDeviceDesc>();
    connect(discoqo, SIGNAL(sig_discovered(UPnPClient::UPnPDeviceDesc)),
            this, SLOT(onNewDevice(UPnPClient::UPnPDeviceDesc)));
    discoqo->activate();
}

void RenderChooseDLG::onNewDevice(UPnPClient::UPnPDeviceDesc newdev)
{
    // Already there ?
    auto it = std::find_if(m_devices.begin(), m_devices.end(),
                           [newdev] (UPnPClient::UPnPDeviceDesc dev) {
                               return newdev.UDN == dev.UDN;
                           });
    if (it != m_devices.end()) {
        return;
    }

    // Test for requested services.
    bool ohonly = (m_flags & int(RSF_OHONLY)) != 0;
    bool upnpavonly = (m_flags & int(RSF_AVTONLY)) != 0;
    bool hasopenhome{false};
    bool hasupnpav{false};
    for (const auto& srv : newdev.services) {
        if (OHProduct::isOHPrService(srv.serviceType)) {
            hasopenhome = true;
        }
        if (AVTransport::isAVTService(srv.serviceType)) {
            hasupnpav = true;
        }
    }
    if (ohonly || upnpavonly) {
        if (ohonly && !hasopenhome) {
            return;
        }
        if (upnpavonly && !hasupnpav) {
            return;
        }
    }
    
    // Device should be added to list
    QString fname = u8s2qs(newdev.friendlyName);
    if (m_curfname == fname) {
        QListWidgetItem *item = new QListWidgetItem(fname);
        QFont font = rndsLW->font();
        font.setBold(true);
        item->setFont(font);
        rndsLW->addItem(item);
        rndsLW->setCurrentItem(item);
    } else {
        rndsLW->addItem(fname);
    }
    m_devices.push_back(newdev);
}
