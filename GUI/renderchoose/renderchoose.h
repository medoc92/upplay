/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _RENDERCHOOSE_H_INCLUDED_
#define _RENDERCHOOSE_H_INCLUDED_
#include <QString>

#include "ui_renderchoose.h"
#include "upadapt/rendererlist.h"

class RenderChooseDLG : public QDialog, public Ui::RenderChooseDLG {
    Q_OBJECT;

public:
    RenderChooseDLG(int flags, const QString& curfname, QWidget *parent = 0) 
        : QDialog(parent), m_flags(flags), m_curfname(curfname) {
        setupUi(this);
        init();
    }
    void init();
    const std::vector<UPnPClient::UPnPDeviceDesc>& getdevices() {
        return m_devices;
    }

public slots:
    void onNewDevice(UPnPClient::UPnPDeviceDesc dev);

private:
    std::vector<UPnPClient::UPnPDeviceDesc> m_devices;
    int m_flags;
    QString m_curfname;
};

#endif /* _RENDERCHOOSE_H_INCLUDED_ */
