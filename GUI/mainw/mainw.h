/* Copyright (C) 2011  Lucio Carreras
 *
 * Copyright (C) 2014-2022 J.F. Dockes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _MAINW_H_INCLUDED_
#define _MAINW_H_INCLUDED_

#include <QMainWindow>
#include <QList>
#include <QKeySequence>

#include "trayicon.h"
#include "HelperStructs/Helper.h"

class QTemporaryFile;
class QSplitter;
class QTabWidget;
class QAction;
class QCloseEvent;
class QNetworkAccessManager;
class QNetworkReply;
class QVBoxLayout;
class QPushButton;

class Application;
class GUI_Playlist;
class PlayerWidget;
class DirBrowser;

namespace Ui {
class Upplay;
}

class GUI_Player : public QMainWindow {
    Q_OBJECT
public:
    // GUI_EXP is for misc experiments, and should not be used for anything else
    enum Layout {GUIP_CLASSIC, GUIP_TABBED, GUIP_EXP};
    explicit GUI_Player(Application *upapp, Layout lo = GUIP_CLASSIC, QWidget *parent = 0);
    ~GUI_Player();

public slots:

    void really_close(bool=false);
    void trayItemActivated (QSystemTrayIcon::ActivationReason reason);

    // For restoring from settings
    void setVolume(int vol);
    
    // Reflect externally triggered audio changes in ui
    void update_track(const MetaData& in);
    // Systray notification. 
    void onNotify(const MetaData& md);
    void setCurrentPosition(quint32 pos_sec);
    void stopped();
    void playing();
    void paused();
    void setVolumeUi(int volume_percent);
    void setMuteUi(bool);
    void enableSourceSelect(bool);
    // When renderer choosen: openhome or not?
    void psl_openhome_renderer(bool, const QString& friendlyname);
    void onPrefsChanged();
#if (defined(Q_OS_MACOS) && (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)))
    void onColorsChanged(Qt::ColorScheme color_scheme);
#endif
    void onFocusNext();
    // This is called with true if the focus goes to the prefs window so that SPC can be used to
    // check/uncheck prefs
    void disablePlayControl(bool);
    // This is for connecting the search panel close/open signals to the showSearch qaction.
    void hideDirSearch();
    void showDirSearch();
    
signals:
    /* Player*/
    void play();
    void pause();
    void stop();
    void backward();
    void forward();
    void sig_mute(bool ison);
    void sig_volume_changed (int);
    void sig_seek(int secs);

    /* File */
    void sig_choose_renderer();
    void sig_choose_source();
    void sig_load_url();
    void sig_save_playlist();
    void sig_load_playlist();
    void sig_open_songcast();

    /* Preferences / View */
    void show_playlists();
    void sig_skin_changed(bool);
    void showSearchPanel(bool);
    void sig_sortprefs();
    void sig_preferences();

    // Closing. See comments in closeEvent()
    void sig_quitting();
    
    /* Dump device description (semi-hidden, only triggered by shortcut) */
    void sig_dumpDescription();

private slots:
    // These are connected to ui signals (either tray or main ctl),
    // and coordinate stuff.
    void onPlayActivated();
    void onPauseActivated();
    void onStopActivated();
    void onBackwardActivated();
    void onForwardActivated();
    void onJumpForwardActivated();
    void onJumpBackwardActivated();
    void onMuteActivated(bool);
    void onVolumeStepActivated(int val);
    void onVolumeHigherActivated();
    void onVolumeLowerActivated();
    void showLibrary(bool, bool resize=true);
    void showPlaylist(bool, bool resize=true);
    void show_fullscreen_toggled(bool);
    void about(bool b=false);
    void help(bool b=false);
    void onIdle();
    
    void sl_no_cover_available();
    void sl_cover_fetch_done(QNetworkReply*);

public:
    GUI_Playlist *playlist() {
        return *playlist_wp;
    }
    PlayerWidget *player() {
        return *player_wp;
    }
    DirBrowser *library() {
        return *library_wp;
    }


protected:
    void closeEvent(QCloseEvent* e) override;
    bool eventFilter(QObject *obj, QEvent *ev) override;
    
private:
    Application *m_upapp{nullptr};
    Ui::Upplay *ui{nullptr};
    QNetworkAccessManager *m_netmanager{nullptr};
    GUI_TrayIcon *m_trayIcon{nullptr};
    MetaData m_metadata;
    bool m_metadata_available{false};
    bool m_overridemin2tray;
    int m_library_width{600};
    int m_library_stretch_factor;
    int m_playlist_height{300};
    int m_playlist_stretch_factor;
    QString m_renderer_friendlyname;

    CSettingsStorage *m_settings{nullptr};
    QTemporaryFile *m_covertempfile{nullptr};
    // Last request for cover data: ignore others
    void *m_currentCoverReply{nullptr};
    Layout m_layout{GUIP_CLASSIC};
    
    QSplitter *splitter{nullptr};
    QVBoxLayout *verticalLayout{nullptr};
    QTabWidget *tabWidget{nullptr};
    QWidget *coverWidget{nullptr};
    // Button version of the top menu.
    QPushButton *menuPB{nullptr};
    QMenu *buttonTopMenu{nullptr};
    QTimer *idleTimer{nullptr};

    enum CurFocus {FOCUS_NONE, FOCUS_PLAYLIST, FOCUS_LIBRARY};
    CurFocus m_curfocus{FOCUS_NONE};

    QAction* m_play_pause_action{nullptr};
    
    DirBrowser **library_wp{nullptr};
    PlayerWidget **player_wp{nullptr};
    GUI_Playlist **playlist_wp{nullptr};

    void classic_ui();
    void tabbed_ui();
    void horizontal_ui();
    // Check config and update our looks
    void setStyle();
    void setupIdleTimer();
    void setupTrayActions();
    void idleDisplay();
    void setupConnections();
    void total_time_changed(qint64);
    void fetch_cover(const QString&);
    QAction* createAction(QKeySequence key_sequence);
    QAction* createAction(QList<QKeySequence>& key_sequences);
    void setCoverSize(int sz);
};

#endif // _MAINW_H_INCLUDED_
