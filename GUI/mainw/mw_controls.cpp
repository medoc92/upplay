// GUI_PlayerButtons.cpp

/* Copyright (C) 2012  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainw.h"
#include "trayicon.h"
#include "application.h"
#include "GUI/playerwidget/playerwidget.h"
#include "GUI/volumewidget/volumewidget.h"
#include "GUI/playctlwidget/playctlwidget.h"
#include "GUI/progresswidget/progresswidget.h"
#include "GUI/mdatawidget/mdatawidget.h"
#include "ui_mainw.h"

/** Slots connected to player or trayicon signals **/
void GUI_Player::onPlayActivated()
{
    emit play();
}

void GUI_Player::onPauseActivated()
{
    emit pause();
}

void GUI_Player::onStopActivated()
{
    emit stop();
}

void GUI_Player::onMuteActivated(bool mute)
{
    emit sig_mute(mute);
}

void GUI_Player::idleDisplay()
{
    MetaData md;
    m_upapp->getIdleMeta(&md);
    player()->mdata()->setData(md);

    player()->progress()->setUi(0);

    m_currentCoverReply = 0;
    sl_no_cover_available();
}

void GUI_Player::onBackwardActivated()
{
    // albumCover->setFocus();
    int cur_pos_sec = (m_metadata.length_ms * player()->progress()->currentValuePc()) / 100000;
    if (cur_pos_sec > 3) {
        emit sig_seek(0);
    } else {
        emit backward();
    }
}

void GUI_Player::onForwardActivated()
{
    emit forward();
}


void GUI_Player::total_time_changed(qint64 total_time)
{
    player()->progress()->setTotalTime(total_time/1000);
}

void GUI_Player::onJumpForwardActivated()
{
    player()->progress()->step(1);
}

void GUI_Player::onJumpBackwardActivated()
{
    player()->progress()->step(-1);
}

// This is called from the external world to update the position
void GUI_Player::setCurrentPosition(quint32 pos_sec)
{
    player()->progress()->setUi(pos_sec);
}

// Called e.g. after reading saved volume from settings: adjust ui and
// set audio volume.
void GUI_Player::setVolume(int pc)
{
    player()->volume()->set(pc);
}

// Called from audio when volume has been changed by another player.
void GUI_Player::setVolumeUi(int pc)
{
    player()->volume()->setUi(pc);
}

void GUI_Player::setMuteUi(bool ismute)
{
    player()->volume()->setMuteUi(ismute);
    if (m_trayIcon)
        m_trayIcon->setMute(ismute);
}

void GUI_Player::onVolumeStepActivated(int val)
{
    if (m_trayIcon) {
        int vol_step = m_trayIcon->get_vol_step();
        player()->volume()->step(vol_step * val);
    }
}

void GUI_Player::onVolumeHigherActivated()
{
    //qDebug() << "GUI_PLayer::volumeHigher";
    player()->volume()->volumeHigher();
}

void GUI_Player::onVolumeLowerActivated()
{
    //qDebug() << "GUI_PLayer::volumeLower";
    player()->volume()->volumeLower();
}
