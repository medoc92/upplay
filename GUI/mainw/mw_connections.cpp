/* Copyright (C) 2013  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainw.h"
#include <QShortcut>
#include <QApplication>
#include <QStyleHints>

#include "GUI/playlist/GUI_Playlist.h"
#include "GUI/playerwidget/playerwidget.h"
#include "GUI/volumewidget/volumewidget.h"
#include "GUI/playctlwidget/playctlwidget.h"
#include "GUI/progresswidget/progresswidget.h"
#include "dirbrowser/dirbrowser.h"
#include "ui_mainw.h"

QAction* GUI_Player::createAction(QList<QKeySequence>& seq_list)
{
    QAction* action = new QAction(this);

    action->setShortcuts(seq_list);
    action->setShortcutContext(Qt::ApplicationShortcut);
    this->addAction(action);
    connect(this, SIGNAL(destroyed()), action, SLOT(deleteLater()));

    return action;
}

QAction* GUI_Player::createAction(QKeySequence seq)
{
    QList<QKeySequence> seq_list;
    seq_list << seq;
    return createAction(seq_list);
}

void GUI_Player::setupConnections()
{
    connect(player()->playctl(), SIGNAL(playrequested()), this, SLOT(onPlayActivated()));
    connect(player()->playctl(), SIGNAL(pauserequested()), this, SLOT(onPauseActivated()));
    connect(player()->playctl(), SIGNAL(forwardrequested()), this, SLOT(onForwardActivated()));
    connect(player()->playctl(), SIGNAL(backwardrequested()), this, SLOT(onBackwardActivated()));
    connect(player()->playctl(), SIGNAL(stoprequested()), this, SLOT(onStopActivated()));
    connect(player()->volume(), SIGNAL(muteChanged(bool)), this, SLOT(onMuteActivated(bool)));
    connect(player()->volume(), SIGNAL(volumeChanged(int)), this, SIGNAL(sig_volume_changed(int)));
    connect(player()->progress(), SIGNAL(seekRequested(int)), this, SIGNAL(sig_seek(int)));
    connect(this, SIGNAL(sig_skin_changed(bool)), library(), SLOT(refresh()));
    connect(this, SIGNAL(sig_quitting()), library(), SLOT(onAboutToExit()));
    connect(this, SIGNAL(showSearchPanel(bool)), library(), SLOT(showSearchPanel(bool)));
    connect(this, SIGNAL(sig_sortprefs()), library(), SLOT(onSortprefs()));
    connect(library(), SIGNAL(sig_next_group_html(QString)),
            playlist(), SLOT(psl_next_group_html(QString)));

#if (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0))
#ifdef Q_OS_MACOS // could likely be used in general when the light/dark mode preference is set to "System"
        connect(qApp->styleHints(), SIGNAL(colorSchemeChanged(Qt::ColorScheme)),
            this, SLOT(onColorsChanged(Qt::ColorScheme)));
#else
        connect(qApp->styleHints(), SIGNAL(colorSchemeChanged(Qt::ColorScheme)),
            this, SLOT(onPrefsChanged()));
#endif
#endif
    
    // file
    connect(ui->actionSelect_Media_Renderer, SIGNAL(triggered(bool)),
            this, SIGNAL(sig_choose_renderer()));
    connect(ui->actionSelect_OH_Source, SIGNAL(triggered(bool)), this, SIGNAL(sig_choose_source()));
    connect(ui->actionLoad_URL, SIGNAL(triggered(bool)), this, SIGNAL(sig_load_url()));
    connect(ui->actionSave_Playlist, SIGNAL(triggered(bool)), this, SIGNAL(sig_save_playlist()));
    connect(ui->actionLoad_Playlist, SIGNAL(triggered(bool)), this, SIGNAL(sig_load_playlist()));
    connect(ui->actionOpen_Songcast, SIGNAL(triggered(bool)), this, SIGNAL(sig_open_songcast()));
    connect(ui->action_Close, SIGNAL(triggered(bool)), this, SLOT(really_close(bool)));

    // view
    connect(ui->action_viewLibrary, SIGNAL(toggled(bool)), this, SLOT(showLibrary(bool)));
    connect(ui->action_viewPlaylist, SIGNAL(toggled(bool)), this, SLOT(showPlaylist(bool)));
    connect(ui->action_viewSearchPanel, SIGNAL(toggled(bool)), this, SIGNAL(showSearchPanel(bool)));
    connect(ui->action_smallPlaylistItems, SIGNAL(toggled(bool)), playlist(),
            SLOT(psl_show_small_playlist_items(bool)));
    connect(ui->action_Fullscreen, SIGNAL(toggled(bool)), this,SLOT(show_fullscreen_toggled(bool)));
    connect(ui->action_Preferences, SIGNAL(triggered(bool)), this, SIGNAL(sig_preferences()));

    // playlist
    connect(ui->actionShow_numbers, SIGNAL(toggled(bool)),
            playlist(), SLOT(btn_numbers_changed(bool)));
    connect(ui->actionRepeat_All, SIGNAL(triggered()),
            playlist(), SLOT(playlist_mode_changed_slot()));
    connect(ui->actionShuffle, SIGNAL(triggered()), playlist(), SLOT(playlist_mode_changed_slot()));
    connect(ui->actionAppend, SIGNAL(triggered()), playlist(), SLOT(playlist_mode_changed_slot()));
    connect(ui->actionReplace, SIGNAL(triggered()), playlist(), SLOT(playlist_mode_changed_slot()));
    connect(ui->actionAutoplay, SIGNAL(triggered()),
            playlist(), SLOT(playlist_mode_changed_slot()));
    connect(ui->actionClear, SIGNAL(triggered()), playlist(), SIGNAL(clear_playlist()));

    // about
    connect(ui->action_about, SIGNAL(triggered(bool)), this, SLOT(about(bool)));
    connect(ui->action_help, SIGNAL(triggered(bool)), this, SLOT(help(bool)));


    // Keyboard shortcuts
    QShortcut *sc = new QShortcut(QKeySequence("Ctrl+Shift+d"), this);
    connect(sc, SIGNAL (activated()), this, SIGNAL(sig_dumpDescription()));
    sc = new QShortcut(QKeySequence("Ctrl+Tab"), this);
    connect(sc, SIGNAL (activated()), this, SLOT(onFocusNext()));

    QList<QKeySequence> lst;

    lst << QKeySequence(Qt::Key_MediaTogglePlayPause) << QKeySequence(Qt::Key_Space) <<
        QKeySequence(Qt::Key_MediaPlay) << QKeySequence(Qt::Key_MediaPause);
    m_play_pause_action = createAction(lst);
    lst.clear();
    connect(m_play_pause_action, SIGNAL(triggered()), player()->playctl(), SLOT(onPlayClicked()));
    
    lst << QKeySequence(Qt::Key_MediaNext) << QKeySequence(Qt::ControlModifier | Qt::Key_Right);
    QAction* fwd_action = createAction(lst);
    lst.clear();
    connect(fwd_action, SIGNAL(triggered()), player()->playctl(), SLOT(onForwardClicked()));
        
    lst << QKeySequence(Qt::Key_MediaPrevious) << QKeySequence(Qt::ControlModifier | Qt::Key_Left);
    QAction* bwd_action = createAction(lst);
    lst.clear();
    connect(bwd_action, SIGNAL(triggered()), player()->playctl(), SLOT(onBackwardClicked()));

    lst << QKeySequence(Qt::Key_MediaStop) << QKeySequence(Qt::ControlModifier | Qt::Key_Space);
    QAction* stop_action = createAction(lst);
    lst.clear();
    connect(stop_action, SIGNAL(triggered()), player()->playctl(), SLOT(onStopClicked()));

    lst << QKeySequence(Qt::Key_VolumeUp) << QKeySequence(Qt::ControlModifier | Qt::Key_Up) <<
        QKeySequence(Qt::Key_Plus);
    QAction* louder_action = createAction(lst);
    lst.clear();
    connect(louder_action, SIGNAL(triggered()), this, SLOT(onVolumeHigherActivated()));

    lst << QKeySequence(Qt::Key_VolumeUp) << QKeySequence(Qt::ControlModifier | Qt::Key_Down) <<
        QKeySequence(Qt::Key_Minus);
    QAction* leiser_action = createAction(lst);
    lst.clear();
    connect(leiser_action, SIGNAL(triggered()), this, SLOT(onVolumeLowerActivated()));
    
    QAction* two_perc_plus_action = createAction(QKeySequence(Qt::Key_Right));
    connect(two_perc_plus_action, SIGNAL(triggered()), this, SLOT(onJumpForwardActivated()));

    QAction* two_perc_minus_action = createAction( QKeySequence(Qt::Key_Left));
    connect(two_perc_minus_action, SIGNAL(triggered()), this, SLOT(onJumpBackwardActivated()));
}
