/* Copyright (C) 2012  Lucio Carreras
 *
 * Copyright (C) 2022  J.F. Dockes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainw.h"

#include <QMessageBox>
#include <QAbstractButton>

#include <libupnpp/upnpplib.hxx>

#include "HelperStructs/CSettingsStorage.h"
#include "dirbrowser/dirbrowser.h"
#include "GUI/playlist/GUI_Playlist.h"
#include "ui_mainw.h"

void GUI_Player::showLibrary(bool b, bool resize)
{
    m_settings->setNoShowLibrary(!b);
    int old_width = width();
    int lib_width = library()->width();
    int new_width = old_width;
    library()->setVisible(b);

    if (!b) {
        // invisble
        QSizePolicy p = library()->sizePolicy();
        m_library_stretch_factor = p.horizontalStretch();

        p.setHorizontalStretch(0);
        library()->setSizePolicy(p);
        m_library_width = lib_width;
        new_width = old_width - lib_width;
    } else {
        // visible
        QSizePolicy p = library()->sizePolicy();
        p.setHorizontalStretch(m_library_stretch_factor);
        library()->setSizePolicy(p);
        new_width = old_width + m_library_width;
    }

    if (resize) {
        QRect rect = geometry();
        rect.setWidth(new_width);
        setGeometry(rect);
    }
}

void GUI_Player::showPlaylist(bool b, bool resize)
{
    int old_height = height();
    int pl_height = playlist()->height();
    int new_height = old_height;
    playlist()->setVisible(b);

    if (!b) {
        // invisble
        QSizePolicy p = playlist()->sizePolicy();
        m_playlist_stretch_factor = p.verticalStretch();
        p.setVerticalStretch(0);
        playlist()->setSizePolicy(p);
        m_playlist_height = pl_height;
        new_height = old_height - pl_height;
    } else {
        // visible
        QSizePolicy p = playlist()->sizePolicy();
        p.setVerticalStretch(m_playlist_stretch_factor);
        playlist()->setSizePolicy(p);
        new_height = old_height + m_playlist_height;
    }
    if (resize) {
        QRect rect = geometry();
        rect.setHeight(new_height);
        setGeometry(rect);
    }
}

void GUI_Player::show_fullscreen_toggled(bool b)
{
    // may happend because of F11 too
    ui->action_Fullscreen->setChecked(b);
    if (b) {
        showFullScreen();
    } else {
        showNormal();
    }

    m_settings->setPlayerFullscreen(b);
}


void GUI_Player::help(bool)
{
    QString link = Helper::createLink("http://www.lesbonscomptes.com/upplay");

    QMessageBox::information(this, tr("Help"), tr("See links on") + "<br />" 
                             + link);
}

// private slot
void GUI_Player::about(bool b)
{
    Q_UNUSED(b);

    QString version = m_settings->getVersion();
    QString link = Helper::createLink("http://www.lesbonscomptes.com/upplay");

    QMessageBox infobox(this);
    infobox.setParent(this);
    QPixmap p =  QPixmap(Helper::getIconPath("logo.png")).
        scaled(150, 150, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    infobox.setIconPixmap(p);
    infobox.setWindowFlags(Qt::Dialog);
    infobox.setModal(true);

    infobox.setWindowTitle(tr("About Upplay"));
    infobox.setText("<b><font size=\"+2\">Upplay Player " + 
                    version + "</font></b>");
    infobox.setInformativeText(
        QString("") +
        UPnPP::LibUPnP::versionString().c_str() + "<br/>" +
        tr("Based on Sayonara, written by Lucio Carreras") + "<br/>" +
        tr("License") + ": GPL<br/>" +
        "Copyright " + UPPLAY_COPYRDATES + "<br/><br/>" + link + "<br/><br/>"
        );
    infobox.setStandardButtons(QMessageBox::Ok);
    infobox.button(QMessageBox::Ok)->setFocusPolicy(Qt::NoFocus);
    infobox.exec();
}
