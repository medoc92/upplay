/* GUI_Player.cpp */

/* Copyright (C) 2012  Lucio Carreras
 *
 * Copyright (C) 2022 J.F. Dockes
 *
 * This file was part of sayonara player. Now part of Upplay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainw.h"

#include <math.h>

#include <QMessageBox>
#include <QUrl>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkAccessManager>
#include <QPushButton>
#include <QScreen>
#include <QTemporaryFile>
#include <QTimer>

#include "libupnpp/log.hxx"

#include "trayicon.h"
#include "upadapt/upputils.h"
#include "GUI/playlist/GUI_Playlist.h"
#include "GUI/progresswidget/progresswidget.h"
#include "GUI/mdatawidget/mdatawidget.h"
#include "GUI/volumewidget/volumewidget.h"
#include "GUI/playctlwidget/playctlwidget.h"
#include "GUI/playerwidget/playerwidget.h"
#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Style.h"
#include "HelperStructs/globals.h"
#include "application.h"
#include "utils/smallut.h"
#ifndef _WIN32
#include <unistd.h>
#endif

#include "ui_mainw.h"

GUI_Player* obj_ref = 0;

GUI_Player::GUI_Player(Application *upapp, Layout lo, QWidget *parent)
    : QMainWindow(parent), m_upapp(upapp), ui(new Ui::Upplay), m_layout(lo)
{
    m_settings = CSettingsStorage::getInstance();
    QSettings settings;

    ui->setupUi(this);

    bool tabbed = settings.value("tabbedui", false).toBool();
    if (tabbed) {
        m_layout = GUIP_TABBED;
    }
    switch (m_layout) {
    case GUIP_CLASSIC:
        classic_ui(); break;
    case GUIP_TABBED:
        tabbed_ui(); break;
    case GUIP_EXP:
        PlayerWidget::setHorizontal(true);
        horizontal_ui();
        break;
    }

    ui->action_Fullscreen->setShortcut(QKeySequence("F11"));
    playlist()->setActions(ui->actionRepeat_All, ui->actionShuffle, ui->actionAppend,
                           ui->actionReplace, ui->actionAutoplay, ui->actionShow_numbers);
    playlist()->setMode(m_settings->getPlaylistMode());


    m_netmanager = new QNetworkAccessManager(this);
    connect(m_netmanager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(sl_cover_fetch_done(QNetworkReply*)));

    bool showSmallPlaylistItems = m_settings->getShowSmallPlaylist();
    ui->action_smallPlaylistItems->setChecked(showSmallPlaylistItems);

    this->setupTrayActions();
    if (m_trayIcon) {
        m_overridemin2tray = false;
    } else {
        m_overridemin2tray = true;
    }

    this->setupConnections();

    setWindowIcon(QIcon(Helper::getIconPath("logo.png")));

    restoreGeometry(settings.value("maingeom").toByteArray());
    onPrefsChanged();
    idleDisplay();
}


bool GUI_Player::eventFilter(QObject *obj, QEvent *ev)
{
    //std::cerr << "EVT: " << eventName(ev->type()) << "\n";
    if (idleTimer) {
        switch (ev->type()) {
//        case QEvent::HoverMove: /*129 Dups MouseMove now that we filter the app?*/
        case QEvent::KeyPress: /*6*/
        case QEvent::MouseMove: /*5*/
        case QEvent::MouseButtonPress: /*2*/
        case QEvent::NativeGesture:
        case QEvent::Gesture:
        case QEvent::Resize:
            idleTimer->start();
            break;
        default:
            break;
        }
    }
    return QMainWindow::eventFilter(obj, ev);
}

void GUI_Player::onIdle()
{
    if (tabWidget) {
        tabWidget->setCurrentIndex(0);
    }
}

#include "ui_maintabw.h"
void GUI_Player::tabbed_ui()
{
    auto lui = new Ui::TabUi();
    lui->setupUi(ui->centralwidget);
    tabWidget = lui->tabWidget;
    player_wp = &lui->player;
    playlist_wp = &lui->playlist;
    library_wp = &lui->library;

    buttonTopMenu = new QMenu();
    buttonTopMenu->addAction(ui->menuFle->menuAction());
    buttonTopMenu->addAction(ui->menuView->menuAction());
    buttonTopMenu->addAction(ui->menuAbout->menuAction());
    menuPB = new QPushButton(this);
    menuPB->setObjectName(QStringLiteral("menuPB"));
    menuPB->setEnabled(true);
    QIcon icon;
    icon.addFile(QString::fromUtf8(":/icons/menu.png"), QSize(), QIcon::Normal, QIcon::On);
    menuPB->setIcon(icon);
    menuPB->setToolTip(tr("Main menu"));
    menuPB->setMenu(buttonTopMenu);
    tabWidget->setCornerWidget(menuPB, Qt::TopRightCorner);
    tabWidget->setElideMode(Qt::ElideRight);
    library()->setTabsWidget(tabWidget, 2);
    ui->action_viewLibrary->setDisabled(true);
    ui->action_viewPlaylist->setDisabled(true);
    ui->menubar->hide();
    setupIdleTimer();
}

void GUI_Player::setupIdleTimer()
{
    if (!tabWidget)
        return;
    QSettings settings;
    int idleTimerSecs = settings.value("tabidletimer", 0).toInt();
    if (idleTimerSecs > 0) {
        if (nullptr == idleTimer) {
            idleTimer = new QTimer(this);
        }
        connect(idleTimer, SIGNAL(timeout()), this, SLOT(onIdle()));
        idleTimer->start(1000 * idleTimerSecs);
        qApp->removeEventFilter(this);
        qApp->installEventFilter(this);
    } else {
        if (nullptr != idleTimer) {
            delete idleTimer;
            idleTimer = nullptr;
        }
        qApp->removeEventFilter(this);
    }        
}

#include "ui_mainclassicw.h"
void GUI_Player::classic_ui()
{
    auto lui = new Ui::ClassicUi();
    lui->setupUi(ui->centralwidget);
    player_wp = &lui->player;
    playlist_wp = &lui->playlist;
    library_wp = &lui->library;
    splitter = lui->splitter;
    splitter->restoreState(m_settings->getSplitterState());
    verticalLayout = lui->verticalLayout;
    ui->action_viewPlaylist->setDisabled(false);
    ui->action_viewLibrary->setDisabled(false);
    ui->action_viewLibrary->setText(tr("&Library"));
    bool show_library = !m_settings->getNoShowLibrary();
    ui->action_viewLibrary->setChecked(show_library);
    library()->setTabsWidget();
    library()->takeFocus();
    showLibrary(show_library);
    showPlaylist(true);
}

#include "ui_mainhw.h"
void GUI_Player::horizontal_ui()
{
    auto lui = new Ui::HorizUi();
    lui->setupUi(ui->centralwidget);
    player_wp = &lui->player;
    playlist_wp = &lui->playlist;
    library_wp = &lui->library;
    verticalLayout = lui->verticalLayout;
    splitter = lui->splitter;
    splitter->restoreState(m_settings->getSplitterState());
    coverWidget = lui->coverWidget;
    library()->setTabsWidget();
}

GUI_Player::~GUI_Player()
{
    delete m_covertempfile;
}


void GUI_Player::onFocusNext()
{
    auto p = playlist();
    auto l = library();
    if (!p || !l)
        return;
    switch (m_curfocus) {
    case FOCUS_NONE:
    case FOCUS_PLAYLIST:
        l->takeFocus();
        m_curfocus = FOCUS_LIBRARY;
        break;
    case FOCUS_LIBRARY:
        p->takeFocus();
        m_curfocus = FOCUS_PLAYLIST;
        break;
    }
}

// This is called from the playlist when new track info is known
// (possibly up from the audio player)
void GUI_Player::update_track(const MetaData& md)
{
    LOGDEB("GUI_Player::update_track: albumart: " << qs2utf8s(md.albumArtURI) << "\n");
    if (!m_metadata.compare(md)) {
        LOGDEB("GUI_Player::update_track(): same track\n");
        return;
    }
    m_metadata = md;

    total_time_changed(md.length_ms);

    player()->mdata()->setData(md);

    QString titlepart, rendererpart;
    if (!md.title.isEmpty()) {
        titlepart = QString(" - ") + md.title;
    }
    if (!m_renderer_friendlyname.isEmpty()) {
        rendererpart = QString(" (") + m_renderer_friendlyname + ")";
    }
    this->setWindowTitle(QString("Upplay") + rendererpart + titlepart);

    if (!md.albumArtURI.isEmpty()) {
        // if there are several art uris, libupnppp returns them as a
        // list with ", " separators.
        std::vector<std::string> v;
        stringSplitString(qs2utf8s(md.albumArtURI), v, ", ");
        if (!v.empty()) {
            fetch_cover(u8s2qs(v[0]));
        }
    } else {
        LOGDEB("Metadata has no art URI\n");
        m_currentCoverReply = 0;
        sl_no_cover_available();
    }

    player()->progress()->setEnabled(true);
    if (m_trayIcon) {
        m_trayIcon->set_enable_play(true);
        m_trayIcon->set_enable_fwd(true);
        m_trayIcon->set_enable_bwd(true);
    }
    player()->progress()->setEnabled(true);

    m_metadata_available = true;
}

void GUI_Player::onNotify(const MetaData& md)
{
    if (m_trayIcon)
        m_trayIcon->songChangedMessage(md);
}

void GUI_Player::fetch_cover(const QString& uri)
{
    LOGDEB("GUI_Player::fetch_cover: " << qs2utf8s(uri) << "\n");
    if (!m_netmanager) {
        m_currentCoverReply = 0;
        return;
    }
    m_currentCoverReply = m_netmanager->get(QNetworkRequest(QUrl(uri)));
}

// Setting style when starting up or prefs changed
void GUI_Player::onPrefsChanged()
{
    setStyle();
    setupIdleTimer();
}

// Light weight light/dark switching on macOS --could likely be used in general when the light/dark
// mode preference is set to "System"
#if (defined(Q_OS_MACOS) && (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)))
void GUI_Player::onColorsChanged(Qt::ColorScheme color_scheme)
{
    bool dark = false;
    if (color_scheme == Qt::ColorScheme::Light) {
        QIcon::setThemeName("light");
        // the light and dark css and qss could also go into a "light" directory as the dark ones
        Helper::setStyleSubDir("");
    } else if (color_scheme == Qt::ColorScheme::Dark) {
        QIcon::setThemeName("dark");
        Helper::setStyleSubDir("dark");
        dark = true;
    }

    // The only thing that has to be done specifically on macOS, likely also in other cases of
    // "System" light/dark
    playlist()->setupButtonIcons(dark); 

    emit sig_skin_changed(dark);
}
#endif

void GUI_Player::hideDirSearch()
{
    ui->action_viewSearchPanel->setChecked(false);
}
void GUI_Player::showDirSearch()
{
    ui->action_viewSearchPanel->setChecked(true);
}

void GUI_Player::setStyle()
{
    auto csettings = CSettingsStorage::getInstance();
    ui->action_Fullscreen->setChecked(csettings->getPlayerFullscreen());

    auto dark = Style::use_dark_colors();
    LOGDEB("GUI_Player::setStyle: dark: " << dark << "\n");
    if (dark) {
        Helper::setStyleSubDir("dark");
        QIcon::setThemeName("dark");
    } else {
        Helper::setStyleSubDir("");
        QIcon::setThemeName("light");
    }

    float scale = Style::get_ui_scale();

    QString qss = Style::get_qstyle(scale);
    qApp->setStyleSheet(qss);
    if (m_trayIcon)
        m_trayIcon->change_skin(qss);

    auto horizontalDPI = QApplication::primaryScreen()->logicalDotsPerInchX();
    LOGDEB("GUI_Player::setStyle: ui scale: " << scale << " hDPI " << horizontalDPI << "\n");

    float multiplier = (scale * horizontalDPI) / 96.0;

    setCoverSize(round(110 * multiplier));
    player()->mdata()->scale(multiplier);
    player()->volume()->updateSkin();

    int buttonsz = 20;
    player()->playctl()->setupButtons(buttonsz * scale);
    if (m_layout == GUIP_TABBED) {
        buttonsz = 14;
    }
    playlist()->setupButtons(buttonsz * scale);
#if (defined(Q_OS_MACOS) && (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0)))
    playlist()->setupButtonIcons(dark);
#endif
    emit sig_skin_changed(dark);
}

void GUI_Player::psl_openhome_renderer(bool onoff, const QString& friendlyname)
{
    enableSourceSelect(onoff);
    m_renderer_friendlyname = friendlyname;
    if (!m_renderer_friendlyname.isEmpty()) {
        this->setWindowTitle(QString("Upplay (") + m_renderer_friendlyname + ")");
    } else {
        this->setWindowTitle(QString("Upplay"));
    }        
    if (playlist()) {
        playlist()->psl_openhome_renderer(onoff);
    }
}

void GUI_Player::enableSourceSelect(bool enb)
{
    ui->actionSelect_OH_Source->setEnabled(enb);
}


void GUI_Player::setupTrayActions()
{
    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        m_trayIcon = nullptr;
        return;
    }
    m_trayIcon = new GUI_TrayIcon(this);

    connect(m_trayIcon, SIGNAL(sig_stop_clicked()), this, SLOT(onStopActivated()));
    connect(m_trayIcon, SIGNAL(sig_bwd_clicked()), this, SLOT(onBackwardActivated()));
    connect(m_trayIcon, SIGNAL(sig_fwd_clicked()), this, SLOT(onForwardActivated()));
    connect(m_trayIcon, SIGNAL(sig_play_clicked()), this, SLOT(onPlayActivated()));
    connect(m_trayIcon, SIGNAL(sig_pause_clicked()), this, SLOT(onPauseActivated()));
    connect(m_trayIcon, SIGNAL(sig_mute_clicked()), player()->volume(), SLOT(toggleMute()));
    connect(m_trayIcon, SIGNAL(sig_close_clicked()), this, SLOT(really_close()));
    connect(m_trayIcon, SIGNAL(sig_show_clicked()), this, SLOT(showNormal()));
    connect(m_trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(trayItemActivated(QSystemTrayIcon::ActivationReason)));
    connect(m_trayIcon, SIGNAL(sig_volume_changed_by_wheel(int)),
            this, SLOT(onVolumeStepActivated(int)));

    m_trayIcon->setPlaying(false);

    m_trayIcon->show();
}

void GUI_Player::trayItemActivated(QSystemTrayIcon::ActivationReason reason)
{
    QSettings settings;

    switch (reason) {

    case QSystemTrayIcon::Trigger:

        if (this->isMinimized() || isHidden()) {
            this->setHidden(false);
            this->showNormal();
            this->activateWindow();
        } else if (settings.value("min2tray").toBool()) {
            this->setHidden(true);
        } else {
            this->showMinimized();
        }

        break;
    case QSystemTrayIcon::MiddleClick:
        if (m_trayIcon)        
            m_trayIcon->songChangedMessage(m_metadata);
        break;
    default:
        break;
    }
}


void GUI_Player::stopped()
{
    //qDebug() << "void GUI_Player::stopped()";
    player()->playctl()->onStopped();
    m_metadata_available = false;
    m_metadata = MetaData();
    idleDisplay();
}

void GUI_Player::playing()
{
    //qDebug() << "void GUI_Player::playing()";
    player()->playctl()->onPlaying();
    if (m_trayIcon)
        m_trayIcon->setPlaying(true);
}

void GUI_Player::paused()
{
    //qDebug() << "void GUI_Player::paused()";
    player()->playctl()->onPaused();
    if (m_trayIcon)
        m_trayIcon->setPlaying(false);
}

void GUI_Player::disablePlayControl(bool disable)
{
    if (disable) {
        m_play_pause_action->setEnabled(false);
    } else {
        m_play_pause_action->setEnabled(true);
    }
}

void GUI_Player::closeEvent(QCloseEvent* e)
{
    if (splitter)
        m_settings->setSplitterState(splitter->saveState());
    QSettings settings;
    if (!isMaximized()) {
        // Don't save geometry if we're currently maximized. At least under X11
        // this saves the maximized size. otoh isFullscreen() does not seem needed
        LOGINF("GUI_Player: exit geometry: w " << width() << " h " << height() << "\n");
        settings.setValue("maingeom", saveGeometry());
    }

    if (!m_overridemin2tray && settings.value("min2tray").toBool()) {
        e->ignore();
        this->hide();
        return;
    } else {
        e->accept();
    }

    // It seems that we still have top level widgets at this point
    // (e.g. the menus?). Checkable with
    // QApplication::topLevelWidgets() The app is going to exit
    // anyway, except apparently when running some KDE desktop
    // versions ?? So call exit directly if we see KDE. I don't know
    // that calling exit in all cases would be an issue actually.
    char *cp = getenv("XDG_CURRENT_DESKTOP");
    if (nullptr == cp) {
        cp = getenv("XDG_SESSION_DESKTOP");
    }
    // We used to use qapp aboutToQuit() signal, but we have trouble
    // with the quitting sometimes segfaulting for some reason, so use
    // our own signal to tell, e.g. cdbrowser to save its state.
    emit sig_quitting();

    if (cp && !strcmp(cp, "KDE")) {
        qApp->exit();
    }

#ifndef _WIN32
    // Returning from closeEvent() dumps core when running under some
    // Window Managers (e.g. i3). Also exit() results in strange
    // things happening. So just get out !
    _exit(0);
#endif
}

void GUI_Player::really_close(bool)
{
    m_overridemin2tray = true;
    this->close();
}
