/* Copyright (C) 2022 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef _FILEDIALOGS_H_INCLUDED_
#define _FILEDIALOGS_H_INCLUDED_

/* Local versions of some filedialog functions, checking that the dialogs don't get bigger than the
 * screen size. On a small touchscreen, if a dialog opens too big so that the buttons are
 * off-screen, there is nothing the user can do. 
 */

#include <QWidget>
#include <QString>
#include <QFileDialog>
#include <QUrl>

namespace MedocUtils {

class FileDialog : public QFileDialog {
    Q_OBJECT;
public:
    static QString getSaveFileName(
        QWidget *parent = nullptr, const QString &caption = QString(),
        const QString &dir = QString(), const QString &filter = QString(),
        QString *selectedFilter = nullptr, QFileDialog::Options options = Options());

    static QUrl getSaveFileUrl(
        QWidget *parent = nullptr, const QString &caption = QString(), const QUrl &dir = QUrl(),
        const QString &filter = QString(), QString *selectedFilter = nullptr,
        Options options = Options(), const QStringList &supportedSchemes = QStringList());

    static QString getOpenFileName(
        QWidget *parent = nullptr, const QString &caption = QString(),
        const QString &dir = QString(), const QString &filter = QString(),
        QString *selectedFilter = nullptr, QFileDialog::Options options = Options());

    static QUrl getOpenFileUrl(
        QWidget *parent = nullptr, const QString &caption = QString(), const QUrl &dir = QUrl(),
        const QString &filter = QString(), QString *selectedFilter = nullptr,
        Options options = Options(), const QStringList &supportedSchemes = QStringList());
};


} // End namespace MedocUtils

#endif /* _FILEDIALOGS_H_INCLUDED_ */
