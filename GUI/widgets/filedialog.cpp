/* Copyright (C) 2022 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "filedialog.h"

#include <QFileDialog>
#include <QScreen>
#include <QStringList>
#include <QWindow>

namespace MedocUtils {


QString FileDialog::getSaveFileName(
        QWidget *parent, const QString &caption, const QString &dir, const QString &filter,
        QString *selectedFilter, QFileDialog::Options options)
{
    const QStringList schemes = QStringList(QStringLiteral("file"));
    const QUrl selectedUrl = getSaveFileUrl(parent, caption, QUrl::fromLocalFile(dir), filter,
                                            selectedFilter, options, schemes);
    return selectedUrl.toLocalFile();
}

QUrl FileDialog::getSaveFileUrl(
    QWidget *parent, const QString &caption, const QUrl &dir, const QString &filter,
    QString *selectedFilter, Options options, const QStringList &supportedSchemes)
{
    QFileDialog dialog(parent, caption, dir.toLocalFile(), filter);
    if (parent) {
        QScreen *screen = parent->window()->windowHandle()->screen();    
        dialog.setMaximumSize(screen->availableSize());
    }
    dialog.setSupportedSchemes(supportedSchemes);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(AcceptSave);
    dialog.setOptions(options);
    if (selectedFilter && !selectedFilter->isEmpty())
        dialog.selectNameFilter(*selectedFilter);
    if (dialog.exec() == QDialog::Accepted) {
        if (selectedFilter)
            *selectedFilter = dialog.selectedNameFilter();
        return dialog.selectedUrls().value(0);
    }
    return QUrl();
}

QString FileDialog::getOpenFileName(
        QWidget *parent, const QString &caption, const QString &dir, const QString &filter,
        QString *selectedFilter, QFileDialog::Options options)
{
    const QStringList schemes = QStringList(QStringLiteral("file"));
    const QUrl selectedUrl = getOpenFileUrl(parent, caption, QUrl::fromLocalFile(dir), filter,
                                            selectedFilter, options, schemes);
    return selectedUrl.toLocalFile();
}


QUrl FileDialog::getOpenFileUrl(
    QWidget *parent, const QString &caption, const QUrl &dir, const QString &filter,
    QString *selectedFilter, Options options, const QStringList &supportedSchemes)
{
    QFileDialog dialog(parent, caption, dir.toLocalFile(), filter);
    if (parent) {
        QScreen *screen = parent->window()->windowHandle()->screen();    
        dialog.setMaximumSize(screen->availableSize());
    }
    dialog.setSupportedSchemes(supportedSchemes);
    if (selectedFilter && !selectedFilter->isEmpty())
        dialog.selectNameFilter(*selectedFilter);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setOptions(options);
    if (dialog.exec() == QDialog::Accepted) {
        if (selectedFilter)
            *selectedFilter = dialog.selectedNameFilter();
        return dialog.selectedUrls().value(0);
    }
    return QUrl();
}

} // End namespace MedocUtils
