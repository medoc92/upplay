/* Copyright (C) 2015 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "playctlwidget.h"
#include "HelperStructs/Helper.h"
#include "ui_playctlwidget.h"

PlayCtlWidget::PlayCtlWidget(QWidget *parent)
    : QWidget(parent), m_playing(false)
{
    ui = new Ui::PlayCtlWidget();
    ui->setupUi(this);
    setupButtons();
    connect(ui->btn_bw, SIGNAL(clicked()), this, SLOT(onBackwardClicked()));
    connect(ui->btn_fw, SIGNAL(clicked()), this, SLOT(onForwardClicked()));
    connect(ui->btn_play, SIGNAL(clicked()), this, SLOT(onPlayClicked()));
    connect(ui->btn_stop, SIGNAL(clicked()), this, SLOT(onStopClicked()));
}

struct ButPath {
    QPushButton *bt;
    const char *path;
    int szincr;
};

void PlayCtlWidget::setupButtons(int _sz)
{
    std::vector<ButPath> plbuttons {
        {ui->btn_stop, "stop.png", 2},
        {ui->btn_bw, "bwd.png", 0},
        {ui->btn_fw, "fwd.png", 0},
        {ui->btn_play, "play.png", 8},
    };
    int pad = 4;
    for (auto& butpath : plbuttons) {
        butpath.bt->setIcon(QIcon(Helper::getIconPath(butpath.path)));
        if (_sz) {
            int sz = _sz + butpath.szincr;
            butpath.bt->setFixedSize(QSize(sz + pad, sz + pad));
            butpath.bt->setIconSize(QSize(sz, sz));
        }
    }
}

bool PlayCtlWidget::playing()
{
    return m_playing;
}

QLayoutItem *PlayCtlWidget::takeStopWidget()
{
    return ui->horizontalLayout->takeAt(1);
}

void PlayCtlWidget::onStopped()
{
    ui->btn_play->setIcon(QIcon(Helper::getIconPath("play.png")));
    m_playing = false;
}

void PlayCtlWidget::onPaused()
{
    ui->btn_play->setIcon(QIcon(Helper::getIconPath("play.png")));
    m_playing = false;
}

void PlayCtlWidget::onPlaying()
{
    ui->btn_play->setIcon(QIcon(Helper::getIconPath("pause.png")));
    m_playing = true;
}

void PlayCtlWidget::onPlayClicked()
{
   if (m_playing) {
       emit pauserequested();
   } else {
       emit playrequested();
   }
}

void PlayCtlWidget::onStopClicked()
{
    emit stoprequested();
}

void PlayCtlWidget::onBackwardClicked()
{
    emit backwardrequested();
}

void PlayCtlWidget::onForwardClicked()
{
    emit forwardrequested();
}
