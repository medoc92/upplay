/* Copyright (C) 2015 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "playerwidget.h"
#include "ui_playervwidget.h"
#include "ui_playerhwidget.h"

bool PlayerWidget::horizontal{false};
void PlayerWidget::setHorizontal(bool onoff)
{
    horizontal = onoff;
}

PlayerWidget::PlayerWidget(QWidget *parent)
    : QWidget(parent)
{
    if (horizontal) {
        hui = new Ui::PlayerHWidget();
        hui->setupUi(this);
    } else {
        vui = new Ui::PlayerVWidget();
        vui->setupUi(this);
    }
    // Reproduce the old setup by moving the stop button around
    QLayoutItem *item = playctl()->takeStopWidget();
    // Curiously this does not work
    //bottomHLayout->addItem(item);
    // But this does
    if (vui) {
        vui->bottomHLayout->addWidget(item->widget(), Qt::AlignTop);
        vui->bottomHLayout->setAlignment(item->widget(), Qt::AlignTop);
    }
    // I guess that this should be done, but I also suspect that
    // I'm seeing related memory issues
    //      item->invalidate(); delete item;
}

VolumeWidgetIF* PlayerWidget::volume() {
    return vui ? vui->volumectl_w : hui->volumectl_w;
}
PlayCtlWidget* PlayerWidget::playctl() {
    return vui ? vui->playctl_w : hui->playctl_w ;
}
ProgressWidget* PlayerWidget::progress() {
    return vui ? vui->progress_w : hui->progress_w;
}
MDataWidget* PlayerWidget::mdata() {
    return vui ? vui->mdata_w : hui->mdata_w;
}
QLayoutItem *PlayerWidget::takeCoverWidget() {
    return vui ? vui->horizontalLayout->takeAt(0) : hui->bottomHLayout->takeAt(0);
}
QPushButton *PlayerWidget::albumCover() {
    return vui ? vui->albumCover : hui->albumCover;
}
