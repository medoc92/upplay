TEMPLATE        = app
TARGET          = upplay

# VERSION is ^egrepped and must stay in the first column
VERSION = 1.8.1
COPYRDATES = 2011-2024

# Possibly override WEBPLATFORM from the command line, or environment
WEBPLATFORM = $${WEBPLATFORM}
isEmpty(WEBPLATFORM){
WEBPLATFORM = $$(WEBPLATFORM)
}
# Else chose it according to the system
isEmpty(WEBPLATFORM){
  greaterThan(QT_MAJOR_VERSION, 5) {
    WEBPLATFORM = webengine
  } else {
    WEBPLATFORM = webkit
  }
  windows {
    WEBPLATFORM = webengine
  }
}


contains(WEBPLATFORM, webengine) {
    QT += widgets webenginewidgets webchannel
    QMAKE_CXXFLAGS += -DUSING_WEBENGINE
    DEFINES += USING_WEBENGINE
} else {
    QT += webkit
    QT += widgets webkitwidgets
}

QMAKE_CXXFLAGS += -DUPPLAY_VERSION=\\\"$$VERSION\\\" 
QMAKE_CXXFLAGS += -DUPPLAY_COPYRDATES=\\\"$$COPYRDATES\\\"
INCLUDEPATH += $$PWD/utils

CONFIG  += c++17 thread

# DEFINES += UPPLAY_HORIZONTAL_LAYOUT

# This is only used for the icons which are referenced from the style sheets.
# Did not find any other reasonably simple way to get rid of it.
RESOURCES += GUI/upplay.qrc

HEADERS += \
        GUI/bareurl/bareurl.h \
        GUI/mainw/mainw.h \
        GUI/mainw/trayicon.h \
        GUI/mdatawidget/mdatawidget.h \
        GUI/mdatawidget/mdatawidgetif.h \
        GUI/playctlwidget/playctlwidget.h \
        GUI/playerwidget/playerwidget.h \
        GUI/playlist/GUI_Playlist.h \
        GUI/playlist/delegate/PlaylistItemDelegate.h \
        GUI/playlist/entry/GUI_PlaylistEntry.h \
        GUI/playlist/entry/GUI_PlaylistEntryBig.h \
        GUI/playlist/entry/GUI_PlaylistEntrySmall.h \
        GUI/playlist/model/PlaylistItemModel.h \
        GUI/playlist/view/ContextMenu.h \
        GUI/playlist/view/PlaylistView.h \
        GUI/prefs/prefs.h \
        GUI/prefs/sortprefs.h \
        GUI/progresswidget/progresswidget.h \
        GUI/progresswidget/progresswidgetif.h \
        GUI/renderchoose/renderchoose.h \
        GUI/songcast/songcastdlg.h \
        GUI/sourcechoose/sourcechoose.h \
        GUI/volumewidget/soundslider.h  \
        GUI/volumewidget/volumewidget.h \
        GUI/volumewidget/volumewidgetif.h \
        GUI/widgets/browserdialog.h \
        GUI/widgets/directslider.h  \
        GUI/widgets/filedialog.h \
        HelperStructs/CSettingsStorage.h \
        HelperStructs/Helper.h \
        HelperStructs/Style.h \
        application.h \
        application_p.h \
        dirbrowser/cdbrowser.h \
        dirbrowser/dirbrowser.h \
        dirbrowser/randplayer.h \
        dirbrowser/rreaper.h \
        notifications/audioscrobbler.h \
        notifications/notifications.h \
        playlist/playlist.h \
        playlist/playlistavt.h \
        playlist/playlistnull.h \
        playlist/playlistohpl.h \
        playlist/playlistohrcv.h \
        playlist/playlistohrd.h \
        upadapt/avtadapt.h \
        upadapt/ohifadapt.h \
        upadapt/ohpladapt.h \
        upadapt/ohrdadapt.h \
        upadapt/songcast.h \
        upadapt/vtime.h \
        upadapt/vvolume.h \
        upqo/avtransport_qo.h \
        upqo/cdirectory_qo.h \
        upqo/discovery_qo.h \
        upqo/ohinfo_qo.h \
        upqo/ohplaylist_qo.h \
        upqo/ohproduct_qo.h \
        upqo/ohradio_qo.h \
        upqo/ohtime_qo.h \
        upqo/ohvolume_qo.h \
        upqo/renderingcontrol_qo.h \
        utils/confgui.h \
        utils/smallut.h \
        utils/writedescription.h
                        
SOURCES += \
        GUI/mainw/mainw.cpp \
        GUI/mainw/mw_connections.cpp  \
        GUI/mainw/mw_controls.cpp  \
        GUI/mainw/mw_cover.cpp  \
        GUI/mainw/mw_menubar.cpp  \
        GUI/mainw/trayicon.cpp \
        GUI/mdatawidget/mdatawidget.cpp \
        GUI/playctlwidget/playctlwidget.cpp \
        GUI/playerwidget/playerwidget.cpp \
        GUI/playlist/GUI_Playlist.cpp \
        GUI/playlist/delegate/PlaylistItemDelegate.cpp \
        GUI/playlist/entry/GUI_PlaylistEntryBig.cpp \
        GUI/playlist/entry/GUI_PlaylistEntrySmall.cpp \
        GUI/playlist/model/PlaylistItemModel.cpp \
        GUI/playlist/view/ContextMenu.cpp \
        GUI/playlist/view/PlaylistView.cpp \
        GUI/prefs/prefs.cpp \
        GUI/prefs/sortprefs.cpp \
        GUI/progresswidget/progresswidget.cpp \
        GUI/renderchoose/renderchoose.cpp \
        GUI/songcast/songcastdlg.cpp \
        GUI/volumewidget/soundslider.cpp  \
        GUI/volumewidget/volumewidget.cpp \
        GUI/widgets/directslider.cpp \
        GUI/widgets/filedialog.cpp \
        HelperStructs/CSettingsStorage.cpp \
        HelperStructs/Helper.cpp \
        HelperStructs/Style.cpp \
        application.cpp \
        dirbrowser/cdb_html.cpp \
        dirbrowser/cdbrowser.cpp \
        dirbrowser/dirb_json.cpp \
        dirbrowser/dirbrowser.cpp \
        dirbrowser/randplayer.cpp \
        dirbrowser/rreaper.cpp \
        notifications/audioscrobbler.cpp \
        notifications/notifications.cpp \
        playlist/playlist.cpp \
        playlist/playlistavt.cpp \
        playlist/playlistohpl.cpp \
        playlist/playlistohrd.cpp \
        upadapt/rendererlist.cpp \
        upadapt/songcast.cpp \
        upadapt/upputils.cpp \
        upplay.cpp \
        upqo/ohpool.cpp \
        upqo/protolist.cpp \
        utils/confgui.cpp \
        utils/conftree.cpp \
        utils/md5.cpp \
        utils/pathut.cpp \
        utils/smallut.cpp \
        utils/utf8iter.cpp \
        utils/writedescription.cpp

# These are not used for now (local radio station list for avt
# players). Maybe one day.
#        playlist/playlistlocrd.h 
#        playlist/playlistlocrd.cpp 

FORMS   = \
        GUI/bareurl/bareurl.ui \
        GUI/mainw/mainw.ui \
        GUI/mainw/maintabw.ui \
        GUI/mainw/mainclassicw.ui \
        GUI/mainw/mainhw.ui \
        GUI/playctlwidget/playctlwidget.ui \
        GUI/playerwidget/playerhwidget.ui \
        GUI/playerwidget/playervwidget.ui \
        GUI/playlist/GUI_Playlist.ui \
        GUI/playlist/entry/GUI_PlaylistEntryBig.ui \
        GUI/playlist/entry/GUI_PlaylistEntrySmall.ui \
        GUI/prefs/sortprefs.ui \
        GUI/progresswidget/progresswidget.ui \
        GUI/renderchoose/renderchoose.ui \
        GUI/songcast/songcastdlg.ui \
        GUI/sourcechoose/sourcechoose.ui \
        GUI/widgets/browserdialog.ui \
        dirbrowser/dirbrowser.ui

unix:!mac {

  QMAKE_CXXFLAGS += -g

  # Comment these if you qtmpris (https://github.com/sailfishos/qtmpris) is not available, or you
  # don't want to enable the MPRIS interface. Upplay only implements play/pause/next/previous at the
  # moment. Not that, on Debian at least, you need to install both libmpris-qt5-dev and
  # libdbusextended-qt5-dev
  lessThan(QT_MAJOR_VERSION, 6) {
    CONFIG +=  mpris-qt5
    DEFINES += HAVE_QTMPRIS
  }

  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  LIBS += -lupnpp -ljsoncpp 
  isEmpty(PREFIX) {
    PREFIX = /usr
  }
  message("Prefix is $$PREFIX")
  DEFINES += PREFIX=\\\"$$PREFIX\\\"
  DEFINES += _LARGE_FILE_SOURCE
  DEFINES += _FILE_OFFSET_BITS=64
  
  INCLUDEPATH += /usr/include/jsoncpp
  
 # Installation stuff
  target.path = "$$PREFIX/bin"

  bdata.files = dirbrowser/cdbrowser.css dirbrowser/dark.css \
              dirbrowser/standard.css dirbrowser/containerscript.js dirbrowser/qwebchannel.js
  bdata.path = $$PREFIX/share/upplay/cdbrowser   
  gdata.files = GUI/common.qss GUI/standard.qss GUI/dark.qss
  gdata.path = $$PREFIX/share/upplay/
  desktop.files += upplay.desktop
  desktop.path = $$PREFIX/share/applications/
  icona.files = GUI/icons/upplay.png
  icona.path = $$PREFIX/share/icons/hicolor/48x48/apps/
  iconb.files = GUI/icons/upplay.png
  iconb.path = $$PREFIX/share/pixmaps/

  INSTALLS += target bdata desktop gdata icona iconb
}

mac {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj

  QCBUILDLOC=Qt_6_7_3_for_macOS
  QMAKE_APPLE_DEVICE_ARCHS = x86_64 arm64
  QMAKE_CXXFLAGS += -Wno-unused-parameter

  LIBS += ../../../npupnp/qmk/build/$$QCBUILDLOC-Release/libnpupnp.a
  LIBS += ../../../libupnpp/qmk/build/$$QCBUILDLOC-Release/libupnpp.a
  LIBS += ../../../libmicrohttpd-1.0.1/src/microhttpd/.libs/libmicrohttpd.a
  LIBS += -lexpat -lcurl

  INCLUDEPATH += sysdeps/jsoncpp
  INCLUDEPATH += ../libupnpp/
  INCLUDEPATH += ../libupnpp/libupnpp/
  DEFINES += _LARGE_FILE_SOURCE
  DEFINES += _FILE_OFFSET_BITS=64

  SOURCES += sysdeps/jsoncpp/jsoncpp.cpp

  ICON = macos/appIcon.icns
  APP_ICON_FILES.files = \
                         GUI/icons/logo.png \
                         GUI/icons/upplay.ico \
                         GUI/icons/upplay.png
  APP_ICON_FILES.path = Contents/Resources

  APP_CSS_FILES.files = \
                         dirbrowser/cdbrowser.css \
                         dirbrowser/dark.css \
                         dirbrowser/standard.css
  APP_CSS_FILES.path = Contents/Resources

  APP_QSS_FILES.files = \
                         GUI/common.qss \
                         GUI/dark.qss \
                         GUI/standard.qss
  APP_QSS_FILES.path = Contents/Resources

  APP_JS_FILES.files = dirbrowser/containerscript.js dirbrowser/qwebchannel.js
  APP_JS_FILES.path = Contents/Resources

  QMAKE_BUNDLE_DATA += APP_ICON_FILES APP_CSS_FILES APP_QSS_FILES APP_JS_FILES

  QMAKE_INFO_PLIST = macos/Info.plist

  # Installation stuff
  target.path = "/Applications"

  INSTALLS += target  
}

windows {
  # Using static libs: will get the one from npupnp
  SOURCES -= utils/utf8iter.cpp
  SOURCES += sysdeps/jsoncpp/jsoncpp.cpp
  SOURCES += windows/getopt.cc
  INCLUDEPATH += sysdeps/jsoncpp
  INCLUDEPATH += $$PWD/../libupnpp
  INCLUDEPATH += $$PWD/../libupnpp/libupnpp
  INCLUDEPATH += windows
  
  DEFINES += PSAPI_VERSION=1

  contains(QMAKE_CC, cl){
    # MSC
    QCBUILDLOC = Desktop_Qt_6_7_3_MSVC2019_64bit
    LIBS += $$PWD/../libupnpp/qmk/build/$$QCBUILDLOC-Release/release/upnpp.lib
    LIBS += $$PWD/../npupnp/qmk/build/$$QCBUILDLOC-Release/release/npupnp.lib
    LIBS += $$PWD/../expat.v140.2.4.1.1/build/native/lib/x64/Release/libexpat.lib
    LIBS += $$PWD/../curl-7.70.0/builds/libcurl-vc-x64-release-static-ipv6/lib/libcurl_a.lib
    LIBS += $$PWD/../libmicrohttpd-0.9.65-w32-bin/x86_64/VS2019/Release-dll/libmicrohttpd-dll.lib
    # Needed by libcurl in this config. Might be possible to ditch with
    # other curl build options ? First is for international urls second
    # is  hash/crypto lib
    LIBS += -lNormaliz
    LIBS += -lAdvapi32
  }

  contains(QMAKE_CC, gcc){
    # MingW
    QMAKE_CXXFLAGS += -Wno-unused-parameter
    QCBUILDLOC = Desktop_Qt_5_8_0_MinGW_32bit
    LIBS += $$PWD/../libupnpp/windows/build-libupnpp-$$QCBUILDLOC-Debug/debug/libupnpp.a
    LIBS += $$PWD/../npupnp/build-libnpupnp-$$QCBUILDLOC-Debug/debug/libnpupnp.a
    LIBS += $$PWD/../curl-7.70.0/lib/libcurl.a
    LIBS += $$PWD/../libmicrohttpd-0.9.65/src/microhttpd/.libs/libmicrohttpd.a
    LIBS += $$PWD/../expat-2.1.0/.libs/libexpat.a
  }

  LIBS += -liphlpapi
  LIBS += -lwldap32
  LIBS += -lws2_32
  LIBS += -lshlwapi
  LIBS += -lShell32
  LIBS += -luuid
}
