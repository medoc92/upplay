/* 
 * Copyright (C) 2023  J.F. Dockes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APPLICATION_P_H
#define APPLICATION_P_H

#include <QThread>
#include "libupnpp/control/mediarenderer.hxx"

class AVTPlayer;
class OHInfoQA;
class OHPlayer;
class OHProductQO;
class OHRadioQA;
class OHTimeQO;
class Playlist;
class VirtualTime;
class VirtualVolume;
class OHVolumeQO;
class RenderingControlQO;
class RdrThrSourceController;

// The RdrThread object owns all UPnP objects, and deletes them when its event loop exits.  All the
// objects are movedToThread() to its thread, and mostly communicate with the main thread through
// signals/slots (queued), with a few exceptions.
//
// Note that the playlist object belongs to us, but it's running in the main thread. The app takes
// care to remove its reference from the directory browser before telling us to exit() just in case
// this changes.
class RdrThread : public QThread {
    Q_OBJECT;
public:
    void run() override;

    std::shared_ptr<Playlist> m_playlist;
    UPnPClient::MRDH m_rdr;
    AVTPlayer *m_avto{nullptr};
    OHTimeQO *m_ohtmo{nullptr};
    OHInfoQA *m_ohinf{nullptr};
    OHPlayer *m_ohplo{nullptr};
    OHRadioQA *m_ohrdo{nullptr};
    OHProductQO *m_ohpro{nullptr};
    OHVolumeQO *m_ohvlo{nullptr};
    RenderingControlQO *m_rdco{nullptr};
    VirtualVolume *m_vvol{nullptr};
    VirtualTime *m_vtime{nullptr};
    RdrThrSourceController *m_srcctl{nullptr};
};

// We sometimes need to delete objects inside the rdr thread, and this needs to happen within the
// thread to ensure that the main thread is not blocked. This objects runs in the rdr thread and is
// connected through signal slots to the main thread to implement the function.
class RdrThrSourceController : public QObject {
    Q_OBJECT;
public:
    RdrThrSourceController(RdrThread *rdrthr)
        : m_rdrthr(rdrthr) {}
public slots:
    void delrdsource();
    void delplsource();
private:
    RdrThread *m_rdrthr{nullptr};
};

#endif // APPLICATION_P_H
