/* Copyright (C) 2022 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _VTIME_H_INCLUDED_
#define _VTIME_H_INCLUDED_
/* A virtual time in song class over the OH and UPnP/AV time interfaces */

#include <QObject>

#include "upqo/ohtime_qo.h"
#include "avtadapt.h"


class VirtualTime : public QObject {
    Q_OBJECT;
public:
    VirtualTime(AVTPlayer *avto, QObject *parent = nullptr)
        : QObject(parent), m_avto(avto) {
        connect(m_avto, SIGNAL(secsInSongChanged(quint32)),
                this, SIGNAL(secsInSongChanged(quint32)));
    }

    VirtualTime(OHTimeQO *ohtmo, QObject *parent = nullptr)
        : QObject(parent), m_ohtmo(ohtmo) {
        connect(m_ohtmo, SIGNAL(secsInSongChanged(quint32)),
                this, SIGNAL(secsInSongChanged(quint32)));
    }

    ~VirtualTime() {}

    quint32 secsInSong() {
        if (m_ohtmo) {
            UPnPClient::OHTime::Time tm;
            tm.seconds = 0;
            m_ohtmo->time(tm);
            return quint32(tm.seconds);
        } else if (m_avto) {
            return m_avto->secsInSong();
        }
        return 0;
    }
    
signals:
    void secsInSongChanged(quint32);
public slots:
    void bogus(){}
private:
    OHTimeQO *m_ohtmo{nullptr};
    AVTPlayer *m_avto{nullptr};
};

#endif /* _VTIME_H_INCLUDED_ */
