/* Copyright (C) 2022 J.F.Dockes
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the
 *     Free Software Foundation, Inc.,
 *     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef _VVOLUME_H_INCLUDED_
#define _VVOLUME_H_INCLUDED_
/* A virtual volume class over the very similar OH and UPnP/AV volume interfaces */

#include <QObject>

#include "upqo/ohvolume_qo.h"
#include "upqo/renderingcontrol_qo.h"


class VirtualVolume : public QObject {
    Q_OBJECT;
public:
    VirtualVolume(RenderingControlQO *rdco, QObject *parent = nullptr)
        : QObject(parent), m_rdco(rdco) {
        connect(m_rdco, SIGNAL(volumeChanged(int)), this, SIGNAL(volumeChanged(int)));
        connect(m_rdco, SIGNAL(muteChanged(bool)), this, SIGNAL(muteChanged(bool)));
    }

    VirtualVolume(OHVolumeQO *ohvlo, QObject *parent = nullptr)
        : QObject(parent), m_ohvlo(ohvlo) {
        connect(m_ohvlo, SIGNAL(volumeChanged(int)), this, SIGNAL(volumeChanged(int)));
        connect(m_ohvlo, SIGNAL(muteChanged(bool)), this, SIGNAL(muteChanged(bool)));
    }

    int volume() {
        if (m_rdco) {
            return m_rdco->volume();
        } else if (m_ohvlo) {
            return m_ohvlo->volume();
        } else {
            return 0;
        }
    }

    bool mute()  {
        if (m_rdco) {
            return m_rdco->mute();
        } else if (m_ohvlo) {
            return m_ohvlo->mute();
        } else {
            return 0;
        }
    }

public slots:
    void setVolume(int vol) {
        if (m_rdco) {
            return m_rdco->setVolume(vol);
        } else if (m_ohvlo) {
            return m_ohvlo->setVolume(vol);
        }
    }

    void setMute(bool mute) {
        if (m_rdco) {
            return m_rdco->setMute(mute);
        } else if (m_ohvlo) {
            return m_ohvlo->setMute(mute);
        }
    }

signals:
    void volumeChanged(int);
    void muteChanged(bool);
    
private:
    OHVolumeQO   *m_ohvlo{nullptr};
    RenderingControlQO *m_rdco{nullptr};
};

#endif /* _VVOLUME_H_INCLUDED_ */
