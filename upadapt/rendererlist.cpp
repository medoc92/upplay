/* Copyright (C) 2017-2019 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "rendererlist.h"

#include <libupnpp/control/discovery.hxx>

using namespace UPnPClient;

std::vector<UPnPDeviceDesc> rendererList(int flags)
{
    std::vector<UPnPDeviceDesc> devices, filtered_devices;
    if (!MediaRenderer::getDeviceDescs(devices) || devices.empty()) {
        return devices;
    }
    bool ohonly = (flags & int(RSF_OHONLY)) != 0;
    bool avtonly = (flags & int(RSF_AVTONLY)) != 0;
    for (auto& device: devices) {
        if (ohonly || avtonly) {
            bool hasopenhome{false};
            bool hasupnpav{false};
            for (const auto& srv : device.services) {
                if (OHProduct::isOHPrService(srv.serviceType)) {
                    hasopenhome = true;
                }
                if (AVTransport::isAVTService(srv.serviceType)) {
                    hasupnpav = true;
                }
            }
            if (ohonly && !hasopenhome) {
                continue;
            }
            if (avtonly && !hasupnpav) {
                continue;
            }
        }
        filtered_devices.push_back(device);
    }
    return filtered_devices;
}

MRDH getRenderer(const std::string& name, bool isfriendlyname)
{
    auto superdir = UPnPDeviceDirectory::getTheDir(1);
    if (superdir == 0) {
        std::cerr << "Can't create UPnP discovery object" << std::endl;
        exit(1);
    }

    UPnPDeviceDesc ddesc;
    if (isfriendlyname) {
        if (superdir->getDevByFName(name, ddesc)) {
            return std::make_shared<MediaRenderer>(ddesc);
        }
    } else {
        if (superdir->getDevByUDN(name, ddesc)) {
            return std::make_shared<MediaRenderer>(ddesc);
        }
    }
    return MRDH();
}
