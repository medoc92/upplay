/* Copyright (C) 2014 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include <string>
#include <set>
#include <fstream>
#include <vector>

#include <QString>
#include <QSettings>

#include <libupnpp/upnpavutils.hxx>
#include <libupnpp/log.hxx>
#include <libupnpp/control/cdircontent.hxx>

#include "HelperStructs/MetaData.h"
#include "upadapt/upputils.h"

#include "playlist/playlist.h"
#include "utils/smallut.h"

using namespace UPnPClient;
using namespace UPnPP;

// Preference ordering for choosing a format when several are
// possible:
//   DSD because people who bother with this probably want to play it
//   lossless compressed,
//   uncompressed
//   lossy. 
// This should/will  probably get into a config file some day.

static const std::map<std::string, int> formatprefs {
{"video/mp4", 0},

{"audio/dff", 10},
{"audio/x-dff", 10},
{"audio/dsd", 10},
{"audio/x-dsd", 10},
{"audio/dsf", 10},
{"audio/x-dsf", 10},

{"application/flac", 20},
{"application/x-flac", 20},
{"audio/flac", 20},
{"audio/x-wavpack", 20},
{"audio/x-flac", 20},
{"audio/x-ape", 20},
{"audio/ape", 20},
{"audio/x-monkeys-audio", 20},

{"audio/wav", 30},
{"audio/x-wav", 30},
{"audio/wave", 30},
{"audio/x-aiff", 30},
{"audio/aif", 30},
{"audio/aiff", 30},
{"audio/L16", 30},

{"audio/x-vorbis+ogg", 40},
{"audio/x-vorbis", 40},
{"audio/ogg", 40},
{"audio/vorbis", 40},
{"audio/x-ogg", 40},
{"audio/aac", 40},
{"audio/m4a", 40},
{"audio/x-m4a", 40},
{"audio/matroska", 40},
{"audio/x-matroska", 40},
{"audio/mp1", 40},
{"audio/mp4", 40},
{"audio/mpeg", 40},
{"audio/x-mpeg", 40},
{"audio/x-ms-wma", 40},
{"audio/x-scpls", 40},
    };

// Upnp field to qstring
static QString uf2qs(const UPnPDirObject *dop, const char *nm, bool isres)
{
    std::string val;
    bool ret = isres? dop->getrprop(0, nm, val) : dop->getprop(nm, val);
    if (ret) 
        return QString::fromUtf8(val.c_str());
    else
        return QString();
}

static std::string dlna_org_pn(const UPnPP::ProtocolinfoEntry& e)
{
    LOGDEB1("dlna_org_pn: input: proto [" << e.protocol << "] net [ " << e.network <<
           "] cf [" << e.contentFormat << "] add. [" << e.additional << "]\n");
    
    std::string out;
    auto idx = e.additional.find("DLNA.ORG_PN=");
    if (idx == std::string::npos)
        return out;
    idx += 12;
    auto semicol = e.additional.find(";", idx);
    if (semicol != std::string::npos) {
        out = e.additional.substr(idx, semicol-idx);
    } else {
        out = e.additional.substr(idx);
    }
    LOGDEB1("dlna_org_pn: returning [" << trimstring(out) << "]\n");
    return trimstring(out);
}

static int chooseResource(std::shared_ptr<Playlist> playlist, const UPnPDirObject *dop)
{
    if (!playlist) {
        // just in case...
        return 0;
    }
    std::vector<UPnPP::ProtocolinfoEntry> rdrprotinf;
    playlist->getprotoinfo(rdrprotinf);
    LOGDEB("chooseResource: got " << rdrprotinf.size() << " protocol info entries and " <<
           dop->m_resources.size() << " resources \n");
#if 0
    {
        std::string allformat;
        for (const auto& e: rdrprotinf) {
            allformat += e.contentFormat + " ";
        }
        LOGDEB0("chooseResource, formats: " << allformat << "\n");
    }
#endif
    
    if (rdrprotinf.size() == 0) {
        // Hope for the best
        return 0;
    }

    // Look at the available formats. and try to find a match with what the player will accept.
    int bestpref{100000};
    int bestidx = -1;
    int idx_mp2 = -1;
    UPnPP::ProtocolinfoEntry e_mp2;
    std::string bestfmt;
    for (unsigned int idx = 0; idx < dop->m_resources.size(); idx++) {
        UPnPP::ProtocolinfoEntry e;
        int pref;
        if (!dop->m_resources[idx].protoInfo(e)) {
            // No protocolinfo ??
            LOGDEB0("chooseResource: idx " << idx << " no protocolinfo\n");
            continue;
        }
        LOGDEB0("chooseResource: idx " << idx << " format [" << e.contentFormat << "]\n");
        if (!std::binary_search(rdrprotinf.begin(), rdrprotinf.end(), e,
                                [] (const UPnPP::ProtocolinfoEntry&a,
                                    const UPnPP::ProtocolinfoEntry&b)->bool {
                                    return a.contentFormat < b.contentFormat;})){
            // Not supported by renderer
            LOGDEB("chooseResource: idx " << idx << " format " <<
                   e.contentFormat << " not supported by renderer" << "\n");
            continue;
        }
        LOGDEB0("chooseResource: " << e.contentFormat << " is supported\n");
        const auto it = formatprefs.find(e.contentFormat);
        if (it != formatprefs.end()) {
            pref = it->second;
        } else {
            pref = 10000;
        }
        if (pref <= bestpref) {
            if (e.contentFormat == "audio/mpeg" || e.contentFormat == "audio/x-mpeg") {
                // audio/mpeg does not discriminate between mp2/mp3 If we have both, prefer mp2 as
                // this is probably the original format.
                if (dlna_org_pn(e) == "MP2_MPS") {
                    idx_mp2 = idx;
                    e_mp2 = e;
                }
            }
            bestpref = pref;
            bestidx = idx;
            bestfmt = e.contentFormat;
        }
    }

    if (bestfmt == "audio/mpeg" || bestfmt == "audio/x-mpeg") {
        if (idx_mp2 >= 0) {
            LOGDEB("chooseResource: prefering mpeg MP2_MPS\n");
            bestidx = idx_mp2;
            bestfmt = e_mp2.contentFormat;
        }
    }
    
    LOGDEB("chooseResource: bestidx " << int(bestidx) << " format " << bestfmt << "\n");
    return bestidx;
}

bool udirentToMetadata(const UPnPDirObject *dop, MetaData *mdp, std::shared_ptr<Playlist> playlist)
{
    if (dop == 0 || mdp == 0)
        return false;
    // Can't do anything with the ids, they have nothing to do with the upnp id and parentid
    mdp->id = -1;
    mdp->artist_id = -1;
    mdp->album_id = -1;

    mdp->title = u8s2qs(dop->m_title);
    LOGDEB1("udirentToMetadata: title: " << qs2utf8s(mdp->title) << "\n");
    mdp->artist = uf2qs(dop, "upnp:artist", false);
    mdp->album = uf2qs(dop, "upnp:album", false);
    mdp->rating = 0;
    QString dur = uf2qs(dop, "duration", true); 
    if (!dur.isEmpty()) {
        std::string sdur((const char*)dur.toUtf8());
        mdp->length_ms = upnpdurationtos(sdur) * 1000;
    } else {
        mdp->length_ms = 0;
    }
    // UPnP DIDL defines a "dc:date" property, YYYY-MM-DD,
    // but I've never seen it set...
    mdp->year = 0;

    mdp->track_num = uf2qs(dop, "upnp:originalTrackNumber", false).toInt();
    mdp->bitrate = uf2qs(dop, "bitrate", true).toInt();
    mdp->is_extern = false;
    mdp->filesize = uf2qs(dop, "size", true).toLongLong();;
    mdp->comment = "";
    mdp->discnumber = 0;
    mdp->genres << uf2qs(dop, "upnp:genre", false);
    mdp->n_discs = -1;
    mdp->is_extern = false;
    mdp->pl_selected = false;
    mdp->pl_dragged = false;
    mdp->pl_playing = false;
    mdp->unused1 = false;
    mdp->is_disabled = false;
    mdp->didl = u8s2qs(dop->getdidl());
    mdp->albumArtURI = uf2qs(dop, "upnp:albumArtURI", false);

    mdp->url = "";
    if (dop->m_resources.empty()) {
        // Happens with radios (because the stream url is defined elsewhere) not always an error
        LOGDEB("Item tt [" << dop->m_title << "] has no resources.\n");
        return true;
    }
    int idx = 0;
    if (playlist) {
        bool noproto{false};
        QVariant value = rendererSetting("noprotoovrrd", u8s2qs(playlist->getFriendlyName()));
        if (value.isValid() && value.toString() != "Default") {
            std::string s = qs2utf8s(value.toString());
            LOGDEB0("udirentToMetadata [" << playlist->getFriendlyName() <<
                    "] rdr-specific noproto is " << s << '\n');
            if (s == "On")
                noproto = true;
        } else {
            LOGDEB0("udirentToMetadata [" << playlist->getFriendlyName() <<
                    "] no rdr-specific noproto or Default\n");
            QSettings settings;
            noproto = settings.value("noprotoinfocheck").toBool();
        }
        LOGDEB0("udirentToMetadata [" << playlist->getFriendlyName() <<
                "] noproto ends up as " << noproto << '\n');
        if (!noproto && (idx = chooseResource(playlist, dop)) < 0) {
            return false;
        }
        LOGDEB0("udirentToMetadata [" << playlist->getFriendlyName() <<
                "] using URL from resource index " << idx << " [" <<
                dop->m_resources[idx].m_uri << "]\n");
    }
    mdp->url = u8s2qs(dop->m_resources[idx].m_uri);

    return true;
}

static std::set<std::string> specfields{"upnp:artist", "upnp:album", "upnp:genre",
        "upnp:albumArtURI", "upnp:originalTrackNumber"};

QString metaDataToHtml(const MetaData* _metap, UPnPDirObject& dirent, bool okdidl)
{
    MetaData empty;
    const MetaData* metap = _metap ? _metap : &empty;
    std::string html, val, line;

    std::string tmpl =
        "<tr><td><b>%(nm):</b></td><td colspan='2'>%(val)</td><td></td></tr>";

    html = "<table style='width:100%'>";
    html += "<tr><th></th><th style='width:15%'></th><th></th></tr>";
    // Some fields are treated specially (see specfields list above), because we want them in order
    // and/or with adjusted label.
    val = okdidl ? dirent.m_title : qs2utf8s(metap->title);
    if (!val.empty()) {
        pcSubst(tmpl, line, {{"nm", "Title"}, {"val", val}});
        html += line;
    }
    val = okdidl ? dirent.f2s("upnp:artist", false) : qs2utf8s(metap->artist);
    if (!val.empty()) {
        pcSubst(tmpl, line, {{"nm", "Artist"}, {"val", val}});
        html += line;
    }
    val = okdidl ? dirent.f2s("upnp:album", false) : qs2utf8s(metap->album);
    if (!val.empty()) {
        pcSubst(tmpl, line, {{"nm", "Album"}, {"val", val}});
        html += line;
    }
    val = okdidl ? dirent.f2s("upnp:genre", false) :
        (metap->genres.size() ? qs2utf8s(metap->genres[0]): std::string());
    if (!val.empty()) {
        pcSubst(tmpl, line, {{"nm", "Genre"}, {"val", val}});
        html += line;
    }
    val = okdidl ? dirent.f2s("upnp:originalTrackNumber", false) : "";
    if (!val.empty()) {
        pcSubst(tmpl, line, {{"nm", "Track number"}, {"val", val}});
        html += line;
    }

    if (okdidl) {
        // Dump all the rest
        for (auto it : dirent.m_props) {
            if (specfields.find(it.first) == specfields.end() && !it.second.empty()) {
                pcSubst(tmpl, line, {{"nm", it.first}, {"val", it.second}});
                html += line;
            }
        }
        val = dirent.f2s("upnp:albumArtURI", false);
        if (!val.empty()) {
            pcSubst(tmpl, line, {{"nm", "Album Art URI"}, {"val", val}});
            html += line;
        }

        // Resources
        std::string tmpl1 = "<tr><td></td><td><b>%(nm):</b></td><td>%(val)</td></tr>";
        for (auto resit : dirent.m_resources) {
            pcSubst(tmpl, line, {{"nm", "Resource URI"}, {"val", resit.m_uri}});
            html += line;
            for (auto it : resit.m_props) {
                pcSubst(tmpl1, line, {{"nm", it.first}, {"val", it.second}});
                html += line;
            }
        }
    }
    html += "</table>";
    LOGDEB1("metaDataToHtml: html: " << html << "\n");
    return u8s2qs(html);
}

QString metaDataToHtml(const MetaData* metap)
{
    // We reparse from didl because we want to catch everything, even what does not go into the
    // MetaData fields. Also it may happen that we have something in MetaData which does not come
    // from the Didl data
    UPnPDirContent dir;
    UPnPDirObject dirent;
    bool okdidl(true);
    LOGDEB("metaDataToHtml: DIDL: " << qs2utf8s(metap->didl) << "\n");
    if (metap->didl.isEmpty() || !dir.parse(qs2utf8s(metap->didl)) || dir.m_items.empty()) {
        okdidl = false;
        LOGDEB1("metaDataToHtml: no DIDL data or no items\n");
    } else {
        dirent = dir.m_items[0];
    }
    return metaDataToHtml(metap, dirent, okdidl);
}

std::string rendererSettingsKey(const QString& varnm, const QString& friendlyname)
{
    return std::string("renderers/") + qs2utf8s(friendlyname) + "/" + qs2utf8s(varnm);
}

QVariant rendererSetting(const QString& varnm, const QString& friendlyname)
{
    QSettings settings;
    QString rdrvarnm = ::u8s2qs(rendererSettingsKey(varnm, friendlyname));
    return settings.value(rdrvarnm);
}

bool playlistToCSV(const MetaDataList& pl, const QString& qfn)
{
    std::ofstream ost;
    ost.open(qs2utf8s(qfn), std::fstream::out | std::ofstream::trunc);
    if (!ost.is_open()) {
        return false;
    }

    std::vector<std::string> fields {"artist", "album", "title", "url"};
    auto line = stringsToCSV(fields);
    ost << line << "\n";
    for (const auto& entry : pl) {
        fields[0] = qs2utf8s(entry.artist);
        fields[1] = qs2utf8s(entry.album);
        fields[2] = qs2utf8s(entry.title);
        fields[3] = qs2utf8s(entry.url);
        line = stringsToCSV(fields);
        ost << line << "\n";
    }
    ost.close();
    return true;
}

bool playlistToM3U(const MetaDataList& pl, const QString& qfn)
{
    std::ofstream ost;
    ost.open(qs2utf8s(qfn), std::fstream::out | std::ofstream::trunc);
    if (!ost.is_open()) {
        return false;
    }
    ost << "#EXTM3U\n";
    for (const auto& entry : pl) {
        int seconds = entry.length_ms / 1000;
        ost << "#EXTART:" << qs2utf8s(entry.artist) << '\n' << 
            "#EXTALB:" << qs2utf8s(entry.album) << '\n' << 
            "#EXTINF:" << seconds <<"," << qs2utf8s(entry.title) << '\n' <<
            qs2utf8s(entry.url) << "\n\n";
    }
    ost.close();
    return true;
}
